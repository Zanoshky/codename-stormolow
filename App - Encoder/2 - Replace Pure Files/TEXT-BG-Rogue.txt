    I always get what I want, eventually.
 
    I remember growing up in the city. There were people
    everywhere - merchants, tradesmen, soldiers, noblemen. It mattered
    little to me, or them. Back then I was just a rugrat, some homeless boy
    running around in the streets with no family. I learned to survive
    though, the best way of course was to steal.
 
    Living on the outskirts I learned to be quick on my feet, escaping
    bullies and the city guard by hiding or climbing over walls and houses.
    After a while though, I got good enough to prevent the authorities from
    even noticing me.
 
    I got myself a new image, smarter clothes and I bathed as often as a
    nobleman. I soon found out that by posing as a poet or a bard and
    dressing smartly got me into circles of a higher pecking order in the
    city. The girls adored me and I played many a maid and a window for
    favour and a bed for the night, only to be seen the night after
    escorting her rival to a dance or a feast.
 
    In this time I grew rich, stealing not only with my fingers and the
    favour of a woman - I also learned to steal with my mind. I learned to
    sit and listen to all I heard and how to find out more. This gave me the
    ability to sell information or to use this information to find out where
    the most costly of items were hidden.
 
    To turn my stolen goods into the coin I needed, I would travel to other
    towns and villages in Opimaesta. I built contacts to pawn off my ill
    gotten gains.
 
    It was only till recently that I found out about an item more valuable
    and sought-after than anything I have ever taken for myself. There was
    first only a rumor of a myth, but eventually real witnesses and reports
    came to the city and my ears heard every little detail.
 
    There was a being of immense power, that had in its possession a magical
    orb. This crystal had the ability to grant you immortality. Life had
    been the only thing I could not steal more of to have, until now.
 
    So I set out from the city to be in the wilder places of Opimaesta,
    moving between smaller towns, looking for this being that carries this
    item of my desire.