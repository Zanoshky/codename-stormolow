﻿//Include header files
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>

//Using name std
using namespace std;

//Globaly declared constants
const int NUMBEROFITEMS = 120;
const int NUMBEROFWEAPONS = 41;
const int NUMBEROFARMORS = 34;
const int NUMBEROFPOTIONS = 29;
const int NUMBEROFCARDS = 13;
const int NUMBEROFSKINNER = 8;
const int NUMBEROFMAP = 3;

//Declaring functions
int code_all();					
int code_string(string &str);
int code_file(char input[],char output[]);
													
//
// STRUCTURES
//

struct items {	
	int ID;
	string Name;
	string Description;
	string Location;
	string Type;
	int Buy;
	int Sell;
	int Requires1;
	int Requires2;
	int Requires3;
}item[NUMBEROFITEMS];

struct armors {
	int ID;
	string Name;
	string Description;
	string Location;
	string Race;
	string Type;
	int Buy;
	int Sell;
	int Health;
	int Defense;
	int Agility;
	int Level;
}armor[NUMBEROFARMORS];

struct weapons {
	int ID;
	string Name;
	string Description;
	string Location;
	string Race;
	string Type;
	int Buy;
	int Sell;
	int Magic;
	int Strength;
	int Agility;
	int Level;
}weapon[NUMBEROFWEAPONS];

struct potions {
	int ID;
	string Name;
	string Type;
	int Buy;
	int Sell;
	int Level;
	int Health;
	int Strength;
	int Magic;
	int Defense;
	int Agility;
	int Requires1;
	int Requires2;
	int Requires3;
}potion[NUMBEROFPOTIONS];

struct deck {
	int Cardvalue;
	string Cardname;
	int Lower_bonus;
	int Higher_bonus;
}cards[NUMBEROFCARDS];

struct skinners{
	int ID;
	string Name;
	int Fur;
	int Hide;
	int Buy;
	int Size;
	int Owned;
}skinner[NUMBEROFSKINNER];

struct maps{
	int ID;
	string Region_Name;
	string Area1;
	int Area1_ID1;
	int Area1_ID2;
	string Area2;
	int Area2_ID1;
	int Area2_ID2;
	string Area3;
	int Area3_ID1;
	int Area3_ID2;
	int Area3_ID3;
}map[NUMBEROFMAP];



//
// Encoder Main Code
//

int main()
{
	char* rownames = new char[161];						//Create a storage place for the rownames (1st row of each excelfile)
	const char point = ';';								//diffrents one row from the other one in CSV files
	char* chararray = new char[100];					//strings can't be passed directly

	//-----------------------

	ifstream itemdata;									//creating a new intputstream 
	itemdata.open("DATA-Items.csv",ios::in);				//set "DATA-Items.csv" as input stream

	if (!itemdata.good())								//checking for errors
	{								
		cout << "Error occurred in file DATA-Items.csv : ";

		if (itemdata.eof())								//eof = End Of File
			cout << "EMPTY FILE \n";
		else
			cout << "404 \n";		

		system("PAUSE");
		return 1;
	}

	itemdata.getline(rownames,160);					//getting the rownames

	for (int itemnumber = 0; itemnumber < NUMBEROFITEMS; itemnumber++)
	{
		itemdata >> item[itemnumber].ID;			//get each id number
		itemdata.ignore();							//ignoring the ; after the number

		itemdata.getline(chararray,50,point);		
		item[itemnumber].Name = chararray;			//setting name

		itemdata.getline(chararray,100,point);
		item[itemnumber].Description = chararray;	//setting description

		itemdata.getline(chararray,100,point);
		item[itemnumber].Location = chararray;	

		itemdata.getline(chararray,100,point);
		item[itemnumber].Type = chararray;

		itemdata >> item[itemnumber].Buy;			
		itemdata.ignore();	

		itemdata >> item[itemnumber].Sell;			
		itemdata.ignore();

		itemdata >> item[itemnumber].Requires1;		
		itemdata.ignore();	

		itemdata >> item[itemnumber].Requires2;		
		itemdata.ignore();	

		itemdata >> item[itemnumber].Requires3;		
		itemdata.ignore();
	}

	itemdata.close();

	//-----------------------

	ifstream armordata;
	armordata.open("DATA-Armors.csv",ios::in);	

	if (!armordata.good())
	{							
		cout << "Error occurred in file DATA-Armors.csv : ";

		if (armordata.eof())					
			cout << "EMPTY FILE \n";
		else
			cout << "404 \n";		

		system("PAUSE");
		return 1;
	}

	armordata.getline(rownames,160);

	for (int armornumber = 0; armornumber < NUMBEROFARMORS; armornumber++)
	{
		armordata >> armor[armornumber].ID;	
		armordata.ignore();				

		armordata.getline(chararray,50,point);		
		armor[armornumber].Name = chararray;

		armordata.getline(chararray,100,point);
		armor[armornumber].Description = chararray;	

		armordata.getline(chararray,100,point);
		armor[armornumber].Location = chararray;

		armordata.getline(chararray,20,point);
		armor[armornumber].Race = chararray;

		armordata.getline(chararray,20,point);
		armor[armornumber].Type = chararray;
		
		armordata >> armor[armornumber].Buy;			
		armordata.ignore();	

		armordata >> armor[armornumber].Sell;			
		armordata.ignore();	

		armordata >> armor[armornumber].Level;		
		armordata.ignore();

		armordata >> armor[armornumber].Health;		
		armordata.ignore();	

		armordata >> armor[armornumber].Defense;		
		armordata.ignore();	

		armordata >> armor[armornumber].Agility;			
		armordata.ignore();	
	}

	armordata.close();

	//-----------------------

	ifstream weapondata;
	weapondata.open("DATA-Weapons.csv",ios::in);		

	if (!weapondata.good())
	{						
		cout << "Error occurred in file DATA-Weapons.csv : ";

		if (weapondata.eof())	
			cout << "EMPTY FILE \n";
		else
			cout << "404 \n";		

		system("PAUSE");
		return 1;
	}

	weapondata.getline(rownames,160);

	for (int weaponnumber = 0; weaponnumber < NUMBEROFWEAPONS; weaponnumber++)
	{
		weapondata >> weapon[weaponnumber].ID;
		weapondata.ignore();			

		weapondata.getline(chararray,50,point);		
		weapon[weaponnumber].Name = chararray;	

		weapondata.getline(chararray,100,point);
		weapon[weaponnumber].Description = chararray;

		weapondata.getline(chararray,100,point);
		weapon[weaponnumber].Location = chararray;

		weapondata.getline(chararray,20,point);
		weapon[weaponnumber].Race = chararray;

		weapondata.getline(chararray,20,point);
		weapon[weaponnumber].Type = chararray;

		weapondata >> weapon[weaponnumber].Buy;			
		weapondata.ignore();	

		weapondata >> weapon[weaponnumber].Sell;			
		weapondata.ignore();	

		weapondata >> weapon[weaponnumber].Level;		
		weapondata.ignore();

		weapondata >> weapon[weaponnumber].Magic;		
		weapondata.ignore();	

		weapondata >> weapon[weaponnumber].Strength;		
		weapondata.ignore();	

		weapondata >> weapon[weaponnumber].Agility;			
		weapondata.ignore();	
	}

	weapondata.close();

	//-----------------------

	ifstream potiondata;
	potiondata.open("DATA-Potions.csv",ios::in);	

	if (!potiondata.good())
	{							
		cout << "Error occurred in file DATA-Potions.csv : ";

		if (potiondata.eof())	
			cout << "EMPTY FILE \n";
		else
			cout << "404 \n";		

		system("PAUSE");
		return 1;
	}

	potiondata.getline(rownames,160);		

	for (int potionnumber = 0; potionnumber < NUMBEROFPOTIONS; potionnumber++)
	{
		potiondata >> potion[potionnumber].ID;	
		potiondata.ignore();		

		potiondata.getline(chararray,50,point);		
		potion[potionnumber].Name = chararray;	

		potiondata.getline(chararray,100,point);
		potion[potionnumber].Type = chararray;

		potiondata >> potion[potionnumber].Buy;			
		potiondata.ignore();	

		potiondata >> potion[potionnumber].Sell;			
		potiondata.ignore();	

		potiondata >> potion[potionnumber].Level;			
		potiondata.ignore();	

		potiondata >> potion[potionnumber].Health;		
		potiondata.ignore();	

		potiondata >> potion[potionnumber].Magic;		
		potiondata.ignore();	

		potiondata >> potion[potionnumber].Strength;		
		potiondata.ignore();	

		potiondata >> potion[potionnumber].Defense;		
		potiondata.ignore();	

		potiondata >> potion[potionnumber].Agility;			
		potiondata.ignore();	

	 	potiondata >> potion[potionnumber].Requires1;		
		potiondata.ignore();

		potiondata >> potion[potionnumber].Requires2;		
		potiondata.ignore();
		
		potiondata >> potion[potionnumber].Requires3;		
		potiondata.ignore();
	}

	potiondata.close();

	//-----------------------

	ifstream cardsdata;
	cardsdata.open("DATA-Cards.csv",ios::in);

	if (!cardsdata.good())
	{
		cout << "Error occurred in file DATA-Cards.csv : ";

		if (cardsdata.eof())					
			cout << "EMPTY FILE \n";
		else
			cout << "404 \n";		

		system("PAUSE");
		return 1;
	}

	cardsdata.getline(rownames,160);

	for (int cardsnumber = 0; cardsnumber < NUMBEROFCARDS; cardsnumber++)
	{
		cardsdata >> cards[cardsnumber].Cardvalue;	
		cardsdata.ignore();				

		cardsdata.getline(chararray,50,point);		
		cards[cardsnumber].Cardname = chararray;	

		cardsdata >> cards[cardsnumber].Lower_bonus;			
		cardsdata.ignore();	

		cardsdata >> cards[cardsnumber].Higher_bonus;			
		cardsdata.ignore();	
	}

	cardsdata.close();

	//-----------------------

	ifstream skinnerdata;
	skinnerdata.open("DATA-Skinner.csv",ios::in);

	if (!skinnerdata.good())
	{	
		cout << "Error occurred in file DATA-Skinner.csv : ";

		if (skinnerdata.eof())					
			cout << "EMPTY FILE \n";
		else
			cout << "404 \n";		
	
		system("PAUSE");
		return 1;
	}

	skinnerdata.getline(rownames,160);	

	for (int skinnernumber = 0; skinnernumber < NUMBEROFSKINNER; skinnernumber++)
	{
		skinnerdata >> skinner[skinnernumber].ID;	
		skinnerdata.ignore();					

		skinnerdata.getline(chararray,50,point);		
		skinner[skinnernumber].Name = chararray;	

		skinnerdata >> skinner[skinnernumber].Buy;			
		skinnerdata.ignore();	

		skinnerdata >> skinner[skinnernumber].Fur;			
		skinnerdata.ignore();	

		skinnerdata >> skinner[skinnernumber].Hide;			
		skinnerdata.ignore();	

		skinnerdata >> skinner[skinnernumber].Size;			
		skinnerdata.ignore();	

		skinnerdata >> skinner[skinnernumber].Owned;			
		skinnerdata.ignore();	
	}

	skinnerdata.close();

	//-----------------------

	ifstream mapdata;
	mapdata.open("DATA-Map.csv",ios::in);	
	if (!mapdata.good())
	{	
		cout << "Error occurred in file DATA-Map.csv : ";

		if (mapdata.eof())			
			cout << "EMPTY FILE \n";
		else
			cout << "404 \n";		
	
		system("PAUSE");
		return 1;
	}

	mapdata.getline(rownames,160);

	for (int mapnumber = 0; mapnumber < NUMBEROFMAP; mapnumber++)
	{
		mapdata >> map[mapnumber].ID;			
		mapdata.ignore();			

		mapdata.getline(chararray,50,point);		
		map[mapnumber].Region_Name = chararray;	

		mapdata.getline(chararray,50,point);		
		map[mapnumber].Area1 = chararray;

		mapdata >> map[mapnumber].Area1_ID1;			
		mapdata.ignore();	

		mapdata >> map[mapnumber].Area1_ID2;			
		mapdata.ignore();	

		mapdata.getline(chararray,50,point);		
		map[mapnumber].Area2 = chararray;

		mapdata >> map[mapnumber].Area2_ID1;			
		mapdata.ignore();	

		mapdata >> map[mapnumber].Area2_ID2;			
		mapdata.ignore();	

		mapdata.getline(chararray,50,point);		
		map[mapnumber].Area3 = chararray;

		mapdata >> map[mapnumber].Area3_ID1;			
		mapdata.ignore();	

		mapdata >> map[mapnumber].Area3_ID2;			
		mapdata.ignore();

		mapdata >> map[mapnumber].Area3_ID3;			
		mapdata.ignore();
	}
	
	mapdata.close();

	//
	// Showing inputed data in console
	//

	for (int itemnumber =0; itemnumber < NUMBEROFITEMS; itemnumber++)	//Showing all items in promt
	{																	//can't do it at once, prompt get's full
		cout << item[itemnumber].ID << " " << item[itemnumber].Name <<"\n\t";
		cout << item[itemnumber].Description<<":\n\t";
		cout << item[itemnumber].Location <<"     ("<< item[itemnumber].Type <<")\n\t";
		cout <<"Value: " << item[itemnumber].Buy <<"(" << item[itemnumber].Sell <<") \n\t ";
		cout << item[itemnumber].Requires1<<"\n\t "<< item[itemnumber].Requires2 <<"\n\t " << item[itemnumber].Requires3 << "\n\n";
		
		if (20==(itemnumber%21))					//shows the first 20 items,  then next 20, etc...
			system("PAUSE");
	}

	for (int weaponnumber =0; weaponnumber < NUMBEROFWEAPONS; weaponnumber++)
	{ 
		cout << weapon[weaponnumber].ID << " " << weapon[weaponnumber].Name <<"\n\t";
		cout << weapon[weaponnumber].Description<<":\n\t";
		cout << weapon[weaponnumber].Type << "    ("<< weapon[weaponnumber].Race <<")\n\t";
		cout <<"Value: " << weapon[weaponnumber].Buy <<"(" << weapon[weaponnumber].Sell <<") \n\t";
		cout << weapon[weaponnumber].Location<<"\n\t";
		cout <<"Strength:   "<< weapon[weaponnumber].Strength << " \n\tMagic:      "<< weapon[weaponnumber].Magic <<"\n\tAgility:    "<< weapon[weaponnumber].Agility <<"\n\t";
		cout <<"Level:      " << weapon[weaponnumber].Level << "\n\n";
		
		if (20==(weaponnumber%21))
			system("PAUSE");
	}

	for (int armornumber =0; armornumber < NUMBEROFARMORS; armornumber++)
	{
		cout <<armor[armornumber].ID << " " << armor[armornumber].Name <<"\n\t";
		cout << armor[armornumber].Description;
		cout << armor[armornumber].Type << "    ("<< armor[armornumber].Race <<")\n\t";
		cout <<":\n\tvalue: " << armor[armornumber].Buy <<"(" << armor[armornumber].Sell <<") \n\t";
		cout << armor[armornumber].Location<<"\n\t";
		cout << "Health:      " << armor[armornumber].Health << " \n\tDefense:     "<< armor[armornumber].Defense<<"\n\tAgility:     "<< armor[armornumber].Agility <<"\n\t";
		cout << "Level:       " << armor[armornumber].Level << "\n\n";
		
		if (20==(armornumber%21))
			system("PAUSE");
	}

	for (int potionnumber =0; potionnumber < NUMBEROFPOTIONS; potionnumber++)
	{
		cout << potion[potionnumber].ID << " " << potion[potionnumber].Name<<"\n\t";
		cout << potion[potionnumber].Type<<":\n\t";
		cout << "value: " << potion[potionnumber].Buy <<"(" << potion[potionnumber].Sell <<")\n\t";
		cout << "Health:        " << potion[potionnumber].Health <<"\n\tMagic:         " << potion[potionnumber].Magic <<"\n\tStrength:      " << potion[potionnumber].Strength <<" \n\tDefense:       "<< potion[potionnumber].Defense<<"\n\tAgility:       "<< potion[potionnumber].Agility <<"\n\t"  << "\n\t ";
		cout << potion[potionnumber].Requires1 <<"\n\t "<< potion[potionnumber].Requires2 <<"\n\t " << potion[potionnumber].Requires1 << "\n\n";

		if (20==(potionnumber%21))	
			system("PAUSE");
	}

	for (int cardsnumber =0; cardsnumber < NUMBEROFCARDS; cardsnumber++)
	{ 
		cout << cards[cardsnumber].Cardvalue << " " << cards[cardsnumber].Cardname <<"\n\t";
		cout << "Range:           "  << cards[cardsnumber].Lower_bonus <<" -> " << cards[cardsnumber].Higher_bonus << "\n\n";
		
		if (20==(cardsnumber%21))				
			system("PAUSE");
	}

	for (int skinnernumber =0; skinnernumber < NUMBEROFSKINNER; skinnernumber++)
	{ 
		cout << skinner[skinnernumber].ID << " " << skinner[skinnernumber].Name << "\n\t";
		cout << "Fur:   " << skinner[skinnernumber].Fur <<" and hide:   " << skinner[skinnernumber].Hide << "\n\t";
		cout << "Size:     " << skinner[skinnernumber].Size << "\n\t";
		cout << "Price:    " << skinner[skinnernumber].Buy << "\n\t";
		cout << "Owned:    "<< skinner[skinnernumber].Owned << "\n\n";
		
		if (20==(skinnernumber%21))				
			system("PAUSE");
	}

	for (int mapnumber =0; mapnumber<NUMBEROFMAP; mapnumber++)
	{
		cout << map[mapnumber].Region_Name << ":\n\t";
		cout << map[mapnumber].Area1 << ": \n\t\t" << map[mapnumber].Area1_ID1 << "\n\t\t" << map[mapnumber].Area1_ID2 <<"\n\t";
		cout << map[mapnumber].Area2 << ": \n\t\t" << map[mapnumber].Area2_ID2 << "\n\t\t" << map[mapnumber].Area2_ID2 <<"\n\t";
		cout << map[mapnumber].Area3 << ": \n\t\t" << map[mapnumber].Area3_ID3 << "\n\t\t" << map[mapnumber].Area3_ID2 <<"\n\t\t" << map[mapnumber].Area3_ID3 << "\n";
	}

	system("PAUSE");

	//
	//
	code_all();											// Main function of this program - encodes all
	//
	//

	//
	//Writing encoded data into files
	//

	ofstream datafile;
	datafile.open("CODE-Data.nsk",ios::out);				//the created file will be in the same directory as the .exe file

	if (datafile.is_open())
	{
		for(int itemnumber=0;itemnumber<NUMBEROFITEMS; itemnumber++)	//sending item per item to the file
		{			
			datafile << item[itemnumber].ID <<";";
			datafile << item[itemnumber].Name << ";" ;
			datafile << item[itemnumber].Description << ";" ;
			datafile << item[itemnumber].Location <<  ";" ;
			datafile << item[itemnumber].Type << ";";
			datafile << item[itemnumber].Buy << ";";
			datafile << item[itemnumber].Sell << ";";
			datafile << item[itemnumber].Requires1 << ";" ;
			datafile << item[itemnumber].Requires2 << ";" ;
			datafile << item[itemnumber].Requires3 << ";" ;	

			//Creating an extra value to check if the ints have been touched; ending the line and flushing stream
			datafile << ( item[itemnumber].ID -item[itemnumber].Buy +item[itemnumber].Sell+item[itemnumber].Requires1-item[itemnumber].Requires2+item[itemnumber].Requires3) << ";" ;
		}

		for(int armornumber=0;armornumber<NUMBEROFARMORS; armornumber++)
		{
			datafile << armor[armornumber].ID <<";";
			datafile << armor[armornumber].Name << ";" ;
			datafile << armor[armornumber].Description << ";" ;
			datafile << armor[armornumber].Location <<  ";" ;
			datafile << armor[armornumber].Race <<  ";" ;
			datafile << armor[armornumber].Type <<  ";" ;
			datafile << armor[armornumber].Buy << ";";
			datafile << armor[armornumber].Sell << ";";
			datafile << armor[armornumber].Level << ";";	
			datafile << armor[armornumber].Health << ";" ;
			datafile << armor[armornumber].Defense << ";" ;
			datafile << armor[armornumber].Agility << ";" ;	
			//SECURITY Measure line
			datafile << (armor[armornumber].ID -armor[armornumber].Buy +armor[armornumber].Sell+armor[armornumber].Health+armor[armornumber].Agility-armor[armornumber].Defense +armor[armornumber].Level) << ";" ;
		}

		for(int weaponnumber=0;weaponnumber<NUMBEROFWEAPONS; weaponnumber++)
		{
			datafile << weapon[weaponnumber].ID <<";";
			datafile << weapon[weaponnumber].Name << ";" ;
			datafile << weapon[weaponnumber].Description << ";" ;
			datafile << weapon[weaponnumber].Location <<  ";" ;
			datafile << weapon[weaponnumber].Race <<  ";" ;
			datafile << weapon[weaponnumber].Type <<  ";" ;
			datafile << weapon[weaponnumber].Buy << ";";
			datafile << weapon[weaponnumber].Sell << ";";
			datafile << weapon[weaponnumber].Level << ";";
			datafile << weapon[weaponnumber].Magic << ";" ;
			datafile << weapon[weaponnumber].Strength << ";" ;
			datafile << weapon[weaponnumber].Agility << ";" ;
			//SECURITY Measure line
			datafile << (weapon[weaponnumber].ID -weapon[weaponnumber].Buy +weapon[weaponnumber].Sell +weapon[weaponnumber].Strength -weapon[weaponnumber].Magic +weapon[weaponnumber].Level) << ";" ;
		}

		for(int potionnumber=0;potionnumber<NUMBEROFPOTIONS; potionnumber++)
		{
			datafile << potion[potionnumber].ID <<";";
			datafile << potion[potionnumber].Name << ";" ;
			datafile << potion[potionnumber].Type << ";" ;
			datafile << potion[potionnumber].Buy << ";";
			datafile << potion[potionnumber].Sell << ";";
			datafile << potion[potionnumber].Level <<";";
			datafile << potion[potionnumber].Health << ";" ;
			datafile << potion[potionnumber].Magic << ";" ;
			datafile << potion[potionnumber].Strength << ";" ;			
			datafile << potion[potionnumber].Defense << ";" ;
			datafile << potion[potionnumber].Agility << ";" ;
			datafile << potion[potionnumber].Requires1 << ";" ;
			datafile << potion[potionnumber].Requires2 << ";" ;
			datafile << potion[potionnumber].Requires3 << ";" ;
			//SECURITY Measure line
			datafile << (potion[potionnumber].ID -potion[potionnumber].Buy +potion[potionnumber].Sell +potion[potionnumber].Strength -potion[potionnumber].Magic) <<  ";";
		}

		for(int cardsnumber=0;cardsnumber<NUMBEROFCARDS; cardsnumber++)
		{
			datafile << cards[cardsnumber].Cardvalue <<";";
			datafile << cards[cardsnumber].Cardname << ";" ;
			datafile << cards[cardsnumber].Lower_bonus << ";" ;
			datafile << cards[cardsnumber].Higher_bonus << ";";
			//SECURITY Measure line
			datafile << (cards[cardsnumber].Cardvalue + cards[cardsnumber].Higher_bonus + cards[cardsnumber].Lower_bonus) << ";";
		}
		
		for(int skinnernumber=0; skinnernumber<NUMBEROFSKINNER ; skinnernumber++)
		{
			datafile << skinner[skinnernumber].ID <<";";
			datafile << skinner[skinnernumber].Name << ";" ;
			datafile << skinner[skinnernumber].Buy << ";";
			datafile << skinner[skinnernumber].Fur << ";" ;
			datafile << skinner[skinnernumber].Hide << ";";
			datafile << skinner[skinnernumber].Size << ";";
			datafile << skinner[skinnernumber].Owned << ";";
			//SECURITY Measure line
			datafile << (skinner[skinnernumber].Buy + 2* skinner[skinnernumber].Fur + skinner[skinnernumber].Hide + skinner[skinnernumber].ID * skinner[skinnernumber].Size + skinner[skinnernumber].Owned) << ";";	
		}
		
		for(int mapnumber=0;mapnumber<NUMBEROFMAP; mapnumber++)
		{
			datafile << map[mapnumber].ID<<";";
			datafile << map[mapnumber].Region_Name<< ";" ;
			datafile << map[mapnumber].Area1 << ";";
			datafile << map[mapnumber].Area1_ID1 << ";" ;
			datafile << map[mapnumber].Area1_ID2 << ";";
			datafile << map[mapnumber].Area2 << ";";
			datafile << map[mapnumber].Area2_ID1 << ";" ;
			datafile << map[mapnumber].Area2_ID2 << ";";
			datafile << map[mapnumber].Area3 << ";";
			datafile << map[mapnumber].Area3_ID1 << ";" ;
			datafile << map[mapnumber].Area3_ID2 << ";";
			datafile << map[mapnumber].Area3_ID3 << ";";
			//SECURITY Measure line
			datafile << (map[mapnumber].Area1_ID1 + 2*map[mapnumber].Area1_ID2	+ 3 * map[mapnumber].Area2_ID1 + map[mapnumber].Area2_ID2 + 2 * map[mapnumber].Area3_ID1 + map[mapnumber].Area3_ID2 + map[mapnumber].Area3_ID3) <<";";
		}
	}

	datafile.close();

	//
	// File names of clean data (input) and file names of encrypted data (output)
	//
	
	code_file("TEXT-BG-Barbarian.txt","CODE-BG-Barbarian.nsk");
	code_file("TEXT-BG-Sorceress.txt", "CODE-BG-Sorceress.nsk");
	code_file("TEXT-BG-Rogue.txt", "CODE-BG-Rogue.nsk");
	code_file("TEXT-HeroPick.txt","CODE-HeroPick.nsk");							
	code_file("TEXT-GlassMarblesOne.txt","CODE-GlassMarblesOne.nsk");
	code_file("TEXT-Logo.txt","CODE-Logo.nsk");	
	code_file("TEXT-Warning.txt", "CODE-Warning.nsk");
	code_file("TEXT-Graveyard.txt", "CODE-Graveyard.nsk");

	system("PAUSE");
	return 0;
}

//
// FUNCTIONS
//

int code_file(char input[],char output[])
{
	fstream inputdata(input,ios::in);								//creating an input stream 
	fstream outputdata(output, ios::out);							//and output stream

	string str;														//Helping string for storage	

	if (!inputdata.good())											//safety checks for input file
	{											
		cout << "Error occurred in input file: \n";
		cout << "File not found: "<< input <<" \n";		
		system("PAUSE");
		return 1;
	}

	if (!outputdata.good())											//safety checks for output file
	{
		cout << "Error occurred in output file: \n";

		if (outputdata.eof())					
			cout<< "EMPTY FILE \n";
		else
			cout << "404 : "<< output <<"\n";		

		system("PAUSE");
		return 1;
	}

	while (getline(inputdata,str))									//getting all the lines of the file
	{
		cout << str << "\n";										//printing them on prompt
		code_string(str);											//coding them
		outputdata << str << "\n";									//Writing them in output file
	}

	inputdata.close();
	outputdata.close();

	return 0;
}

//---------------------------------------------------------------------------------------------------------------------------------
											
int code_all()
{				
	for (int itemnumber = 0; itemnumber<NUMBEROFITEMS; itemnumber++)	//coding all items
	{
		item[itemnumber].ID					=(item[itemnumber].ID+1)*758;
		item[itemnumber].Buy				=(item[itemnumber].Buy+1)*48;
		item[itemnumber].Sell				=(item[itemnumber].Sell+1)*5;
		item[itemnumber].Requires1			=(item[itemnumber].Requires1+1)*97;
		item[itemnumber].Requires2			=(item[itemnumber].Requires2+1)*12;
		item[itemnumber].Requires3			=(item[itemnumber].Requires3+1)*5;
		
		code_string(item[itemnumber].Name);								//passing name to string coder.
		code_string(item[itemnumber].Description);						//passing descr to string coder.
		code_string(item[itemnumber].Location);							//passing Location to string coder.
		code_string(item[itemnumber].Type);		
	}
	
	for (int weaponnumber = 0; weaponnumber<NUMBEROFWEAPONS; weaponnumber++)
	{
		weapon[weaponnumber].ID					=(weapon[weaponnumber].ID+1)*758;
		weapon[weaponnumber].Buy				=(weapon[weaponnumber].Buy+1)*48;
		weapon[weaponnumber].Sell				=(weapon[weaponnumber].Sell+1)*5;
		weapon[weaponnumber].Level				=(weapon[weaponnumber].Level+1)*25;
		weapon[weaponnumber].Magic				=(weapon[weaponnumber].Magic+1)*15;
		weapon[weaponnumber].Strength			=(weapon[weaponnumber].Strength+1)*5;
		weapon[weaponnumber].Agility			=(weapon[weaponnumber].Agility+1)*57;

		code_string(weapon[weaponnumber].Name);	
		code_string(weapon[weaponnumber].Description);
		code_string(weapon[weaponnumber].Location);	
		code_string(weapon[weaponnumber].Race);	
		code_string(weapon[weaponnumber].Type);	
	}

	for (int armornumber = 0; armornumber<NUMBEROFARMORS; armornumber++)
	{
		armor[armornumber].ID				=(armor[armornumber].ID+1)*758;
		armor[armornumber].Buy				=(armor[armornumber].Buy+1)*48;
		armor[armornumber].Sell				=(armor[armornumber].Sell+1)*5;
		armor[armornumber].Level			=(armor[armornumber].Level+1)*13;
		armor[armornumber].Health			=(armor[armornumber].Health+1)*158;
		armor[armornumber].Defense			=(armor[armornumber].Defense+1)*158;
		armor[armornumber].Agility			=(armor[armornumber].Agility+1)*57;
		
		code_string(armor[armornumber].Name);
		code_string(armor[armornumber].Description);
		code_string(armor[armornumber].Location);
		code_string(armor[armornumber].Race);
		code_string(armor[armornumber].Type);
	}

	for (int potionnumber = 0; potionnumber<NUMBEROFPOTIONS; potionnumber++)
	{ 
		potion[potionnumber].Agility		=(potion[potionnumber].Agility+1)*57;
		potion[potionnumber].Buy			=(potion[potionnumber].Buy+1)*48;
		potion[potionnumber].Defense		=(potion[potionnumber].Defense+1)*158;
		potion[potionnumber].Health			=(potion[potionnumber].Health+1)*158;
		potion[potionnumber].ID				=(potion[potionnumber].ID+1)*758;
		potion[potionnumber].Magic			=(potion[potionnumber].Magic+1)*15;
		potion[potionnumber].Requires1		=(potion[potionnumber].Requires1+1)*97;
		potion[potionnumber].Requires2		=(potion[potionnumber].Requires2+1)*12;
		potion[potionnumber].Requires3		=(potion[potionnumber].Requires3+1)*5;
		potion[potionnumber].Sell			=(potion[potionnumber].Sell+1)*5;
		potion[potionnumber].Strength		=(potion[potionnumber].Strength+1)*5;

		code_string(potion[potionnumber].Name);	
		code_string(potion[potionnumber].Type);	
	}

	for (int cardsnumber = 0; cardsnumber<NUMBEROFCARDS; cardsnumber++)
	{
		cards[cardsnumber].Cardvalue		=(cards[cardsnumber].Cardvalue+1)*35;
		cards[cardsnumber].Lower_bonus		=(cards[cardsnumber].Lower_bonus+1)*762;
		cards[cardsnumber].Higher_bonus		=(cards[cardsnumber].Higher_bonus+1)*95;
			
		code_string(cards[cardsnumber].Cardname);
	}

	for (int skinnernumber = 0; skinnernumber < NUMBEROFSKINNER; skinnernumber++)
	{
		skinner[skinnernumber].ID			=(skinner[skinnernumber].ID+1)*97;
		skinner[skinnernumber].Fur			=(skinner[skinnernumber].Fur+1)*31;
		skinner[skinnernumber].Hide			=(skinner[skinnernumber].Hide+1)*12;
		skinner[skinnernumber].Buy			=(skinner[skinnernumber].Buy+1)*84;
		skinner[skinnernumber].Size			=(skinner[skinnernumber].Size+1)*45;

		code_string(skinner[skinnernumber].Name);
	}

	for (int mapnumber(0); mapnumber < NUMBEROFMAP; mapnumber++)
	{
		map[mapnumber].ID					 =(map[mapnumber].ID+1)*37;
		map[mapnumber].Area1_ID1			 =(map[mapnumber].Area1_ID1+1)*154;
		map[mapnumber].Area1_ID2			 =(map[mapnumber].Area1_ID2+1)*75;
		map[mapnumber].Area2_ID1			 =(map[mapnumber].Area2_ID1+1)*97;
		map[mapnumber].Area2_ID2			 =(map[mapnumber].Area2_ID2+1)*34;
		map[mapnumber].Area3_ID1			 =(map[mapnumber].Area3_ID1+1)*16;
		map[mapnumber].Area3_ID2			 =(map[mapnumber].Area3_ID2+1)*18;
		map[mapnumber].Area3_ID3			 =(map[mapnumber].Area3_ID3+1)*39;

		code_string(map[mapnumber].Area1);
		code_string(map[mapnumber].Area2);
		code_string(map[mapnumber].Area3);
		code_string(map[mapnumber].Region_Name);
	}

	return 0;
}


int code_string(string &str)
{		
	int const codesize(29);

	//Private XOR modifyed key that makes the MAGIC
	//IT MUST BE THE SAME AS IN THE MAIN FILE OF GAME TO DECRYPT IT; save ASCII characters are: 16 ->19; 22; 24->26; 28 ->31; 128 ->255.
	static int const CODE[codesize] = {14,21,18,17,16,24,18,19,16,16,18,17,16,18,25,18,15,14,21,15,16,17,18,19,20,19,18,17,16};
	int codeletter(0);

	char *a = new char[str.size()+1];				//strings don't support '^' so every string gets an equivalent char[]
	a[str.size()]=0;								//end of the string is always 0 so has to be the end of the char
	memcpy(a,str.c_str(),str.size());				//copying string to char
	for (unsigned int letter = 0; letter < (str.size()); letter++,codeletter++)	 //coding for every letter, BUT the terminating 0;
	{ 
		a[letter]= static_cast <int> (a[letter]) + CODE[codeletter];			//exclusive disjuntion 
		if (codeletter == (codesize-1))											//checking of the end of the code has been reached 
		{										
			codeletter=0;														//going to the next letter of the code
		}
	}

	str=a;
	return 0;
}