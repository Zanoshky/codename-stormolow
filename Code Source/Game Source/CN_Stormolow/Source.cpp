/*
	Codename: Stormolow

	Author: Marko Zanoski	www.facebook.com/zanoski.marko
	Email: zanoski.marko@gmail.com

	This work is licensed under a http://creativecommons.org/licenses/by-nc/3.0/deed.en_US

	Current Version: GraduationDay BETA 1.0
	----------------------------------
	14.12.2012	-	SchoolDays 0.1
	07.12.2012	-	SchoolDays 0.1-a1
	30.11.2012	-	SchoolDays 0.3
	13.12.2012	-	SchoolDays 0.4
	17.03.2013	-	SchoolDays 0.6
	01.08.2013  -   SchoolDays 0.8
	12.03.2014  -   GraduationDay BETA 1.0

	NOTE:
	72 Chars Wide Screen / + 4 From left side

	Version History:
	14.11.2012 - Character Classes Function
	15.11.2012 - Player Pick Class Loop
	17.11.2012 - Added Readfromfiledesign Function, CleanScreen, Set WindowSize, Improved Player Pick Class Loop.
	19.11.2012 - Added Random_Number Function, Background Character Section.
	23.11.2012 - Added Town Loop, General Store Loop, Added Commands: Help book in town, Inventory, General Store.
	28.11.2012 - Added Camp Loop, Fixed Bugs In General Store, Buying Items.
	03.12.2012 - Fixed Redundancy, Change Variable Names, Renamed Command Buy, Added Emporium Back Command, 
				 Added World Section, Improved Camp Section, Added Potion Command in Camp / World. 
				 Recreated Travel Section, Added Battle Section. Recreated enemies variables.
	06.12.2012 - Fixed battle system.
	08.12.2012 - Added Battle Report tracker.
	10.12.2012 - Edited whole player/enemy variable system, Created new armor/weapon/player/enemy system,
				 Recreated Marketplace(Old Bazaar), Added Command Shortcuts, Minor bug fixes.
	12.12.2012 - Implemented backpack, skinning, fixed market bug, added new sections in market - Apothecary,
				 Skinning and Merchant, change look, changed texts, and few minor bugs
	13.12.2012 - Finished Skinning, Apothecary, Merchant, Fixed Backpack bug, Renamed all fails into fizzle,
				 Improved Inventory
	14.12.2012 - Edited Character Selection. Fixed Help
	12.01.2013 - Changed all variables. Added vector backpack.
	24.01.2013 - Fixed the HiLo Game. Mostly the look
	25.01.2013 - Implemented Encoding of Databases, fixed battle section - reduced code, fixed bugs.
	26.01.2013 - Fixed issues with Databases, fixed merchant.
	29.01.2013 - Combined Stats into function. Less code.
	12.02.2013 - Skinner and Craftsman Progress. Fixed big bypass at craftsman.
	13.02.2013 - Fixing the database files
	15.02.2013 - Re-arranged whole code to make it more readable and easier to understand. Fixed code redundancy. 300 Lines,
				 Added icon to the project.
	19.02.2013 - Deleted all cin >> and reworked them into getline. Redundant code of cin.ignore() was fixed.
				 Fixed HUD Text output. Cleaner, more readable. Reworked the backpack into vector of structs.
				 Improved speed of whole programs by chaning from i++ to ++i - results are 3% faster.
				 Changed the potion kits into vector of struct. Added few fuctions to handle the vector of potions.
	21.02.2013 - Reworked Victory. Possibility to loot enemies - generalised option to work for all type of enemies (furry and other),
				 Reworked battle system. Successfully implanted escape system. Finished enemies function for lvl 1 to 10. 
	28.02.2013 - Reworked databases in past few days. Added  Type and Race to weapons and armors. Reworked Armorsmith and Weaponsmith
				 to handle new database, less code. 28% faster results.
	05.03.2013 - Made the GB: Map struct, implanted map traveling.
	06.03.2013 - Improved the encryption.
	17.03.2013 - Added Pharmacist, Increased size of Window from 55 to -> 57
	30.03.2013 - Changed Crafting System, from strict 1-2-3  to 1-2-3|2-3-1|3-2-1 System.
	27.07.2013 - Changed the Looting System. Added function find item by id.
	28.07.2013 - Added credit list on game loading screen.
	01.08.2013 - Reworked account menu into function - 700 lines of code less.
	             Changed the names of encrypted files. Fixed bug where high number would crash while buying in Armorsmith, Weaponsmith
				 Pharmacist.
	05.08.2013 - Fixed bug while traveling. When picking location when to search. Do While Bug!
	06.08.2013 - Fixed bug when traveling. Under statistics it would show town name even if you would travel or camp.
	07.08.2013 - Reworked Use_Resoration_Potion. Putted in a safety net. To ask user if he is sure to use a potion.
				 And a block net from usage when user is full at health or magic.
	12.08.2013 - Made the Mining section. Fixed time from 20h per year to 9h per year. Fixed text on lots of places.
				 Made the Resting in INN.
	26.10.2013 - Fixed Crafting with no items bug, unable to exit.
	29.10.2013 - Added disarm/ armor / weapon any time to change over account option
	15.02.2014 - Fixed lots of GUI issues. Fixed data encryption 2.0. Fixed card dealing lower higher issue.
				 Created Buying section in merchant. Added exit from crafting. Added story to game.
	16.02.2014 - Added drop from backpack. Fixed lots of Warnings Level 3 And Level 4.
	
	-------------------------------------------------------------------------------------------
										Code Snippets
	-------------------------------------------------------------------------------------------
	        AUTOR			DATE POSTED         Solution / code / snippet
	-------------------------------------------------------------------------------------------
	Pubby				-	12.Feb.2012		-	Fixed rand Warning
	Duoas				-	29.Apr.2009		-	void ClearScreen()
	Dharmesh			-	32.Mar.2005		-	void SetWindowsSize()
	Charles Salvia		-	11.Jan.2011		-	bool is_number()
*///-------------------------------------------------------------------------------------------

#include <ctime>
#include <string>
#include <vector>
#include <cmath>
#include <thread>
#include <cstdlib>
#include <conio.h>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <algorithm>
#include <windows.h>

#include "database.h"
#include "resource.h"

using namespace std;

//--------------------------------------------------------------------------------Function Declaration
void Mining_attempt (vector<holder>& backpack, int& backpack_free_space, armors *armor, weapons *weapon, items *item, string& UserChoice, string& item_name, int& player_mining_level, int& player_mining_experience);
void access_account (string location_access, vector<holder>& backpack, vector<string>& backpack_names, int& backpack_free_space, armors *armor, weapons *weapon, items *item, potions *potion, vector<holder>& potion_kit, vector<string>& potionkit_names, int& potionkit_free_space, string& UserChoice, int& user_valid_number, int& user_ID_choice, int& item_id, int& i, string& town_name, string& player_class_name, int& player_glass_marbles, int& player_combined_max_health, int& player_combined_health, int& player_combined_max_magic, int& player_combined_magic, int& player_combined_strength, int& player_combined_defense, int& player_combined_agility, string& player_armor_stats_name, int& player_armor_stats_health, int& player_armor_stats_defense, int& player_armor_stats_agility, string& player_shield_stats_name, int& player_shield_stats_health, int& player_shield_stats_defense, int& player_shield_stats_agility, string& player_offhand_weapon_stats_name, int& player_offhand_weapon_stats_magic, int& player_offhand_weapon_stats_agility, int& player_offhand_weapon_stats_strength, string& player_main_weapon_stats_name, int& player_main_weapon_stats_magic, int& player_main_weapon_stats_agility, int& player_main_weapon_stats_strength, int& showcase_required_combat, int& showcase_required_mining, int& showcase_required_farming, int& showcase_required_gambling, double& overall_player_level, int& overall_level_progress, int& player_combat_level, int& player_combat_experience, int& player_farming_level, int& player_farming_experience, int& player_mining_level, int& player_mining_experience, int& player_gambling_level, int& player_gambling_experience, int& total_number_of_travels, int& current_number_of_travels, int& player_main_weapon_ID_carried, int& player_offhand_weapon_ID_carried, int& special_weapon_equiped, int& player_armor_ID_carried, int& player_shield_ID_carried);
void ClearScreen();
void WaitForEnter();
void Is_Positive (int number);
int Random_Number (int& number);
bool is_number (const string& s);
bool SetWindow (int Width, int Height);
//----------------------------------------
void potion_kit_open (vector<string>& pkit);
void combine_potion_kit (vector<holder>& pkit, vector<string>& kitNames, potions *potion);
//----------------------------------------
void rucksack_open (vector<string>& holder);
void holder_reset (vector<holder>& backpack);
bool compareByID (const holder &a, const holder &b);
void holder_upgrade (vector<holder>& backpack, int increase);
void holder_add_item (vector<holder>& backpack, int item_id);
void holder_remove_item (vector<holder>& backpack, int item_id);
void holder_space_counter (vector<holder>& backpack, int& freespace);
void holder_furandhide_counter (vector<holder>& backpack, int& fur, int& hide);
void find_item_name_by_id (int item_id, string& item_name, armors *armor, weapons *weapon, items *item);
void combine_rucksack (vector<holder>& backpack, vector<string>& ruckNames, armors *armor, weapons *weapon, items *item);
//----------------------------------------
void Mining_Level_Checkup (int got_exp, int& level, int& experience);
void Farming_Level_Checkup (int got_exp, int& level, int& experience);
void Gambling_Level_Checkup (int got_exp, int& level, int& experience);
void Overall_Level_Checkup(double& overall, int combat, int mining, int farming, int gambling);
void Combat_Level_Checkup (string player_class, int got_exp, int& level, int& experience, int& max_health, int& health, int&max_magic, int& magic, int& strength, int& defense, int& agility);
//----------------------------------------
void Escape_Attempt (int plevel, int elevel, int pdefense, int& status, int& duration);
void Super_Damage_Calculator (double& damage, int health, int magic, int agility, int level, int& enemy_health);
void Damage_Calculator (double& adamage, int astrength, int aagility, int alevel, int& dhealth, int ddefense, int dagility);
void Use_Restoration_Potion (int& health, int& max_health, int& magic, int& max_magic, int choice_ID, vector<holder>& pkit, potions *potion);
void Refresh_Equipment_Stats(int& cHea, int& cMHea, int& cMag, int& cMMag, int& cStr, int& cDef, int& cAgi, int bHea, int bMag, int bStr, int bDef, int bAgi, int aHea, int aDef, int aAgi, int sHea, int sDef, int sAgi, int mwMag, int mwStr, int mwAgi, int ofwMag, int ofwStr, int ofwAgi);
//----------------------------------------
void Rogue_Stats (int& max_health, int& health, int&max_magic, int& magic, int& strength, int& defense, int& agility);
void Barbarian_Stats (int& max_health, int& health, int&max_magic, int& magic, int& strength, int& defense, int& agility);
void Sorceress_Stats (int& max_health, int& health, int&max_magic, int& magic, int& strength, int& defense, int& agility);
//----------------------------------------
void Final_Boss (int& max_health, int& health, int&max_magic, int& magic, int& strength, int& defense, int& agility, int& level, int& potions, int& experience, int& itemID1, int& itemID1chance, int& itemID2, int& itemID2chance);
void Enemy_Stats_Level_1 (int& max_health, int& health, int&max_magic, int& magic, int& strength, int& defense, int& agility, int& level, int& potions, int& experience, int& itemID1, int& itemID1chance, int& itemID2, int& itemID2chance);
void Enemy_Stats_Level_2 (int& max_health, int& health, int&max_magic, int& magic, int& strength, int& defense, int& agility, int& level, int& potions, int& experience, int& itemID1, int& itemID1chance, int& itemID2, int& itemID2chance);
void Enemy_Stats_Level_3 (int& max_health, int& health, int&max_magic, int& magic, int& strength, int& defense, int& agility, int& level, int& potions, int& experience, int& itemID1, int& itemID1chance, int& itemID2, int& itemID2chance);
void Enemy_Stats_Level_4 (int& max_health, int& health, int&max_magic, int& magic, int& strength, int& defense, int& agility, int& level, int& potions, int& experience, int& itemID1, int& itemID1chance, int& itemID2, int& itemID2chance);
void Enemy_Stats_Level_5 (int& max_health, int& health, int&max_magic, int& magic, int& strength, int& defense, int& agility, int& level, int& potions, int& experience, int& itemID1, int& itemID1chance, int& itemID2, int& itemID2chance);
void Enemy_Stats_Level_6 (int& max_health, int& health, int&max_magic, int& magic, int& strength, int& defense, int& agility, int& level, int& potions, int& experience, int& itemID1, int& itemID1chance, int& itemID2, int& itemID2chance);
void Enemy_Stats_Level_7 (int& max_health, int& health, int&max_magic, int& magic, int& strength, int& defense, int& agility, int& level, int& potions, int& experience, int& itemID1, int& itemID1chance, int& itemID2, int& itemID2chance);
void Enemy_Stats_Level_8 (int& max_health, int& health, int&max_magic, int& magic, int& strength, int& defense, int& agility, int& level, int& potions, int& experience, int& itemID1, int& itemID1chance, int& itemID2, int& itemID2chance);
void Enemy_Stats_Level_9 (int& max_health, int& health, int&max_magic, int& magic, int& strength, int& defense, int& agility, int& level, int& potions, int& experience, int& itemID1, int& itemID1chance, int& itemID2, int& itemID2chance);
void Enemy_Stats_Level_10 (int& max_health, int& health, int&max_magic, int& magic, int& strength, int& defense, int& agility, int& level, int& potions, int& experience, int& itemID1, int& itemID1chance, int& itemID2, int& itemID2chance);

//--------------------------------------------------------------------------------thread Functions

// Thread Date Variables
int game_time_minutes = 0;
int game_time_minutes_max = 30;
int game_time_hours = 0;
int game_time_hours_max = 18;
int game_time_day = 1;
int game_time_day_max = 5;
int game_time_week = 1;
int game_time_week_max = 2;
int game_time_month = 1;
int game_time_month_max = 6;
int game_time_year = 1336;

//Daily mining points for mining in Luikach mine
int mining_points = 3;

//DateSystem thread - continuesly background counting numbers and sleeping
void DateSystem()
{
	do
	{
		Sleep (1000);
		game_time_minutes++;

		if (game_time_minutes >= game_time_minutes_max)
		{
			game_time_minutes = 0;
			game_time_hours++;
		}
		if (game_time_hours >= game_time_hours_max)
		{
			game_time_hours = 0;
			game_time_day++;

			mining_points = 3;		//Resetting daily mining points to 3
		}
		if (game_time_day > game_time_day_max)
		{
			game_time_day = 1;
			game_time_week++;
		}
		if (game_time_week > game_time_week_max)
		{
			game_time_week = 1;
			game_time_month++;
		}
		if (game_time_month > game_time_month_max)
		{
			game_time_month = 1;
			game_time_year++;
		}
	}while (TRUE);
}

//--------------------------------------------------------------------------------main () Function

int main()
{
	if (load())		// Load and check database for errors, unauthorized access or modifying of CN_S.dat file
	{
		exit (EXIT_FAILURE);	// If it was tempered, shut down application.
	}

	char gamemakerteam[] = "                   Maarten Craeynest & Marko Zanoski                   ";

	//----------------------------------------Potion Kit
	int player_potions = 0;
	int potionkit_free_space = 0;

	vector<string> potionkit_names(16,"        LOCKED   SPACE        ");
	vector<holder> potion_kit(5);

	potion_kit.reserve(16);
	holder_reset(potion_kit);
	
	//----------------------------------------Backpack
	int backpack_free_space = 0;
	vector<string> backpack_names(30,"        LOCKED   SPACE        ");

	vector<holder> backpack(15);
	backpack.reserve(30);

	holder_reset(backpack);

	//----------------------------------------
	int number_of_fur = 0;
	int number_of_hide = 0;
	
	int money_required = 0;
	int player_glass_marbles = 500;
	
	//----------------------------------------
	int total_player_encounter = 0;
	int current_player_encounter = 0;

	int total_number_of_travels = 0;
	int current_number_of_travels = 0;

	//----------------------------------------Base Player Stats
	string player_class_name = "NoClass";
	int player_base_max_health = 0;
	int player_base_health = 0;
	int player_base_max_magic = 0;
	int player_base_magic = 0;
	int player_base_strength = 0; 
	int player_base_defense = 0; 
	int player_base_agility = 0;
	//----
	double overall_player_level = 1;
	int overall_level_progress = 0;

	int player_combat_level = 1;
	int player_combat_experience = 0;

	int player_farming_level = 1;
	int player_farming_experience = 0;

	int player_mining_level = 1;
	int player_mining_experience = 0;

	int player_gambling_level = 1;
	int player_gambling_experience = 0;
	//----
	int showcase_required_combat = 0;
	int showcase_required_mining = 0;
	int showcase_required_farming = 0;
	int showcase_required_gambling = 0;

	//----------------------------------------Player Main Weapon Stats
	int special_weapon_available = 0;
	int special_weapon_equiped = 0;

	string player_main_weapon_stats_name = "None";
	int player_main_weapon_ID_carried = 999;
	int player_main_weapon_stats_magic = 0;
	int player_main_weapon_stats_agility = 0;
	int player_main_weapon_stats_strength = 0;

	//----------------------------------------Player Offhand Weapon Stats
	string player_offhand_weapon_stats_name = "None";
	int player_offhand_weapon_ID_carried = 999;
	int player_offhand_weapon_stats_magic = 0;
	int player_offhand_weapon_stats_agility = 0;
	int player_offhand_weapon_stats_strength = 0;

	//----------------------------------------Player Armor Stats
	string player_armor_stats_name = "None";
	int player_armor_ID_carried = 999;
	int player_armor_stats_health = 0;
	int player_armor_stats_defense = 0;
	int player_armor_stats_agility = 0;

	//----------------------------------------Player Shield Stats
	string player_shield_stats_name = "None";
	int player_shield_ID_carried = 999;
	int player_shield_stats_health = 0;
	int player_shield_stats_defense = 0;
	int player_shield_stats_agility = 0;

	//----------------------------------------Player Combined Stats (Player Base + Weapon + Armor)
	int player_combined_max_health = 0;
	int player_combined_health = 0;
	int player_combined_max_magic = 0;
	int player_combined_magic = 0;
	int player_combined_strength = 0; 
	int player_combined_defense = 0; 
	int player_combined_agility = 0;

	//----------------------------------------Base Enemy Stats
	string enemy_class_name = "NoClass";
	int enemy_base_max_health = 0;
	int enemy_base_health = 0;
	int enemy_base_max_magic = 0;
	int enemy_base_magic = 0;
	int enemy_base_strength = 0; 
	int enemy_base_defense = 0; 
	int enemy_base_agility = 0;
	
	int enemy_level = 1;
	int enemy_potions = 0;
	int enemy_experience = 0;
	int enemy_itemID1 = 999;
	int enemy_itemID1chance = 0;
	int enemy_itemID2 = 999;
	int enemy_itemID2chance = 0;

	//----------------------------------------Enemy Weapon Stats
	string enemy_weapon_stats_name = "None";
	int enemy_weapon_stats_magic = 0;
	int enemy_weapon_stats_agility = 0;
	int enemy_weapon_stats_strength = 0;

	//----------------------------------------Enemy Armor Stats
	string enemy_armor_stats_name = "None";
	int enemy_armor_stats_health = 0;
	int enemy_armor_stats_defense = 0;
	int enemy_armor_stats_agility = 0;

	//----------------------------------------Enemy Combined Stats (Enemy Base + Weapon + Armor)
	int enemy_combined_max_health = 0;
	int enemy_combined_health = 0;
	int enemy_combined_max_magic = 0;
	int enemy_combined_magic = 0;
	int enemy_combined_strength = 0; 
	int enemy_combined_defense = 0; 
	int enemy_combined_agility = 0;

	//----------------------------------------Damage Calculation
	double enemy_damage = 0;
	double player_damage = 0;
	double total_damage_output = 0;
	double total_damage_recived = 0;

	//----------------------------------------Player Turn Choice
	int player_escape = 0;
	int player_attack_super = 0;
	int player_attack_normal = 0;

	//----------------------------------------Enemy Turn Choice
	int enemy_escape = 0;
	int enemy_attack_super = 0;
	int enemy_attack_normal = 0;

	//----------------------------------------Turn Tracker
	int enemy_turn = 0;
	int player_turn = 0;
	int calculation_turn = 0;
	int tried_to_escape_turn = 0;
	int enemy_escape_successful = 0;
	int player_escape_successful = 0;
	int player_taunted = 0;
	int enemy_taunted = 0;

	//----------------------------------------Other Variables
	int i = 0;
	int j = 0;
	int h = 0;
	int error_proof = 0;
	int user_LOOP_choice = 0;
	int random_x = 0;
	int enemy_random_x = 0;
	int player_craft_one = 0;
	int player_craft_two = 0;
	int player_craft_three = 0;
	int recently_visit_town = 0; 
	char temp_char;

	//----------------------------------------Shop Tracker Variables
	size_t shopdex;
	int array_hit = 999;
	size_t id_checker = 0;
	int user_ID_choice = 0;
	int user_valid_number = 0;
	size_t id_end_merchant = 0;
	size_t id_start_merchant = 0;

	vector<string> shoperbag_names(15,"Error");
	vector<holder> shoperbag(15);
	holder_reset(shoperbag);

	//----------------------------------------Shop Item Variables From Database
	string item_name = "Error";
	string item_description = "Error";
	string item_location = "Error";
	string item_product1name = "Error";
	string item_product2name = "Error";
	string item_product3name = "Error";

	int item_id = 999;
	int item_buy = 0;
	int item_sell = 0;
	int item_level = 0;
	int item_health = 0;
	int item_magic = 0;
	int item_strength = 0;
	int item_defense = 0;
	int item_agility = 0; 

	//----------------------------------------Loops
	bool bExitLoop = false;
	bool bExitLoop2 = false;
	bool bExitLoop3 = false;
	bool bExitLoop4 = false;
	bool bExitLocation = false;

	//----------------------------------------Location Check up
	int location_checkup = 1;
	int location_town = 1;
	int location_camp = 0;
	int location_world = 0;
	int location_travel = 0;
	int location_battle = 0;
	int location_gameover = 0;

	//----------------------------------------Strings
	string statusofloot = "Error";
	string town_name;
	string UserChoice;
	string special_attack = "Error";

	//----------------------------------------Textual File Read
	ifstream nsk_file;
	string drawline;

	//----------------------------------------Gambling variables
	int new_card = 0;
	int current_card = 0;
	int bet_size = 0;
	int total_win = 0;
	int pool_money = 0;
	int game_rounds = 1;
	int keep_playing = 0;
	double round_bonus = 0;

	//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: Code Start

	SetWindow(80,57);
	srand ( static_cast < unsigned int > (time(NULL)) );

	thread Thread_Date_System (DateSystem);

	//:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:. Game Introduction

	cout << endl;
	cout << "                                  W A R N I N G                             " << endl;
	cout << "    ------------------------------------------------------------------------" << endl;
	cout << endl;

	nsk_file.open("CODE-Warning.nsk");

	if (!nsk_file.good())
	{
		cout << "    |ERROR 001 - Missing 'CODE-Warning.nsk' file." << endl;
		exit (EXIT_FAILURE);
	}

	while (getline(nsk_file,drawline))
	{
		code_string(drawline);
		cout << drawline << endl;
		Sleep(150);
	}

	nsk_file.close();


	cout << endl;
	cout << "    ------------------------------------------------------------------------" << endl;
	cout << "    |Press ENTER to continue..." << endl;
	cout << "    |";
	WaitForEnter();

	//:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.  Game Introduction END / Intro start

	ClearScreen();
	cout << endl;
	cout << "    ------------------------------------------------------------------------" << endl;
	cout << endl;
	cout << endl;

	//----------------------------------------

	nsk_file.open("CODE-Logo.nsk");

	if (!nsk_file.good())
	{
		cout << "    |ERROR 002 - Missing 'CODE-Logo.nsk' file." << endl;
		exit (EXIT_FAILURE);
	}

	while (getline(nsk_file,drawline))
	{
		code_string(drawline);
		cout << drawline << endl;
		Sleep(170);
	}

	nsk_file.close();
	
	//----------------------------------------

	cout << endl;
	cout << endl;
	cout << "                                GAME MADE BY:" << endl << endl;
	cout << "    ";

	for (i = 0 ; gamemakerteam[i] !='\0' ; ++i)
	{
		cout << gamemakerteam[i];
		Sleep(88);
	}

	cout << endl << endl;
	cout << setw(56) << "---------------------------------" << endl;
	cout << endl;
	cout << setw(20) << "CREDIT LIST:"			<< endl;
	cout << setw(38) << "ZAK LUDICK"			<< " |" << " Writer" << endl;
	cout << setw(38) << "JOSIP ZUKINA"			<< " |" << " Artist" << endl;
	cout << setw(38) << "MARKO ZANOSKI"			<< " |" << " Game Programmer" << endl;
	cout << setw(38) << "KATHARINA STEIN"		<< " |" << " Writer" << endl;
	cout << setw(38) << "MAARTEN CRAEYNEST"		<< " |" << " System Engineer" <<endl;
	cout << setw(38) << "---------------------"	<< " | ---------------------" << endl;
	cout << endl;
	cout << setw(20) << "ALPHA TESTERS:"		<< endl;
	cout << setw(38) << "NIKOLA BIRUS"			<< " |" << endl;
	cout << setw(38) << "IVAN BOSNIC"			<< " |" << endl;
	cout << setw(38) << "ENRRICO BUIC"			<< " |" << endl;
	cout << setw(38) << "FLORIS RODING"			<< " |" << endl;
	cout << setw(38) << "MATEA TOMIC"			<< " |" << endl;
	cout << setw(38) << "DINO VISNJIC"			<< " |" << endl;
	cout << endl;
	cout << endl;
	cout << "    ------------------------------------------------------------------------" << endl;
	cout << "    |Press ENTER to continue..." << endl;
	cout << "    |";
	WaitForEnter();

	//:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.  Intro END / Picking Character Class START

	do
	{
		ClearScreen();
		cout << endl;
		cout << "                                CHOOSE YOUR HERO" << endl;
		cout << "    ------------------------------------------------------------------------" << endl;

		//----------------------------------------

		nsk_file.open("CODE-HeroPick.nsk");

		if (!nsk_file.good())
		{
			cout << "    |ERROR 003 - Missing 'CODE-HeroPick.nsk' file." << endl;
			exit (EXIT_FAILURE);
		}
	
		while (getline(nsk_file,drawline))
		{
			code_string(drawline);
			cout << drawline << endl;
		}

		nsk_file.close();

		//----------------------------------------

		cout << "    ------------------------------------------------------------------------" << endl;
		cout << "         HEALTH  =   5/5   |     HEALTH  =   3/5   |     HEALTH  =   3/5    " << endl;
		cout << "          MAGIC  =   2/5   |      MAGIC  =   3/5   |      MAGIC  =   5/5    " << endl;
		cout << "       STRENGTH  =   2/5   |   STRENGTH  =   2/5   |   STRENGTH  =   1/5    " << endl;
		cout << "        DEFENSE  =   2/5   |    DEFENSE  =   1/5   |    DEFENSE  =   2/5    " << endl;
		cout << "        AGILITY  =   1/5   |    AGILITY  =   3/5   |    AGILITY  =   1/5    " << endl;
		cout << endl;
		cout << "       Merciless           |      Deadly           |      Innocent          " << endl;
		cout << "          Bloodthirsty     |         Precise       |         Hell Raiser    " << endl;
		cout << "    ------------------------------------------------------------------------" << endl;
		cout << "           BARBARIAN       |         ROGUE         |        SORCERESS       " << endl;
		cout << "    ------------------------------------------------------------------------" << endl;
		cout << "    |Your decision: ";

		getline(cin, UserChoice);
		transform(UserChoice.begin(), UserChoice.end(),UserChoice.begin(), toupper);

		//-------------------------------------

		if (UserChoice == "BARBARIAN" || UserChoice == "B")
		{
			special_attack = "RAGE";
			player_class_name = "Barbarian";
			Barbarian_Stats(player_base_max_health, player_base_health, player_base_max_magic, player_base_magic, player_base_strength, player_base_defense, player_base_agility);
			bExitLoop = true;
		}

		else if (UserChoice == "ROGUE" || UserChoice == "R")
		{
			special_attack = "FURY";
			player_class_name = "Rogue";
			Rogue_Stats(player_base_max_health, player_base_health, player_base_max_magic, player_base_magic, player_base_strength, player_base_defense, player_base_agility);
			bExitLoop = true;
		}

		else if (UserChoice == "SORCERESS" || UserChoice == "S")
		{
			special_attack = "CAST";
			player_class_name = "Sorceress";
			Sorceress_Stats(player_base_max_health, player_base_health, player_base_max_magic, player_base_magic, player_base_strength, player_base_defense, player_base_agility);
			bExitLoop = true;
		}

	}while (!bExitLoop);

	//:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.  Picking Character Class END / Background Character Story START

	ClearScreen();

	if (player_class_name == "Barbarian")
	{
		cout << endl;
		cout << "                           BARBARIAN BACKGROUND STORY                       " << endl;
		cout << "    ------------------------------------------------------------------------" << endl;
		cout << endl;

		nsk_file.open("CODE-BG-Barbarian.nsk");

		if (!nsk_file.good())
		{
			cout << "    |ERROR 004 - Missing 'CODE-BG-Barbarian.nsk' file." << endl;
			exit (EXIT_FAILURE);
		}
	
		while (getline(nsk_file,drawline))
		{
			code_string(drawline);
			cout << drawline << endl;
			Sleep(500);
		}

		nsk_file.close();
	}

	//----------------------------------------

	else if (player_class_name == "Sorceress")
	{
		cout << endl;
		cout << "                           SORCERESS BACKGROUND STORY                       " << endl;
		cout << "    ------------------------------------------------------------------------" << endl;
		cout << endl;

		nsk_file.open("CODE-BG-Sorceress.nsk");

		if (!nsk_file.good())
		{
			cout << "    |ERROR 005 - Missing 'CODE-BG-Sorceress.nsk' file." << endl;
			exit (EXIT_FAILURE);
		}
	
		while (getline(nsk_file,drawline))
		{
			code_string(drawline);
			cout << drawline << endl;
			Sleep(500);
		}

		nsk_file.close();
	}
	
	//----------------------------------------

	else if (player_class_name == "Rogue")
	{
		cout << endl;
		cout << "                             ROGUE BACKGROUND STORY                         " << endl;
		cout << "    ------------------------------------------------------------------------" << endl;
		cout << endl;

		nsk_file.open("CODE-BG-Rogue.nsk");

		if (!nsk_file.good())
		{
			cout << "    |ERROR 006 - Missing 'CODE-BG-Rogue.nsk' file." << endl;
			exit (EXIT_FAILURE);
		}
	
		while (getline(nsk_file,drawline))
		{
			code_string(drawline);
			cout << drawline << endl;
			Sleep(500);
		}

		nsk_file.close();
	}
	
	cout << endl;
	cout << "    ------------------------------------------------------------------------" << endl;
	cout << "    |Press ENTER to continue..." << endl;
	cout << "    |";
	WaitForEnter();

	//-------------------------------------Background Character Story END
	
	//-------------------------------------Interactive beginers tutorial END


	holder_add_item (backpack, 551);
	holder_add_item (backpack, 551);
	holder_add_item (backpack, 551);


	do	// MOST IMPORTANT LOOP - Loop That Checks Player Location
	{

		while (location_town == 1)	//----------------------------------------Player Location: TOWN {} START
		{	
			location_town = 0;
			bExitLocation = false;
			recently_visit_town = 0;
			current_player_encounter = 0;
			current_number_of_travels = 0;
			
			Random_Number(random_x);

			if (random_x <= 10)
			{
				town_name = "Angalash";
			}

			else if (random_x <= 20)
			{
				town_name = "Berchius";
			}

			else if (random_x <= 30)
			{
				town_name = "Coristar";
			}

			else if (random_x <= 40)
			{
				town_name = "Erriolaf";
			}

			else if (random_x <= 50)
			{
				town_name = "Gothlina";
			}

			else if (random_x <= 60)
			{
				town_name = "Kevivico";
			}

			else if (random_x <= 70)
			{
				town_name = "Lingalah";
			}

			else if (random_x <= 80)
			{
				town_name = "Meadface";
			}

			else if (random_x <= 90)
			{
				town_name = "Riscuala";
			}

			else
			{
				town_name = "Xerikkes";
			}

			//-------------------------------------Do-While Loop While Player is still in town - IN SAME TOWN.
			do
			{	//-------------------------------------bExitLocation {} Start

				Refresh_Equipment_Stats(player_combined_health, player_combined_max_health, player_combined_magic, player_combined_max_magic, player_combined_strength, player_combined_defense, player_combined_agility, player_base_health, player_base_magic, player_base_strength, player_base_defense, player_base_agility, player_armor_stats_health, player_armor_stats_defense, player_armor_stats_agility, player_shield_stats_health, player_shield_stats_defense, player_shield_stats_agility,player_main_weapon_stats_magic, player_main_weapon_stats_strength, player_main_weapon_stats_agility, player_offhand_weapon_stats_magic, player_offhand_weapon_stats_strength, player_offhand_weapon_stats_agility);
				//FIXMe This is a problematic function - keeps refil palyer health on new town exit
				//----------------------------------------

				ClearScreen();
				cout << endl;
				cout << "    TOWN" << endl;
				cout << "    ------------------------------------------------------------------------" << endl;
				cout << endl;
				cout << "     You have arrived in Town " << town_name << "." << endl;
				cout << endl;
				cout << "     Walking through town and wondering what to do next..." << endl;
				cout << endl;
				cout << endl;
				cout << "    ------------------------------------------------------------------------" << endl;
				cout << "                   INN  |  MARKETPLACE  |  JOURNEY  |  ACCOUNT              " << endl;
				cout << "    ------------------------------------------------------------------------" << endl;
				cout << "    |Your decision: ";

				getline(cin, UserChoice);
				transform(UserChoice.begin(), UserChoice.end(),UserChoice.begin(), toupper);

				//----------------------------------------

				ClearScreen();

				if (UserChoice == "INN" || UserChoice == "I")
				{
					bExitLoop = false;
					do
					{
						ClearScreen();
						cout << endl;
						cout << "    TOWN -> INN" << endl;
						cout << "    ------------------------------------------------------------------------" << endl;
						cout << endl;
						cout << "     Entering the inn..." << endl;
						cout << endl;
						cout << "     Jolly music and full of people...what would you like to do?" << endl;
						cout << endl;
						cout << endl;
						cout << "                          Available: "<< player_glass_marbles << " [Glass Marbles]" << endl;
						cout << "    ------------------------------------------------------------------------" << endl;
						cout << "                            EXIT  |  REST  |  GAMBLE                        " << endl;
						cout << "    ------------------------------------------------------------------------" << endl;
						cout << "    |Your decision: ";

						getline(cin, UserChoice);
						transform(UserChoice.begin(), UserChoice.end(),UserChoice.begin(), toupper);

						//----------------------------------------

						ClearScreen();

						if (UserChoice == "EXIT" || UserChoice == "E")
						{
							bExitLoop = true;
						}

						else if (UserChoice == "REST" || UserChoice == "R")
						{
							bExitLoop2 = false;

							do
							{
								ClearScreen();
								cout << endl;
								cout << "    TOWN -> INN -> RESTING" << endl;
								cout << "    ------------------------------------------------------------------------" << endl;
								cout << endl;
								cout << "     Inn keeper grunts, and asks you how long would you like to rest ?" << endl;
								cout << endl;
								cout << "     Price Menu:" << endl;
								cout << "                 2 Hour Rest -  6 Glass Marbles" << endl;
								cout << "                 3 Hour Rest -  9 Glass Marbles" << endl;
								cout << "                 4 Hour Rest - 12 Glass Marbles" << endl;
								cout << "                 5 Hour Rest - 15 Glass Marbles" << endl;
								cout << "                 6 Hour Rest - 18 Glass Marbles" << endl;
								cout << "                 7 Hour Rest - 21 Glass Marbles" << endl;
								cout << "                 8 Hour Rest - 24 Glass Marbles" << endl;
								cout << endl;
								cout << endl;
								cout << "      Time: " << setw (2) << game_time_hours << ":" << setw (2) << game_time_minutes << endl;
								cout << endl;
								cout << "       Day: " << game_time_day << endl;
								cout << "      Week: " << game_time_week << endl;
								cout << "     Month: " << game_time_month << endl;
								cout << "      Year: " << game_time_year << endl;
								cout << endl;
								cout << endl;
								cout << "                          Available: "<< player_glass_marbles << " [Glass Marbles]" << endl;
								cout << setw (27) << "HEALTH: " << setw(3) << player_combined_health << "/" << setw(3) << player_combined_max_health << setw(20) << "MAGIC: " << setw(3) << player_combined_magic << "/" << setw(3) << player_combined_max_magic << endl;
								cout << "    ------------------------------------------------------------------------" << endl;
								cout << "                                  (HOURS)  |  EXIT                             " << endl;
								cout << "    ------------------------------------------------------------------------" << endl;
								cout << "    |Your decision: ";

								getline(cin, UserChoice);
								user_valid_number = is_number(UserChoice);
								user_ID_choice = atoi(UserChoice.c_str());
								transform(UserChoice.begin(), UserChoice.end(),UserChoice.begin(), toupper);

								//-------------------------------------

								ClearScreen();

								if (UserChoice == "EXIT" || UserChoice == "E")
								{
									bExitLoop2 = true;
								}

								else if (user_ID_choice >= 2 && user_ID_choice <= 8 )
								{
									if (player_glass_marbles >= ((user_ID_choice - 2) * 3) + 6)
									{
										player_glass_marbles -= ((user_ID_choice - 2) * 3) + 6;

										//FIXME - dodaj restoration hp and magic

										if (game_time_hours + user_ID_choice > 18)
										{
											game_time_day += 1;
											game_time_hours = game_time_hours + user_ID_choice - 18;
										}

										else
										{
											game_time_hours = game_time_hours + user_ID_choice;
										}

										ClearScreen();
										cout << endl;
										cout << "    TOWN -> INN -> RESTING" << endl;
										cout << "    ------------------------------------------------------------------------" << endl;
										cout << endl;
										cout << "     You have rested well for " << user_ID_choice << " hours." << endl;
										cout << endl;
										//FIXME cout << "     You have restored Health and Magic" << user_ID_choice << " hours." << endl;
										cout << endl;
										cout << endl;
										cout << "    ------------------------------------------------------------------------" << endl;
										cout << "    |Press ENTER to continue..." << endl;
										cout << "    |";
										WaitForEnter();
									}	//--------------------------------------------------------(player_glass_marbles >= ((user_ID_choice - 2) * 3) + 6) {} END
									
									else
									{
										ClearScreen();
										cout << endl;
										cout << "                                     FIZZLE" << endl;
										cout << "    ------------------------------------------------------------------------" << endl;
										cout << endl;
										cout << "     Not enough money to afford a room." << endl;
										cout << endl;
										cout << "    ------------------------------------------------------------------------" << endl;
										cout << "    |Press ENTER to continue..." << endl;
										cout << "    |";
										WaitForEnter();
									}

								}

							}while (!bExitLoop2);	// do-while "GAMBLING" Loop End
						}

						else if (UserChoice == "GAMBLE" || UserChoice == "G")
						{
							bExitLoop2 = false;

							do
							{
								ClearScreen();
								cout << endl;
								cout << "    TOWN -> INN -> GAMBLING" << endl;
								cout << "    ------------------------------------------------------------------------" << endl;
								cout << endl;
								cout << "     Entering the back room of Inn." << endl;
								cout << endl;
								cout << "     Serious guys with cards in hands... what game do you want to play ?" << endl;
								cout << endl;
								cout << "    " << setw(36) << "High Low Card Game" << endl;
								cout << endl;
								cout << endl;
								cout << "                          Available: "<< player_glass_marbles << " [Glass Marbles]" << endl;
								cout << "    ------------------------------------------------------------------------" << endl;
								cout << "                                  EXIT  |  HILO                             " << endl;
								cout << "    ------------------------------------------------------------------------" << endl;
								cout << "    |Your decision: ";
	
								getline(cin, UserChoice);
								transform(UserChoice.begin(), UserChoice.end(),UserChoice.begin(), toupper);

								//-------------------------------------

								ClearScreen();

								if (UserChoice == "EXIT" || UserChoice == "E")
								{
									bExitLoop2 = true;
								}

								else if (UserChoice == "HILO" || UserChoice == "H")
								{																	// HILO Game {} START
									bExitLoop3 = false;
									do
									{
										ClearScreen();
										cout << endl;
										cout << "    TOWN -> INN -> GAMBLING -> HILO Game                                    " << endl;
										cout << "    ------------------------------------------------------------------------" << endl;
										cout << endl;
										cout << "     Welcome to High Low Game table.                                        " << endl;
										cout << endl;
										cout << "     Game Rules are simple:                                                 " << endl;
										cout << endl;
										cout << "               1. ENTER A BET." << endl;
										cout << "                 -> Minimal bet is 10 Glass Marbles to enter the game.      " << endl;
										cout << "                 -> The higher the stake, the greater the bonus per round.  " << endl;
										cout << endl;
										cout << "               2. YOU WILL BE DELT WITH A SINGLE CARD.                      " << endl;
										cout << endl;
										cout << "               3. MAKE THE CALL.                                            " << endl;
										cout << "                 -> After receiving the first card, you have to say if the  " << endl;
										cout << "                    following card will be higher or lower in value. If     " << endl;
										cout << "                    your prediction is right, you win!" << endl;
										cout << endl;
										cout << "               4. ....?" << endl;
										cout << endl;
										cout << "               5. REPEAT FOR PROFIT." << endl;
										cout << endl;
										cout << "               NOTE - If you receive the same card, you lose immediately.    " << endl;
										cout << endl;
										cout << endl;
										cout << "    ------------------------------------------------------------------------" << endl;
										cout << "                                  BET  |  EXIT                              " << endl;
										cout << "    ------------------------------------------------------------------------" << endl;
										cout << "    |Your decision: ";

										getline(cin, UserChoice);
										transform(UserChoice.begin(), UserChoice.end(),UserChoice.begin(), toupper);

										//-------------------------------------

										ClearScreen();
										
										if (UserChoice == "EXIT" || UserChoice == "E")
										{
											bExitLoop3 = true;
										}

										else if (UserChoice == "BET" || UserChoice == "B")
										{
											bet_size = 0;
											total_win = 0;
											pool_money = 0;
											game_rounds = 1;

											//----------------------------------------

											cout << endl;
											cout << "                                     DEALER                                 " << endl;
											cout << "    ------------------------------------------------------------------------" << endl;
											cout << endl;
											cout << "     Type in your initial bet to play the game.                             " << endl;
											cout << endl;
											cout << "     Glass Marbles: " << player_glass_marbles << endl;
											cout << "     Remember, bigger the amount - bigger the reward.                       " << endl;
											cout << endl;
											cout << endl;
											cout << "    ------------------------------------------------------------------------" << endl;
											cout << "    |Your decision: ";

											getline(cin, UserChoice);
											user_valid_number = is_number(UserChoice);
											bet_size = atoi(UserChoice.c_str());
											transform(UserChoice.begin(), UserChoice.end(),UserChoice.begin(), toupper);
											
											//-------------------------------------

											ClearScreen();

											if (user_valid_number == 1)
											{
												if (bet_size >= 10)
												{
													if (player_glass_marbles >= bet_size)
													{
														bExitLoop4 = false;
														player_glass_marbles -= bet_size;
														Gambling_Level_Checkup (bet_size/2, player_gambling_level, player_gambling_experience);

														do
														{
															current_card = rand() % 13;

														} while (current_card == 0 || current_card == 12);

														new_card = rand() % 13;

														do
														{
															ClearScreen();
															cout << endl;
															cout << "                                     DEALER                                 " << endl;
															cout << "    ------------------------------------------------------------------------" << endl;
															cout << endl;
															cout << "                                   FIRST CARD                               " << endl;
															cout << "                                   __________                               " << endl;
															cout << "                                  |          |                              " << endl;
															cout << "                                  |          |                              " << endl;
															cout << "                                  |          |                              " << endl;
															cout << "                                  |    " << setw(2) << cards[current_card].Cardvalue << "    |                              " << endl;
															cout << "                                  |          |                              " << endl;
															cout << "                                  |          |                              " << endl;
															cout << "                                  |          |                              " << endl;
															cout << "                                   ~~~~~~~~~~                            " << endl;
															cout << endl;
															cout << "                         1-2-3-4-5-6-7-8-9-10-11-12-13                      " << endl;
															cout << endl;
															cout << endl;
															cout << "    ------------------------------------------------------------------------" << endl;
															cout << "                              HIGHER    |   LOWER" << endl;
															cout << "    ------------------------------------------------------------------------" << endl;
															cout << "    |Your decision: ";

															getline(cin, UserChoice);
															transform(UserChoice.begin(), UserChoice.end(),UserChoice.begin(), toupper);
															
															//----------------------------------------

															if (UserChoice == "HIGHER" || UserChoice == "H" || UserChoice == "LOWER" || UserChoice == "L")
															{
																bExitLoop4 = true;

																cout << endl << endl << endl << endl << endl << endl;
																cout << "                                   NEXT  CARD                               " << endl;
																cout << "                                   __________                               " << endl;
																cout << "                                  |          |                              " << endl;
																cout << "                                  |          |                              " << endl;
																cout << "                                  |          |                              " << endl;
																cout << "                                  |    " << setw(2) << cards[new_card].Cardvalue << "    |                              " << endl;
																cout << "                                  |          |                              " << endl;
																cout << "                                  |          |                              " << endl;
																cout << "                                  |          |                              " << endl;
																cout << "                                   ~~~~~~~~~~                            " << endl;
																cout << endl;
																cout << "                                 Your guess is:                             " << endl;
																cout << "                                     ";

																if ( (UserChoice == "HIGHER") && (new_card > current_card) || (UserChoice == "H") && (new_card > current_card) )
																{
																	keep_playing = 1;
																	pool_money = cards[current_card].Higher_bonus + bet_size;
																	Gambling_Level_Checkup (20, player_gambling_level, player_gambling_experience);
																	
																	cout << "Valid!" << endl << endl;
																}

																else if ( (UserChoice == "LOWER") && (new_card < current_card) || (UserChoice == "L") && (new_card < current_card) ) 
																{
																	keep_playing = 1;
																	pool_money = cards[current_card].Lower_bonus + bet_size;
																	Gambling_Level_Checkup (20, player_gambling_level, player_gambling_experience);
																	
																	cout << "Valid!" << endl << endl;	
																}

																else
																{
																	Gambling_Level_Checkup (10, player_gambling_level, player_gambling_experience);
																	
																	cout << "False!" << endl << endl;
																	cout << "                                   GAME  LOST                               " << endl;
																	cout << "    ------------------------------------------------------------------------" << endl;
																	cout << "    |Press ENTER to continue..." << endl;
																	cout << "    |";
																	WaitForEnter();
																	break;
																}

																//----------------------------------------

																cout << "    ------------------------------------------------------------------------" << endl;
																cout << "    |Press ENTER to continue..." << endl;
																cout << "    |";
																WaitForEnter();
															}

														}while(!bExitLoop4);

														bExitLoop4 = false;

														while (keep_playing == 1)
														{
															round_bonus = floor(((static_cast<double> (bet_size) - 5) / 5) * game_rounds + 0.5);
															total_win = pool_money + static_cast<int> (round_bonus);
															current_card = new_card;

															while (current_card == 0 || current_card == 12)
															{
																ClearScreen();
																cout << endl;
																cout << "                                     DEALER                                 " << endl;
																cout << "    ------------------------------------------------------------------------" << endl;
																cout << "     Sorry, the King and the Ace are not valid cards for new round. " << endl;
																cout << "     a new card will be dealt to you." << endl;

																do
																{

																	current_card = rand() % 13;

																}while (current_card == 0 || current_card == 12);

																cout << endl;
																cout << endl;
																cout << "    ------------------------------------------------------------------------" << endl;
																cout << "    |Press ENTER to continue..." << endl;
																cout << "    |";
																WaitForEnter();
															}

															new_card = rand() % 13;

															do
															{
																ClearScreen();
																cout << endl;
																cout << "                                     DEALER                                 " << endl;
																cout << "    ------------------------------------------------------------------------" << endl;
																cout << endl;
																cout << endl;
																cout << "                                      CARD                                  " << endl;
																cout << "                                   __________                               " << endl;
																cout << "                                  |          |                              " << endl;
																cout << "                                  |          |                              " << endl;
																cout << "                                  |          |                              " << endl;
																cout << "                                  |    " << setw(2) << cards[current_card].Cardvalue << "    |                              " << endl;
																cout << "                                  |          |                              " << endl;
																cout << "                                  |          |                              " << endl;
																cout << "                                  |          |                              " << endl;
																cout << "                                   ~~~~~~~~~~                            " << endl;
																cout << endl;
																cout << "                         1-2-3-4-5-6-7-8-9-10-11-12-13                      " << endl;
																cout << endl;
																cout << endl;
																cout << "    Current POT: " << pool_money << endl;
																cout << "    Bonus for playing " << game_rounds << " round(s) = " << round_bonus << endl;
																cout << "    Total Payout on Collect: " << total_win << endl;
																cout << endl;
																cout << endl;
																cout << "    ------------------------------------------------------------------------" << endl;
																cout << "                          COLLECT  |  HIGHER  |  LOWER                      " << endl;
																cout << "    ------------------------------------------------------------------------" << endl;
																cout << "    |Your decision: ";
																														
																getline(cin, UserChoice);
																transform(UserChoice.begin(), UserChoice.end(),UserChoice.begin(), toupper);

																if (UserChoice == "COLLECT" || UserChoice == "C")
																{
																	ClearScreen();
																	bExitLoop4 = true;
																	keep_playing = 0;
																	player_glass_marbles += total_win;

																	cout << endl;
																	cout << endl;
																	cout << "                                     PAYOUT                                 " << endl;
																	cout << "    ------------------------------------------------------------------------" << endl;
																	cout << endl;
																	cout << "     Congratulations ! " << endl;
																	cout << "     You have played " << game_rounds << " rounds." << endl;
																	cout << "     You have won " << total_win << " Glass Marbles." << endl;
																	cout << endl;
																	cout << "    ------------------------------------------------------------------------" << endl;
																	cout << "    |Press ENTER to continue..." << endl;
																	cout << "    |";
																	WaitForEnter();

																}

																else if (UserChoice == "HIGHER" || UserChoice == "H" || UserChoice == "LOWER" ||UserChoice == "L")
																{
																	bExitLoop4 = true;

																	cout << endl << endl << endl << endl << endl << endl;
																	cout << "                                   NEXT  CARD                               " << endl;
																	cout << "                                   __________                               " << endl;
																	cout << "                                  |          |                              " << endl;
																	cout << "                                  |          |                              " << endl;
																	cout << "                                  |          |                              " << endl;
																	cout << "                                  |    " << setw(2) << cards[new_card].Cardvalue << "    |                              " << endl;
																	cout << "                                  |          |                              " << endl;
																	cout << "                                  |          |                              " << endl;
																	cout << "                                  |          |                              " << endl;
																	cout << "                                   ~~~~~~~~~~                            " << endl;
																	cout << endl;
																	cout << "                                 Your guess is:                             " << endl;
																	cout << "                                     ";

																	if ( (UserChoice == "HIGHER") && (new_card > current_card) || (UserChoice == "H") && (new_card > current_card) )
																	{
																		keep_playing = 1;
																		game_rounds += 1;
																		pool_money += cards[current_card].Higher_bonus;
																		Gambling_Level_Checkup (20, player_gambling_level, player_gambling_experience);
																		
																		cout << "Valid!" << endl << endl;
																	}

																	else if ( (UserChoice == "LOWER") && (new_card < current_card) || (UserChoice == "L") && (new_card < current_card) ) 
																	{
																		keep_playing = 1;
																		game_rounds += 1;
																		pool_money += cards[current_card].Lower_bonus;
																		Gambling_Level_Checkup (20, player_gambling_level, player_gambling_experience);
																		
																		cout << "Valid!" << endl << endl;
																	}

																	else
																	{
																		keep_playing = 0;
																		Gambling_Level_Checkup (10, player_gambling_level, player_gambling_experience);
																		
																		cout << "False!" << endl << endl;
																		cout << "                                   GAME  LOST                               " << endl;
																		cout << "    ------------------------------------------------------------------------" << endl;
																		cout << "    |Press ENTER to continue..." << endl;
																		cout << "    |";
																		WaitForEnter();
																		break; // Safe exit tested
																	}

																	//----------------------------------------

																	cout << "    ------------------------------------------------------------------------" << endl;
																	cout << "    |Press ENTER to continue..." << endl;
																	cout << "    |";
																	WaitForEnter();
																}

															}while(!bExitLoop4);	// do-while keep playing for bad commands
														}	// keep playing end
													}

													else
													{
														cout << endl;
														cout << "                                     FIZZLE" << endl;
														cout << "    ------------------------------------------------------------------------" << endl;
														cout << endl;
														cout << "     Your offer was above your financial limit." << endl;
														cout << endl;
														cout << "    ------------------------------------------------------------------------" << endl;
														cout << "    |Press ENTER to continue..." << endl;
														cout << "    |";
														WaitForEnter();
													}
												}

												else
												{
													cout << endl;
													cout << "                                     FIZZLE" << endl;
													cout << "    ------------------------------------------------------------------------" << endl;
													cout << endl;
													cout << "     Your offer was under the table limit." << endl;
													cout << endl;
													cout << "    ------------------------------------------------------------------------" << endl;
													cout << "    |Press ENTER to continue..." << endl;
													cout << "    |";
													WaitForEnter();
												}
											}

											else
											{
												cout << endl;
												cout << "                                     FIZZLE" << endl;
												cout << "    ------------------------------------------------------------------------" << endl;
												cout << endl;
												cout << "     Your offer was confusing. Please make a valid numerical offer." << endl;
												cout << endl;
												cout << "    ------------------------------------------------------------------------" << endl;
												cout << "    |Press ENTER to continue..." << endl;
												cout << "    |";
												WaitForEnter();
											}

										}

									}while(!bExitLoop3);	// do-while "HILO" Loop End

								}	// else if (UserChoice == "HILO" || UserChoice == "H") {} {} END

							}while (!bExitLoop2);	// do-while "GAMBLING" Loop End

						}	// else if (UserChoice == "GAMBLE" || UserChoice == "G") {} End

					}while (!bExitLoop);	// do-while "INN" Loop End

				}	// if (UserChoice == "INN" || UserChoice == "I") {} End

				//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

				else if (UserChoice == "MARKETPLACE" || UserChoice == "M")
				{
					bExitLoop = false;

					do
					{
						ClearScreen();
						cout << endl;
						cout << "    TOWN -> MARKETPLACE" << endl;
						cout << "    ------------------------------------------------------------------------" << endl;
						cout << endl;
						cout << "     Entering the marketplace..." << endl;
						cout << endl;
						cout << "     Section I am looking for is:" << endl;
						cout << endl;
						cout << endl;
						cout << "    " << setw(42) << "ARMORSMITH" << endl;
						cout << endl;
						cout << "    " << setw(42) << "CRAFTSMAN" << endl;
						cout << endl;
						cout << "    " << setw(42) << "MERCHANT" << endl;
						cout << endl;
						cout << "    " << setw(42) << "PHARMACIST" << endl;
						cout << endl;
						cout << "    " << setw(42) << "SKINNER" << endl;
						cout << endl; 
						cout << "    " << setw(42) << "WEAPONSMITH" << endl;
						cout << endl;
						cout << endl;
						cout << "                          Available: "<< player_glass_marbles << " [Glass Marbles]" << endl;
						cout << "    ------------------------------------------------------------------------" << endl;
						cout << "                                      EXIT                                  " << endl;
						cout << "    ------------------------------------------------------------------------" << endl;
						cout << "    |Your decision: ";

						getline(cin, UserChoice);
						transform(UserChoice.begin(), UserChoice.end(),UserChoice.begin(), toupper);

						//----------------------------------------

						ClearScreen();

						if (UserChoice == "EXIT" || UserChoice == "E")
						{
							bExitLoop = true;
						}

						else if (UserChoice == "MERCHANT" || UserChoice == "M" )
						{
							bExitLoop2 = false;

							while (recently_visit_town == 0)
							{
								recently_visit_town = 1;
								holder_reset(shoperbag);

								do
								{
									
									Random_Number(random_x);

								}while (random_x >= 35);
								
								id_start_merchant = 500 + random_x;
								id_end_merchant = id_start_merchant + 15;

								for (i = 0 ; i < 15 ; ++i)
								{
									do
									{
										
										Random_Number(random_x);
										random_x += 500;

									} while(random_x <= (int) id_start_merchant || random_x >= (int) id_end_merchant);

									holder_add_item(shoperbag,random_x);
								}
							}

							combine_rucksack (backpack, backpack_names, armor, weapon, item);
							combine_rucksack (shoperbag, shoperbag_names, armor, weapon, item);

							do
							{
								ClearScreen();
								cout << endl;
								cout << "    TOWN -> MARKETPLACE -> MERCHANT" << endl;
								cout << "    ------------------------------------------------------------------------" << endl;
								cout << endl;
								cout << "    Entering the Merchant Store..." << endl;
								cout << endl;
								cout << "    Would you like to BUY items or SELL them ?" << endl;
								cout << endl;
								cout << "    Following Items are available:" << endl;
								cout << endl;
								cout << endl;
								cout << "                          Available: "<< player_glass_marbles << " [Glass Marbles]" << endl;
								cout << "    ------------------------------------------------------------------------" << endl;
								cout << "                             EXIT  |  BUY  |  SELL                          " << endl;
								cout << "    ------------------------------------------------------------------------" << endl;
								cout << "    |Your decision: ";

								getline(cin, UserChoice);
								transform(UserChoice.begin(), UserChoice.end(),UserChoice.begin(), toupper);
										
								//-------------------------------------
								
								ClearScreen();

								if (UserChoice == "EXIT" || UserChoice == "E")
								{
									bExitLoop2 = true;
								}

								else if (UserChoice == "BUY" || UserChoice == "B")
								{
									bExitLoop3 = false;
									
									do
									{
										ClearScreen();
										cout << endl;
										cout << "    TOWN -> MARKETPLACE -> MERCHANT -> BUYING" << endl;
										cout << "    ------------------------------------------------------------------------" << endl;
										cout << endl;
										cout << "    Entering the Merchant Store..." << endl;
										cout << endl;
										cout << "    Type in an IDN of item You want to buy..." << endl;
										cout << endl;
										cout << "    Following Items are available:" << endl;
										cout << endl;
										cout << endl;
										cout << "    ------------------------------------------------------------------------" << endl;
										cout << "    |IDN|                     NAME                     |   BUYING  PRICE   |" << endl;
										cout << "    ------------------------------------------------------------------------" << endl;

										for (i = 0 ; i < (int) shoperbag.size() ; ++i)
										{
											if (shoperbag[i].ID == 999)
											{
												break;
											}

											else
											{
												cout << "    |"	 << setw(3) << i << "|"
													 << setw(31) << shoperbag[i].Name
													 << setw(16) << "|" 
													 << setw(11) << shoperbag[i].Value
													 << setw(9)	 << "|" << endl;
											}
										}

										cout << "    ------------------------------------------------------------------------" << endl;
										cout << endl;
										cout << endl;
										cout << endl;
										cout << endl;
										cout << "                          Available: "<< player_glass_marbles << " [Glass Marbles]" << endl;
										cout << "    ------------------------------------------------------------------------" << endl;
										cout << "                                  (IDN)  |  EXIT                             " << endl;
										cout << "    ------------------------------------------------------------------------" << endl;
										cout << "    |Your decision: ";

										getline(cin, UserChoice);
										user_valid_number = is_number(UserChoice);
										user_ID_choice = atoi(UserChoice.c_str());
										transform(UserChoice.begin(), UserChoice.end(),UserChoice.begin(), toupper);

										//----------------------------------------

										ClearScreen();

										if (UserChoice == "EXIT" || UserChoice == "E")
										{
											bExitLoop3 = true;
										}

										else if (user_valid_number == 1)
										{
											if (user_ID_choice < i)
											{
												holder_space_counter (backpack,backpack_free_space);

												item_id = shoperbag[user_ID_choice].ID;
												item_name = shoperbag[user_ID_choice].Name;
												item_buy = shoperbag[user_ID_choice].Value;

												if (backpack_free_space >= 1)
												{
													if (player_glass_marbles >= item_buy)
													{
														holder_add_item (backpack, item_id);
														holder_remove_item (shoperbag, item_id);
														player_glass_marbles -= item_buy; 	

														bExitLoop3 = true;

														cout << endl;
														cout << "                                   PURCHASING" << endl;
														cout << "    ------------------------------------------------------------------------" << endl;
														cout << endl;
														cout << "     Successfully bought: " << item_name << endl;
														cout << endl;
														cout << "     Cost: " << item_buy << endl;
														cout << endl;
														cout << "     Glass Marbles: " << player_glass_marbles << endl;
														cout << endl;
														cout << "    ------------------------------------------------------------------------" << endl;
														cout << "    |Press ENTER to continue..." << endl;
														cout << "    |";
														WaitForEnter();
													} // (player_glass_marbles >= item_buy) {} END

													else
													{
														bExitLoop3 = true;
														money_required = (item_buy - player_glass_marbles);

														cout << endl;
														cout << "                                     FIZZLE" << endl;
														cout << "    ------------------------------------------------------------------------" << endl;
														cout << endl;
														cout << "     You do not have enough Glass Marbles to purchase this item." << endl;
														cout << "     You need " << money_required << " more Glass Marbles." << endl;
														cout << endl;
														cout << "    ------------------------------------------------------------------------" << endl;
														cout << "    |Press ENTER to continue..." << endl;
														cout << "    |";
														WaitForEnter();
													}

												} // (backpack_free_space >= 1) {} END

												else
												{
													cout << endl;
													cout << "                                     FIZZLE" << endl;
													cout << "    ------------------------------------------------------------------------" << endl;
													cout << endl;
													cout << "     No free space in backpack." << endl;
													cout << endl;
													cout << "    ------------------------------------------------------------------------" << endl;
													cout << "    |Press ENTER to continue..." << endl;
													cout << "    |";
													WaitForEnter();
												}

											}

											else if (user_ID_choice < (int) backpack.size())
											{
												cout << endl;
												cout << "                                     FIZZLE" << endl;
												cout << "    ------------------------------------------------------------------------" << endl;
												cout << endl;
												cout << "     Merchant backpack does not have item in this backpack slot." << endl;
												cout << endl;
												cout << "    ------------------------------------------------------------------------" << endl;
												cout << "    |Press ENTER to continue..." << endl;
												cout << "    |";
												WaitForEnter();
											}

											else if (user_ID_choice >= (int) backpack.size())
											{
												cout << endl;
												cout << "                                     FIZZLE" << endl;
												cout << "    ------------------------------------------------------------------------" << endl;
												cout << endl;
												cout << "     Merchant backpack does not have so much slots." << endl;
												cout << endl;
												cout << "    ------------------------------------------------------------------------" << endl;
												cout << "    |Press ENTER to continue..." << endl;
												cout << "    |";
												WaitForEnter();
											}

										}	// Valid Number Checker  {} END

										else
										{
											cout << endl;
											cout << "                                     FIZZLE" << endl;
											cout << "    ------------------------------------------------------------------------" << endl;
											cout << endl;
											cout << "     Please type in the correct IDN of Item." << endl;
											cout << endl;
											cout << "    ------------------------------------------------------------------------" << endl;
											cout << "    |Press ENTER to continue..." << endl;
											cout << "    |";
											WaitForEnter();
										}
										
									}while(!bExitLoop3);	// do-while MERCHANT BUY Loop - {} END
									
								}
								
								else if (UserChoice == "SELL" || UserChoice == "S")
								{
									bExitLoop3 = false;
									
									do
									{
										ClearScreen();
										cout << endl;
										cout << "    TOWN -> MARKETPLACE -> MERCHANT -> SELLING" << endl;
										cout << "    ------------------------------------------------------------------------" << endl;
										cout << endl;
										cout << "    Entering the Merchant Store..." << endl;
										cout << endl;
										cout << "    Type in an IDN of item You want to sell..." << endl;
										cout << endl;
										cout << "    Following Items are available:" << endl;
										cout << endl;
										cout << endl;
										cout << "    ------------------------------------------------------------------------" << endl;
										cout << "    |IDN|                     NAME                     |   SELLING PRICE   |" << endl;
										cout << "    ------------------------------------------------------------------------" << endl;

										for (i = 0 ; i < (int) backpack.size() ; ++i)
										{
											if (backpack[i].ID == 999)
											{
												break;
											}

											else
											{
												cout << "    |"	 << setw(3) << i << "|"
													 << setw(31) << backpack[i].Name
													 << setw(16) << "|" 
													 << setw(11) << backpack[i].Value
													 << setw(9)	 << "|" << endl;
											}
										}

										cout << "    ------------------------------------------------------------------------" << endl;
										cout << endl;
										cout << endl;
										cout << endl;
										cout << endl;
										cout << "                          Available: "<< player_glass_marbles << " [Glass Marbles]" << endl;
										cout << "    ------------------------------------------------------------------------" << endl;
										cout << "                                  (IDN)  |  EXIT                             " << endl;
										cout << "    ------------------------------------------------------------------------" << endl;
										cout << "    |Your decision: ";

										getline(cin, UserChoice);
										user_valid_number = is_number(UserChoice);
										user_ID_choice = atoi(UserChoice.c_str());
										transform(UserChoice.begin(), UserChoice.end(),UserChoice.begin(), toupper);

										//----------------------------------------

										ClearScreen();

										if (UserChoice == "EXIT" || UserChoice == "E")
										{
											bExitLoop3 = true;
										}

										else if (user_valid_number == 1)
										{
											if (user_ID_choice < i)
											{
												item_id = backpack[user_ID_choice].ID;
												item_name = backpack[user_ID_choice].Name;
												item_sell = backpack[user_ID_choice].Value;

												holder_remove_item (backpack, item_id);
												player_glass_marbles += item_sell; 	

												//----------------------------------------

												cout << endl;
												cout << "                                    SELLING" << endl;
												cout << "    ------------------------------------------------------------------------" << endl;
												cout << endl;
												cout << "     Successfully sold: " << item_name << endl;
												cout << endl;
												cout << "     Received: " << item_sell << endl;
												cout << endl;
												cout << "     Glass Marbles: " << player_glass_marbles << endl;
												cout << endl;
												cout << "    ------------------------------------------------------------------------" << endl;
												cout << "    |Press ENTER to continue..." << endl;
												cout << "    |";
												WaitForEnter();
											}

											else if (user_ID_choice < (int) backpack.size())
											{
												cout << endl;
												cout << "                                     FIZZLE" << endl;
												cout << "    ------------------------------------------------------------------------" << endl;
												cout << endl;
												cout << "     No item in this backpack slot." << endl;
												cout << endl;
												cout << "    ------------------------------------------------------------------------" << endl;
												cout << "    |Press ENTER to continue..." << endl;
												cout << "    |";
												WaitForEnter();
											}

											else if (user_ID_choice >= (int) backpack.size())
											{
												cout << endl;
												cout << "                                     FIZZLE" << endl;
												cout << "    ------------------------------------------------------------------------" << endl;
												cout << endl;
												cout << "     Your backpack does not have so much slots." << endl;
												cout << endl;
												cout << "    ------------------------------------------------------------------------" << endl;
												cout << "    |Press ENTER to continue..." << endl;
												cout << "    |";
												WaitForEnter();
											}

										}	// Valid Number Checker  {} END

										else
										{
											cout << endl;
											cout << "                                     FIZZLE" << endl;
											cout << "    ------------------------------------------------------------------------" << endl;
											cout << endl;
											cout << "     Please type in the correct IDN of Item." << endl;
											cout << endl;
											cout << "    ------------------------------------------------------------------------" << endl;
											cout << "    |Press ENTER to continue..." << endl;
											cout << "    |";
											WaitForEnter();
										}
										
									}while(!bExitLoop3);	// do-while MERCHANT SELL Loop - {} END
								}
								
							}while(!bExitLoop2);	// do-while MERCHANT Loop - {} END

						}	// if "MERCHANT" - {} End

						//:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.
						//:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.
						//:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.

						else if (UserChoice == "PHARMACIST" || UserChoice == "P" )
						{
							bExitLoop2 = false;

							do
							{
								ClearScreen();
								cout << endl;
								cout << "    TOWN -> MARKETPLACE -> PHARMACIST" << endl;
								cout << "    ------------------------------------------------------------------------" << endl;
								cout << endl;
								cout << "     Entering the Pharmacist Store..." << endl;
								cout << "     Following Potions are available:" << endl;
								cout << endl;
								cout << endl;
								cout << "    ------------------------------------------------------------------------" << endl;
								cout << "    |IDN|               NAME                |  HEALTH  |  MAGIC  |  PRICE  |" << endl;
								cout << "    ------------------------------------------------------------------------" << endl;

								for (shopdex = 0; shopdex < NUMBEROFPOTIONS; ++shopdex)
								{
									if ((potion[shopdex].Level <= player_combat_level) && potion[shopdex].Type == "BuyRestoration")
									{
											cout << "    |" << setw(3) << shopdex		<< "|"
											<< setw(35)		<< potion[shopdex].Name		<< "|    "
											<< setw(3)		<< potion[shopdex].Health	<< "   |   "
											<< setw(3)		<< potion[shopdex].Magic	<< "   |   "
											<< setw(3)		<< potion[shopdex].Buy		<< "   |"
											<< endl;
									}

								}

								cout << "    ------------------------------------------------------------------------" << endl;
								cout << endl;
								cout << endl;
								cout << "                          Available: "<< player_glass_marbles << " [Glass Marbles]" << endl;
								cout << "    ------------------------------------------------------------------------" << endl;
								cout << "                                  (IDN)  |  EXIT                             " << endl;
								cout << "    ------------------------------------------------------------------------" << endl;
								cout << "    |Your decision: ";

								getline(cin, UserChoice);
								user_valid_number = is_number(UserChoice);
								user_ID_choice = atoi(UserChoice.c_str());
								transform(UserChoice.begin(), UserChoice.end(),UserChoice.begin(), toupper);

								if (user_ID_choice < 0 || user_ID_choice >= NUMBEROFPOTIONS)
								{
									user_valid_number = 0;
								}

								//----------------------------------------
								
								ClearScreen();

								if (UserChoice == "EXIT" || UserChoice == "E")
								{
									bExitLoop2 = true;
								}

								else if (user_valid_number == 1)
								{
									if ((potion[user_ID_choice].Type == "BuyRestoration") && (potion[user_ID_choice].Level <= player_combat_level))
									{
										for (id_checker = 0 ; id_checker < NUMBEROFPOTIONS ; ++id_checker)
										{	// For Loop to find ID user entered  {} START
											if (user_ID_choice ==  (int) id_checker)
											{	// If searching {} START
												holder_space_counter (potion_kit,potionkit_free_space);

												item_id = potion[user_ID_choice].ID;
												item_name = potion[user_ID_choice].Name;
												item_health = potion[user_ID_choice].Health;
												item_magic = potion[user_ID_choice].Magic;
												item_strength = potion[user_ID_choice].Strength;
												item_defense = potion[user_ID_choice].Defense;
												item_agility = potion[user_ID_choice].Agility;
												item_level = potion[user_ID_choice].Level;
												item_buy = potion[user_ID_choice].Buy;
												item_sell = potion[user_ID_choice].Sell;

												if (potionkit_free_space >= 1)
												{
													if (player_combat_level >= item_level)
													{	
														if (player_glass_marbles >= item_buy)
														{
															player_glass_marbles -= item_buy;
															holder_add_item(potion_kit,item_id);
														
															bExitLoop2 = true;

															cout << endl;
															cout << "                                   PURCHASING" << endl;
															cout << "    ------------------------------------------------------------------------" << endl;
															cout << endl;
															cout << "     Successfully purchased: " << item_name << endl;
															cout << "     Glass Marbles left: " << player_glass_marbles << endl;
															cout << endl;
															cout << "     " << item_name << " has been added to potion kit." << endl;
															cout << endl;
															cout << endl;
															cout << "    ------------------------------------------------------------------------" << endl;
															cout << "    |Press ENTER to continue..." << endl;
															cout << "    |";
															WaitForEnter();
														} // (player_glass_marbles >= item_buy) {} END

														else
														{
															bExitLoop2 = true;
															money_required = (item_buy - player_glass_marbles);

															cout << endl;
															cout << "                                     FIZZLE" << endl;
															cout << "    ------------------------------------------------------------------------" << endl;
															cout << endl;
															cout << "     You do not have enough Glass Marbles to purchase this potion." << endl;
															cout << "     You need " << money_required << " more Glass Marbles." << endl;
															cout << endl;
															cout << "    ------------------------------------------------------------------------" << endl;
															cout << "    |Press ENTER to continue..." << endl;
															cout << "    |";
															WaitForEnter();
														}

													}	// (player_combat_level >= item_level) {} END

													else
													{
														bExitLoop2 = true;
														money_required = (item_level - player_combat_level);
													
														cout << endl;
														cout << "                                     FIZZLE" << endl;
														cout << "    ------------------------------------------------------------------------" << endl;
														cout << endl;
														cout << "     Your level is not high enough to purchase " << item_name << "." << endl;
														cout << "     You need " << money_required << " more levels to purchase it." << endl;
														cout << endl;
														cout << "    ------------------------------------------------------------------------" << endl;
														cout << "    |Press ENTER to continue..." << endl;
														cout << "    |";
														WaitForEnter();
													}

												} // (backpack_free_space >= 1) {} END

												else
												{
													cout << endl;
													cout << "                                     FIZZLE" << endl;
													cout << "    ------------------------------------------------------------------------" << endl;
													cout << endl;
													cout << "     No free space in potion kit." << endl;
													cout << endl;
													cout << "    ------------------------------------------------------------------------" << endl;
													cout << "    |Press ENTER to continue..." << endl;
													cout << "    |";
													WaitForEnter();
												}

											}	// (user_ID_choice == id_checker) {} END

										}	// (id_checker = 0 ; id_checker < NUMBEROFPOTIONS ; ++id_checker) {} END

									}	// ((potion[user_ID_choice].Type == "BuyRestoration") && (potion[user_ID_choice].Level <= player_combat_level)) {} END

									else
									{
										cout << endl;
										cout << "                                     FIZZLE" << endl;
										cout << "    ------------------------------------------------------------------------" << endl;
										cout << endl;
										cout << "     You have typed in wrong IDN. Please select one of the available." << endl;
										cout << endl;
										cout << "    ------------------------------------------------------------------------" << endl;
										cout << "    |Press ENTER to continue..." << endl;
										cout << "    |";
										WaitForEnter();
									}

								}	// (user_valid_number == 1) {} END

								else
								{
									cout << endl;
									cout << "                                     FIZZLE" << endl;
									cout << "    ------------------------------------------------------------------------" << endl;
									cout << endl;
									cout << "     Please type in a valid numerical number to purchase a potion." << endl;
									cout << endl;
									cout << "    ------------------------------------------------------------------------" << endl;
									cout << "    |Press ENTER to continue..." << endl;
									cout << "    |";
									WaitForEnter();
								}

							}while(!bExitLoop2);	// do-while "PHARMACIST" Loop End

						}	//---------------------------------------- else if "PHARMACIST" {} END

						//:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.
						//:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.
						//:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.

						else if (UserChoice == "ARMORSMITH" || UserChoice == "A" )
						{
							bExitLoop2 = false;

							do
							{
								ClearScreen();
								cout << endl;
								cout << "    TOWN -> MARKETPLACE -> ARMORSMITH" << endl;
								cout << "    ------------------------------------------------------------------------" << endl;
								cout << endl;
								cout << "     Entering the " << player_class_name << "  Armor Section..." << endl;
								cout << "     Following Armors are available:" << endl;
								cout << endl;
								cout << endl;
								cout << "    ------------------------------------------------------------------------" << endl;
								cout << "    |IDN|           NAME           |  HEA  |  DEF  |  AGI  |  LVL  |  BUY  |" << endl;
								cout << "    ------------------------------------------------------------------------" << endl;

								for (shopdex = 0; shopdex < NUMBEROFARMORS; ++shopdex)
								{
									if ((armor[shopdex].Race == "All" || armor[shopdex].Race == player_class_name) && (armor[shopdex].ID != 309 && armor[shopdex].ID != 319 && armor[shopdex].ID != 329 && armor[shopdex].ID != 333) && (armor[shopdex].Level <= player_combat_level))
									{
											cout << "    |" << setw(3) << shopdex		<< "|"
											<< setw(26)		<< armor[shopdex].Name		<< "|  "
											<< setw(3)		<< armor[shopdex].Health	<< "  |  "
											<< setw(3)		<< armor[shopdex].Defense	<< "  |  "
											<< setw(3)		<< armor[shopdex].Agility	<< "  |  "
											<< setw(3)		<< armor[shopdex].Level		<< "  |  "
											<< setw(3)		<< armor[shopdex].Buy		<< "  |"
											<< endl;
									}

									if (player_class_name == "Barbarian" && shopdex == 29)
									{
										cout << "    |---|--------------------------|-------|-------|-------|-------|-------|" << endl;
									}

								}

								cout << "    ------------------------------------------------------------------------" << endl;
								cout << endl;
								cout << endl;
								cout << "                          Available: "<< player_glass_marbles << " [Glass Marbles]" << endl;
								cout << "    ------------------------------------------------------------------------" << endl;
								cout << "                                  (IDN)  |  EXIT                             " << endl;
								cout << "    ------------------------------------------------------------------------" << endl;
								cout << "    |Your decision: ";

								getline(cin, UserChoice);
								user_valid_number = is_number(UserChoice);
								user_ID_choice = atoi(UserChoice.c_str());
								transform(UserChoice.begin(), UserChoice.end(),UserChoice.begin(), toupper);

								if (user_ID_choice < 0 || user_ID_choice >= NUMBEROFARMORS)
								{
									user_valid_number = 0;
								}

								//----------------------------------------
								
								ClearScreen();

								if (UserChoice == "EXIT" || UserChoice == "E")
								{
									bExitLoop2 = true;
								}

								else if (user_valid_number == 1)
								{
									if ((armor[user_ID_choice].Race == "All" || armor[user_ID_choice].Race == player_class_name) && (armor[user_ID_choice].ID != 309 && armor[user_ID_choice].ID != 319 && armor[user_ID_choice].ID != 329 && armor[user_ID_choice].ID != 333))
									{
										for (id_checker = 0 ; id_checker < NUMBEROFARMORS ; ++id_checker)
										{	// For Loop to find ID user entered  {} START
											if (user_ID_choice == (int) id_checker)
											{	// If searching {} START
												holder_space_counter (backpack,backpack_free_space);

												item_id = armor[user_ID_choice].ID;
												item_name = armor[user_ID_choice].Name;
												item_health = armor[user_ID_choice].Health;
												item_defense = armor[user_ID_choice].Defense;
												item_agility = armor[user_ID_choice].Agility;
												item_level = armor[user_ID_choice].Level;
												item_buy = armor[user_ID_choice].Buy;
												item_sell = armor[user_ID_choice].Sell;

												if (backpack_free_space >= 1)
												{
													if (player_combat_level >= item_level)
													{	
														if (player_glass_marbles >= item_buy)
														{
															i = 999;
															bExitLoop3 = false;
															player_glass_marbles -= item_buy;
														
															do
															{
																ClearScreen();
																cout << endl;
																cout << "    TOWN -> MARKETPLACE -> ARMORSMITH " << endl;
																cout << "    ------------------------------------------------------------------------" << endl;
																cout << endl;
																cout << "     Please select position of protection to equip:" << endl;			
																cout << endl;
																cout << setw(35) << "ARMOR" << endl;
																cout << setw(35) << "SHIELD" << endl;
																cout << endl;
																cout << endl;
																cout << "    ------------------------------------------------------------------------" << endl;
																cout << "                                ARMOR  |  SHIELD                            " << endl;
																cout << "    ------------------------------------------------------------------------" << endl;
																cout << "    |Your decision: ";

																getline(cin, UserChoice);
																transform(UserChoice.begin(), UserChoice.end(),UserChoice.begin(), toupper);
															
																ClearScreen();
															
																if ((UserChoice == "ARMOR" || UserChoice == "A") && (player_class_name == armor[user_ID_choice].Race) && (armor[user_ID_choice].Type == "Armor"))
																{
																	bExitLoop3 = true;
																
																	i = player_armor_ID_carried;
																	holder_add_item (backpack, i);
																	player_armor_ID_carried = item_id;

																	player_armor_stats_name = item_name;
																	player_armor_stats_health = item_health;
																	player_armor_stats_defense = item_defense;
																	player_armor_stats_agility = item_agility;
																}

																else if ((UserChoice == "SHIELD" || UserChoice == "S") && (player_class_name == armor[user_ID_choice].Race) && (armor[user_ID_choice].Type == "Shield"))
																{
																	bExitLoop3 = true;
																
																	i = player_shield_ID_carried;
																	holder_add_item (backpack, i);
																	player_shield_ID_carried = item_id;

																	player_shield_stats_name = item_name;
																	player_shield_stats_health = item_health;
																	player_shield_stats_defense = item_defense;
																	player_shield_stats_agility = item_agility;
																}
															
																else
																{
																	cout << endl;
																	cout << "                                     FIZZLE" << endl;
																	cout << "    ------------------------------------------------------------------------" << endl;
																	cout << endl;
																	cout << "     Please select right allocation of protection." << endl;
																	cout << endl;
																	cout << "    ------------------------------------------------------------------------" << endl;
																	cout << "    |Press ENTER to continue..." << endl;
																	cout << "    |";
																	WaitForEnter();
																} 
															}while(!bExitLoop3);	// do-while "POSITION" Loop End
															
															//-------------------------------------

															bExitLoop2 = true;

															cout << endl;
															cout << "                                   PURCHASING" << endl;
															cout << "    ------------------------------------------------------------------------" << endl;
															cout << endl;
															cout << "     Successfully purchased: " << item_name << endl;
															cout << "     Glass Marbles left: " << player_glass_marbles << endl;
															cout << endl;
															cout << "     The following things have changed:" << endl;
															cout << endl;

															cout << setw(20) << "Health = ";
																							Is_Positive (item_health);
																							cout << item_health << endl;
															cout << setw(20) << "Defense = ";
																							Is_Positive (item_defense);
																							cout << item_defense << endl;
															cout << setw(20) << "Agility = ";
																							Is_Positive (item_agility);
																							cout << item_agility << endl;
															cout << endl;
															cout << endl;
															cout << "    ------------------------------------------------------------------------" << endl;
															cout << "    |Press ENTER to continue..." << endl;
															cout << "    |";
															WaitForEnter();
														} // (player_glass_marbles >= item_buy) {} END

														else
														{
															bExitLoop2 = true;
															money_required = (item_buy - player_glass_marbles);

															cout << endl;
															cout << "                                     FIZZLE" << endl;
															cout << "    ------------------------------------------------------------------------" << endl;
															cout << endl;
															cout << "     You do not have enough Glass Marbles to purchase this armor." << endl;
															cout << "     You need " << money_required << " more Glass Marbles." << endl;
															cout << endl;
															cout << "    ------------------------------------------------------------------------" << endl;
															cout << "    |Press ENTER to continue..." << endl;
															cout << "    |";
															WaitForEnter();
														}

													}	// (player_combat_level >= item_level) {} END

													else
													{
														bExitLoop2 = true;
														money_required = (item_level - player_combat_level);
													
														cout << endl;
														cout << "                                     FIZZLE" << endl;
														cout << "    ------------------------------------------------------------------------" << endl;
														cout << endl;
														cout << "     Your level is not high enough to use " << item_name << "." << endl;
														cout << "     You need " << money_required << " more levels to use it." << endl;
														cout << endl;
														cout << "    ------------------------------------------------------------------------" << endl;
														cout << "    |Press ENTER to continue..." << endl;
														cout << "    |";
														WaitForEnter();
													}

												} // (backpack_free_space >= 1) {} END

												else
												{
													cout << endl;
													cout << "                                     FIZZLE" << endl;
													cout << "    ------------------------------------------------------------------------" << endl;
													cout << endl;
													cout << "     No free space in backpack." << endl;
													cout << endl;
													cout << "    ------------------------------------------------------------------------" << endl;
													cout << "    |Press ENTER to continue..." << endl;
													cout << "    |";
													WaitForEnter();
												}

											}	// (user_ID_choice == id_checker) {} END

										}	// (id_checker = 0 ; id_checker < NUMBEROFARMORS ; ++id_checker) {} END

									}	// ((armor[user_ID_choice].Race == "All") || (player_class_name == armor[user_ID_choice].Race)) {} END

									else
									{
										cout << endl;
										cout << "                                     FIZZLE" << endl;
										cout << "    ------------------------------------------------------------------------" << endl;
										cout << endl;
										cout << "     You have typed in wrong IDN. Please select one of the available." << endl;
										cout << endl;
										cout << "    ------------------------------------------------------------------------" << endl;
										cout << "    |Press ENTER to continue..." << endl;
										cout << "    |";
										WaitForEnter();
									}

								}	// (user_valid_number == 1) {} END

								else
								{
									cout << endl;
									cout << "                                     FIZZLE" << endl;
									cout << "    ------------------------------------------------------------------------" << endl;
									cout << endl;
									cout << "     Please type in a valid numerical number to purchase an armor." << endl;
									cout << endl;
									cout << "    ------------------------------------------------------------------------" << endl;
									cout << "    |Press ENTER to continue..." << endl;
									cout << "    |";
									WaitForEnter();
								}

							}while(!bExitLoop2);	// do-while "ARMORSMITH" Loop End

						}	//---------------------------------------- else if "ARMORSMITH" {} END

						//:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.

						else if (UserChoice == "WEAPONSMITH" || UserChoice == "W" )
						{
							bExitLoop2 = false;

							do
							{
								ClearScreen();
								cout << endl;
								cout << "    TOWN -> MARKETPLACE -> WEAPONSMITH" << endl;
								cout << "    ------------------------------------------------------------------------" << endl;
								cout << endl;
								cout << "     Entering the " << player_class_name << "  Weapon Section..." << endl;
								cout << "     Following Weapons is available:" << endl;
								cout << endl;
								cout << endl;
								cout << "    ------------------------------------------------------------------------" << endl;
								cout << "    |IDN|           NAME           |  MAN  |  STR  |  AGI  |  LVL  |  BUY  |" << endl;
								cout << "    ------------------------------------------------------------------------" << endl;

								for (shopdex = 0; shopdex < NUMBEROFWEAPONS; ++shopdex)
								{
									if ((weapon[shopdex].Race == "All" || weapon[shopdex].Race == player_class_name) && (weapon[shopdex].ID != 104 && weapon[shopdex].ID != 109 && weapon[shopdex].ID != 114 && weapon[shopdex].ID != 119 && weapon[shopdex].ID != 124 && weapon[shopdex].ID != 128 && weapon[shopdex].ID != 132 && weapon[shopdex].ID != 136 && weapon[shopdex].ID != 140) && (weapon[shopdex].Level <= player_combat_level))
									{
											cout << "    |" << setw(3) << shopdex		<< "|"
											<< setw(26)		<< weapon[shopdex].Name		<< "|  "
											<< setw(3)		<< weapon[shopdex].Magic	<< "  |  "
											<< setw(3)		<< weapon[shopdex].Strength	<< "  |  "
											<< setw(3)		<< weapon[shopdex].Agility	<< "  |  "
											<< setw(3)		<< weapon[shopdex].Level	<< "  |  "
											<< setw(3)		<< weapon[shopdex].Buy		<< "  |"
											<< endl;
									}

									if ((player_class_name == "Sorceress" && shopdex == 36) || shopdex == 4 || shopdex == 9 || shopdex == 14 || shopdex == 19 || shopdex == 24)
									{
										cout << "    |---|--------------------------|-------|-------|-------|-------|-------|" << endl;
									}

								}

								cout << "    ------------------------------------------------------------------------" << endl;
								cout << endl;
								cout << endl;
								cout << "                          Available: "<< player_glass_marbles << " [Glass Marbles]" << endl;
								cout << "    ------------------------------------------------------------------------" << endl;
								cout << "                                  (IDN)  |  EXIT                             " << endl;
								cout << "    ------------------------------------------------------------------------" << endl;
								cout << "    |Your decision: ";

								getline(cin, UserChoice);
								user_valid_number = is_number(UserChoice);
								user_ID_choice = atoi(UserChoice.c_str());
								transform(UserChoice.begin(), UserChoice.end(),UserChoice.begin(), toupper);

								if (user_ID_choice < 0 || user_ID_choice >= NUMBEROFWEAPONS)
								{
									user_valid_number = 0;
								}

								//----------------------------------------
								
								ClearScreen();

								if (UserChoice == "EXIT" || UserChoice == "E")
								{
									bExitLoop2 = true;
								}

								else if (user_valid_number == 1)
								{
									if ((weapon[user_ID_choice].Race == "All" || weapon[user_ID_choice].Race == player_class_name) && (weapon[user_ID_choice].ID != 104 && weapon[user_ID_choice].ID != 109 && weapon[user_ID_choice].ID != 114 && weapon[user_ID_choice].ID != 119 && weapon[user_ID_choice].ID != 124 && weapon[user_ID_choice].ID != 128 && weapon[user_ID_choice].ID != 132 && weapon[user_ID_choice].ID != 136 && weapon[user_ID_choice].ID != 140))
									{
										for (id_checker = 0 ; id_checker < NUMBEROFWEAPONS ; ++id_checker)
										{	// For Loop to find ID user entered  {} START
											if (user_ID_choice == (int) id_checker)
											{	// If searching {} START
												holder_space_counter (backpack,backpack_free_space);

												item_id = weapon[user_ID_choice].ID;
												item_name = weapon[user_ID_choice].Name;
												item_magic = weapon[user_ID_choice].Magic;
												item_strength = weapon[user_ID_choice].Strength;
												item_agility = weapon[user_ID_choice].Agility;
												item_level = weapon[user_ID_choice].Level;
												item_buy = weapon[user_ID_choice].Buy;
												item_sell = weapon[user_ID_choice].Sell;

												if (backpack_free_space >= 1)
												{
													if (player_combat_level >= item_level)
													{	
														if (player_glass_marbles >= item_buy)
														{
															i = 999;
															bExitLoop3 = false;
															special_weapon_available = 0;
															player_glass_marbles -= item_buy;

															if (backpack_free_space >= 2)
															{
																special_weapon_available = 1;
															}
														
															do
															{
																ClearScreen();
																cout << endl;
																cout << "    TOWN -> MARKETPLACE -> WEAPONSMITH " << endl;
																cout << "    ------------------------------------------------------------------------" << endl;
																cout << endl;
																cout << "     Please select position of weapon to equip:" << endl;
																cout << endl;
																cout << setw(35) << "DISARM WEAPONS" << endl;
																cout << setw(35) << "MAIN WEAPON" << endl;
																cout << setw(35) << "OFFHAND WEAPON" << endl;

																if (special_weapon_available == 1)
																{
																	cout << setw(35) << "SPECIAL WEAPON" << endl;
																	cout << endl;
																	cout << endl;
																	cout << "    ------------------------------------------------------------------------" << endl;
																	cout << "                     MAIN  |  OFFHAND  |  SPECIAL  |  DISARM                " << endl;
																}

																else
																{
																	cout << endl;
																	cout << endl;
																	cout << "    ------------------------------------------------------------------------" << endl;
																	cout << "                           MAIN  |  OFFHAND  |  DISARM                      " << endl;
																}

																cout << "    ------------------------------------------------------------------------" << endl;
																cout << "    |Your decision: ";

																getline(cin, UserChoice);
																transform(UserChoice.begin(), UserChoice.end(),UserChoice.begin(), toupper);
															
																ClearScreen();
															
																if ((UserChoice == "MAIN" || UserChoice == "M") && (special_weapon_equiped == 0) && (weapon[user_ID_choice].Race == "All") && (weapon[user_ID_choice].Type == "One Handed"))
																{
																	bExitLoop3 = true;

																	if (player_main_weapon_ID_carried != 999)
																	{
																		i = player_main_weapon_ID_carried;
																		holder_add_item (backpack, i);
																		player_main_weapon_ID_carried = 999;
																	}

																	player_main_weapon_ID_carried = item_id;
																	player_main_weapon_stats_name = item_name;
																	player_main_weapon_stats_magic = item_magic;
																	player_main_weapon_stats_strength = item_strength;
																	player_main_weapon_stats_agility = item_agility;
																
																}
															
																else if ((UserChoice == "OFFHAND" || UserChoice == "O") && (special_weapon_equiped == 0) && (((player_class_name == "Rogue") && (weapon[user_ID_choice].Type == "One Handed")) || ((player_class_name == "Sorceress") && (weapon[user_ID_choice].Type == "Parchment Scroll"))))
																{
																	bExitLoop3 = true;
																
																	if (player_offhand_weapon_ID_carried != 999)
																	{
																		i = player_offhand_weapon_ID_carried;
																		holder_add_item (backpack, i);
																		player_offhand_weapon_ID_carried = 999;
																	}

																	player_offhand_weapon_ID_carried = item_id;
																	player_offhand_weapon_stats_name = item_name;
																	player_offhand_weapon_stats_magic = item_magic;
																	player_offhand_weapon_stats_strength = item_strength;
																	player_offhand_weapon_stats_agility = item_agility;
																}
															
																else if ((UserChoice == "SPECIAL" || UserChoice == "S") && (special_weapon_available == 1) && (player_class_name == weapon[user_ID_choice].Race) && ((weapon[user_ID_choice].Type == "Two Handed") || (weapon[user_ID_choice].Type == "Grand Staff") || (weapon[user_ID_choice].Type == "Ranged Weapon")))
																{
																	bExitLoop3 = true;
																
																	if (player_main_weapon_ID_carried != 999)
																	{
																		i = player_main_weapon_ID_carried;
																		holder_add_item (backpack, i);
																		player_main_weapon_ID_carried = 999;
																	}

																	if (player_offhand_weapon_ID_carried != 999)
																	{
																		i = player_offhand_weapon_ID_carried;
																		holder_add_item (backpack, i);
																		player_offhand_weapon_ID_carried = 999;
																	}

																	special_weapon_equiped = 1;
																	player_main_weapon_ID_carried = item_id;

																	player_main_weapon_stats_name = item_name;
																	player_main_weapon_stats_magic = item_magic;
																	player_main_weapon_stats_strength = item_strength;
																	player_main_weapon_stats_agility = item_agility;
																}

																else if (UserChoice == "DISARM" || UserChoice == "D")
																{
																	if (player_main_weapon_ID_carried != 999)
																	{
																		i = player_main_weapon_ID_carried;
																		holder_add_item (backpack, i);
																		player_main_weapon_ID_carried = 999;
																	}

																	if (player_offhand_weapon_ID_carried != 999)
																	{
																		i = player_offhand_weapon_ID_carried;
																		holder_add_item (backpack, i);
																		player_offhand_weapon_ID_carried = 999;
																	}

																	special_weapon_equiped = 0;

																	player_main_weapon_stats_name = "None";
																	player_main_weapon_stats_magic = 0;
																	player_main_weapon_stats_strength = 0;
																	player_main_weapon_stats_agility = 0;

																	player_offhand_weapon_stats_name = "None";
																	player_offhand_weapon_stats_magic = 0;
																	player_offhand_weapon_stats_strength = 0;
																	player_offhand_weapon_stats_agility = 0;
																	
																	cout << endl;
																	cout << "    TOWN -> MARKETPLACE -> WEAPONSMITH " << endl;
																	cout << "    ------------------------------------------------------------------------" << endl;
																	cout << endl;
																	cout << "     Your weapons has been stored in backpack successfully." << endl;
																	cout << endl;
																	cout << "    ------------------------------------------------------------------------" << endl;
																	cout << "    |Press ENTER to continue..." << endl;
																	cout << "    |";
																	WaitForEnter();
																} // (UserChoice == "DISARM" || UserChoice == "D") {} END
															
																else
																{
																	cout << endl;
																	cout << "                                     FIZZLE" << endl;
																	cout << "    ------------------------------------------------------------------------" << endl;
																	cout << endl;
																	cout << "     Please select right allocation of weapon." << endl;
																	cout << endl;
																	cout << "    ------------------------------------------------------------------------" << endl;
																	cout << "    |Press ENTER to continue..." << endl;
																	cout << "    |";
																	WaitForEnter();
																}

															}while(!bExitLoop3);	// do-while "POSITION" Loop End
															
															//-------------------------------------

															bExitLoop2 = true;

															cout << endl;
															cout << "                                   PURCHASING" << endl;
															cout << "    ------------------------------------------------------------------------" << endl;
															cout << endl;
															cout << "     Successfully purchased: " << item_name << endl;
															cout << "     Glass Marbles left: " << player_glass_marbles << endl;
															cout << endl;
															cout << "     The following things have changed:" << endl;
															cout << endl;

															cout << setw(20) << "Magic = ";
																							Is_Positive (item_magic);
																							cout << item_magic << endl;
															cout << setw(20) << "Strength = ";
																							Is_Positive (item_strength);
																							cout << item_strength << endl;
															cout << setw(20) << "Agility = ";
																							Is_Positive (item_agility);
																							cout << item_agility << endl;
															cout << endl;
															cout << endl;
															cout << "    ------------------------------------------------------------------------" << endl;
															cout << "    |Press ENTER to continue..." << endl;
															cout << "    |";
															WaitForEnter();
														} // (player_glass_marbles >= item_buy) {} END

														else
														{
															bExitLoop2 = true;
															money_required = (item_buy - player_glass_marbles);

															cout << endl;
															cout << "                                     FIZZLE" << endl;
															cout << "    ------------------------------------------------------------------------" << endl;
															cout << endl;
															cout << "     You do not have enough Glass Marbles to purchase this weapon." << endl;
															cout << "     Missing " << money_required << " more Glass Marbles." << endl;
															cout << endl;
															cout << "    ------------------------------------------------------------------------" << endl;
															cout << "    |Press ENTER to continue..." << endl;
															cout << "    |";
															WaitForEnter();
														}

													}	//(player_combat_level >= item_level) {} END

													else
													{
														bExitLoop2 = true;
														money_required = (item_level - player_combat_level);
													
														cout << endl;
														cout << "                                     FIZZLE" << endl;
														cout << "    ------------------------------------------------------------------------" << endl;
														cout << endl;
														cout << "     Your level is not high enough to use " << item_name << "." << endl;
														cout << "     You need " << money_required << " more levels to use it." << endl;
														cout << endl;
														cout << "    ------------------------------------------------------------------------" << endl;
														cout << "    |Press ENTER to continue..." << endl;
														cout << "    |";
														WaitForEnter();
													}

												} // (backpack_free_space >= 1) {} END

												else
												{
													cout << endl;
													cout << "                                     FIZZLE" << endl;
													cout << "    ------------------------------------------------------------------------" << endl;
													cout << endl;
													cout << "     No free space in backpack." << endl;
													cout << endl;
													cout << "    ------------------------------------------------------------------------" << endl;
													cout << "    |Press ENTER to continue..." << endl;
													cout << "    |";
													WaitForEnter();
												}

											}	// (user_ID_choice == id_checker) {} END

										}	// (id_checker = 0 ; id_checker < NUMBEROFWEAPONS ; ++id_checker) {} END

									}	// ((weapon[user_ID_choice].Race == "All") || (player_class_name == weapon[user_ID_choice].Race)) {}END

									else
									{
										cout << endl;
										cout << "                                     FIZZLE" << endl;
										cout << "    ------------------------------------------------------------------------" << endl;
										cout << endl;
										cout << "     You have typed in wrong IDN. Please select one of the available." << endl;
										cout << endl;
										cout << "    ------------------------------------------------------------------------" << endl;
										cout << "    |Press ENTER to continue..." << endl;
										cout << "    |";
										WaitForEnter();
									}

								}	// (user_valid_number == 1) {} END

								else
								{
									cout << endl;
									cout << "                                     FIZZLE" << endl;
									cout << "    ------------------------------------------------------------------------" << endl;
									cout << endl;
									cout << "     Please type in the correct IDN of Weapon." << endl;
									cout << endl;
									cout << "    ------------------------------------------------------------------------" << endl;
									cout << "    |Press ENTER to continue..." << endl;
									cout << "    |";
									WaitForEnter();
								}

							}while(!bExitLoop2);	// do-while "WEAPONSMITH" Loop End

						}	//----------------------------------------"WEAPONSMITH" {} END

						//:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.

						else if (UserChoice == "CRAFTSMAN" || UserChoice == "C" )
						{
							bExitLoop2 = false;
							
							do
							{
								array_hit = 999;
								player_craft_one = 999;
								player_craft_two = 999;
								player_craft_three = 999;
								combine_rucksack (backpack, backpack_names, armor, weapon, item);

								ClearScreen();
								cout << endl;
								cout << "    TOWN -> MARKETPLACE -> CRAFTSMAN" << endl;
								cout << "    ------------------------------------------------------------------------" << endl;
								cout << endl;
								cout << "    Approaching the anvil." << endl;
								cout << "    Following Items is available:" << endl;
								cout << endl;
								cout << endl;
								cout << "    ------------------------------------------------------------------------" << endl;
								
								combine_rucksack (backpack, backpack_names, armor, weapon, item);
								rucksack_open(backpack_names);

								cout << "    ------------------------------------------------------------------------" << endl;
								cout << endl;
								cout << endl;
								cout << "    ------------------------------------------------------------------------" << endl;
								cout << "                         HELP  |  1  |  2  |  3  |  EXIT                    " << endl;
								cout << "    ------------------------------------------------------------------------" << endl;
								cout << "    |Your decision: ";

								getline(cin, UserChoice);
								user_valid_number = is_number(UserChoice);
								user_LOOP_choice = atoi(UserChoice.c_str());
								transform(UserChoice.begin(), UserChoice.end(),UserChoice.begin(), toupper);
								
								//----------------------------------------

								ClearScreen();

								if (UserChoice == "EXIT" || UserChoice == "E")
								{
									bExitLoop2 = true;
								}

								else if (UserChoice == "HELP" || UserChoice == "H")
								{
									cout << endl;
									cout << "                                      HELP" << endl;
									cout << "    ------------------------------------------------------------------------" << endl;
									cout << endl;
									cout << "    Crafting enables you to craft from one item, or combine two or more" << endl;
									cout << "    items together to create newer, better, useful item. In example, you" << endl;
									cout << "    can create from one Coal ore , a Peat. Try to combine more items to" << endl;
									cout << "    find by yourself better, rare items..." << endl;
									cout << endl;
									cout << endl;
									cout << "    Do not forget, crafting can be successful as much as it can fail. ;)" << endl;
									cout << endl;
									cout << "    ------------------------------------------------------------------------" << endl;
									cout << "    |Press ENTER to continue..." << endl;
									cout << "    |";
									WaitForEnter();
								}
								else if (user_valid_number == 1)
								{
									if ((user_LOOP_choice == 1) || (user_LOOP_choice == 2) || (user_LOOP_choice == 3))
									{
										for ( j = 1 ; j <= user_LOOP_choice ; ++j)
										{
											bExitLoop3 = false;
											
											do
											{
												combine_rucksack (backpack, backpack_names, armor, weapon, item);

												ClearScreen();
												cout << endl;
												cout << "                                    CRAFTING                                " << endl;
												cout << "    ------------------------------------------------------------------------" << endl;
												cout << endl;
												cout << "     Available items You can use for crafting:" << endl;
												cout << endl;
												cout << endl;
												cout << "    ------------------------------------------------------------------------" << endl;
												cout << "    |IDN|                               NAME                               |" << endl;
												cout << "    ------------------------------------------------------------------------" << endl;
												
												for (i = 0 ; i < (int) backpack.size() ; ++i)
												{
													if (backpack[i].ID == 999)
													{
														break;
													}
													else
													{
														cout << "    |" << setw(3) << i << "|" << setw(34) << backpack[i].Name << setw(33) << "|" << endl;
													}
												}

												cout << "    ------------------------------------------------------------------------" << endl;
												cout << endl;
												cout << endl;

												//----------------------------------------Zamjeni imenima

												cout << "     First item is: ";

												if (player_craft_one != 999)
												{
													cout << item_product1name << endl;
												}

												else 
												{
													cout << "Nothing" << endl;
												}

												//----------------------------------------Zamjeni imenima

												cout << "     Second item is: ";
											
												if (player_craft_two != 999)
												{
													cout << item_product2name << endl;
												}

												else 
												{
													cout << "Nothing" << endl;
												}

												//----------------------------------------Zamjeni imenima

												cout << "     Third item is: ";
											
												if (player_craft_three != 999)
												{
													cout << item_product3name << endl;
												}

												else 
												{
													cout << "Nothing" << endl;
												}
											
												cout << endl << endl;
											
												cout << "    ------------------------------------------------------------------------" << endl;
												cout << "                                 (IDN)  |  EXIT                             " << endl;
												cout << "    ------------------------------------------------------------------------" << endl;
												cout << "    |Your decision: ";


												getline(cin, UserChoice);
												user_valid_number = is_number(UserChoice);
												user_ID_choice = atoi(UserChoice.c_str());
												transform(UserChoice.begin(), UserChoice.end(),UserChoice.begin(), toupper);
	
												//----------------------------------------

												ClearScreen();

												if (UserChoice == "EXIT" || UserChoice == "E")
												{
													bExitLoop3 = true;
													j = 3;
												}

												else if (user_valid_number == 1)
												{
													if (user_ID_choice < i)
													{
														if (j == 1)
														{
															player_craft_one = backpack[user_ID_choice].ID;
															item_product1name = backpack_names[user_ID_choice];
															holder_remove_item (backpack, player_craft_one);
															bExitLoop3 = true;
														}

														else if (j == 2)
														{
															player_craft_two = backpack[user_ID_choice].ID;
															item_product2name = backpack_names[user_ID_choice];
															holder_remove_item (backpack, player_craft_two);
															bExitLoop3 = true;
														}

														else if (j == 3)
														{
															player_craft_three = backpack[user_ID_choice].ID;
															item_product3name = backpack_names[user_ID_choice];
															holder_remove_item (backpack, player_craft_three);
															bExitLoop3 = true;
														}

													}

													else if (user_ID_choice < (int) backpack.size())
													{
														cout << endl;
														cout << "                                     FIZZLE" << endl;
														cout << "    ------------------------------------------------------------------------" << endl;
														cout << endl;
														cout << "     No item in this backpack slot." << endl;
														cout << endl;
														cout << "    ------------------------------------------------------------------------" << endl;
														cout << "    |Press ENTER to continue..." << endl;
														cout << "    |";
														WaitForEnter();
													}

													else if (user_ID_choice >= (int) backpack.size())
													{
														cout << endl;
														cout << "                                     FIZZLE" << endl;
														cout << "    ------------------------------------------------------------------------" << endl;
														cout << endl;
														cout << "     Your backpack does not have so much slots." << endl;
														cout << endl;
														cout << "    ------------------------------------------------------------------------" << endl;
														cout << "    |Press ENTER to continue..." << endl;
														cout << "    |";
														WaitForEnter();
													}
												} // (user_valid_number == 1) {} END

												else
												{
													cout << endl;
													cout << "                                     FIZZLE" << endl;
													cout << "    ------------------------------------------------------------------------" << endl;
													cout << endl;
													cout << "     Please type in valid numerical number of Item IDN." << endl;
													cout << endl;
													cout << "    ------------------------------------------------------------------------" << endl;
													cout << "    |Press ENTER to continue..." << endl;
													cout << "    |";
													WaitForEnter();
												}

											}while (!bExitLoop3);	// do-while Loop for Bad User Input

										} // ( j = 1 ; j <= user_LOOP_choice ; ++j) {} END

									} // ((user_LOOP_choice == 1) || (user_LOOP_choice == 2) || (user_LOOP_choice == 3)) {} END

									else
									{
										cout << endl;
										cout << "                                     FIZZLE" << endl;
										cout << "    ------------------------------------------------------------------------" << endl;
										cout << endl;
										cout << "     Please type in either 1, 2 or 3." << endl;
										cout << endl;
										cout << "    ------------------------------------------------------------------------" << endl;
										cout << "    |Press ENTER to continue..." << endl;
										cout << "    |";
										WaitForEnter();
									}
									
									//:::::::::::::::::-> Searching and providing item

									if (player_craft_one != 999)
									{
										for (shopdex = 0 ; shopdex < NUMBEROFITEMS ; ++shopdex)
										{
											if ( ( (item[shopdex].Requires1 == player_craft_one) && (item[shopdex].Requires2 == player_craft_two) && (item[shopdex].Requires3 == player_craft_three) ) || ( (item[shopdex].Requires1 == player_craft_two) && (item[shopdex].Requires2 == player_craft_three) && (item[shopdex].Requires3 == player_craft_one) ) || ( (item[shopdex].Requires1 == player_craft_three) && (item[shopdex].Requires2 == player_craft_one) && (item[shopdex].Requires3 == player_craft_two) ) )
											{
												array_hit = item[shopdex].ID;
												item_name = item[shopdex].Name;
											}
										}
	
										if (array_hit == 999)
										{
											holder_add_item(backpack,player_craft_one);
											holder_add_item(backpack,player_craft_two);
											holder_add_item(backpack,player_craft_three);

											cout << endl;
											cout << "                                     FIZZLE" << endl;
											cout << "    ------------------------------------------------------------------------" << endl;
											cout << endl;
											cout << "     Failed to craft new item." << endl;
											cout << endl;
											cout << "    ------------------------------------------------------------------------" << endl;
											cout << "    |Press ENTER to continue..." << endl;
											cout << "    |";
											WaitForEnter();
										}

										else
										{
											holder_add_item(backpack,array_hit);

											cout << endl;
											cout << "                                    CRAFTING                                " << endl;
											cout << "    ------------------------------------------------------------------------" << endl;
											cout << endl;
											cout << "     Success!" << endl;
											cout << endl;
											cout << "     You have successfully crafted " << item_name << endl;
											cout << endl;
											cout << "    ------------------------------------------------------------------------" << endl;
											cout << "    |Press ENTER to continue..." << endl;
											cout << "    |";
											WaitForEnter();
										}

									}	// (player_craft_one != 999) {} END

								} // (user_valid_number == 1) {} END

							}while(!bExitLoop2);	// do-while Loop to check Bad User Inputs

						}	//---------------------------------------- else if "CRAFTSMAN" {} END

						//:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.

						else if (UserChoice == "SKINNER" || UserChoice == "S" )
						{
							bExitLoop2 = false;

							do
							{
								i = 999;
								holder_furandhide_counter (backpack, number_of_fur, number_of_hide);

								ClearScreen();
								cout << endl;
								cout << "    TOWN -> MARKETPLACE -> SKINNER" << endl;
								cout << "    ------------------------------------------------------------------------" << endl;
								cout << endl;
								cout << "    Entering the Skinners Store" << endl;
								cout << "    Following Items is available:" << endl;
								cout << endl;
								cout << endl;
								cout << "    ------------------------------------------------------------------------" << endl;
								cout << "    |IDN|                 NAME                 |   FUR  |  HIDE  |  PRICE  |" << endl;
								cout << "    ------------------------------------------------------------------------" << endl;
								
								for (shopdex = 0; shopdex < NUMBEROFSKINNER ; ++shopdex)
								{
									cout	<< "    |  " << skinner[shopdex].ID	<< "|"
											<< setw(38)		<<	skinner[shopdex].Name		<< "|  "
											<< setw(3)		<<	skinner[shopdex].Fur		<< "   |  "
											<< setw(3)		<<	skinner[shopdex].Hide		<< "   |  "
											<< setw(3)		<<	skinner[shopdex].Buy		<< "    |"
											<< endl;

									if (shopdex == 3)
									{
										cout << "    |---|--------------------------------------|--------|--------|---------|" << endl;
									}
								}

								cout << "    ------------------------------------------------------------------------" << endl;
								cout << endl;
								cout << endl;
								cout << "    " << setw(15) << "Number of Fur:" << " " << setw(3) << number_of_fur << setw(49) << "Number of Hide:" << " " << number_of_hide << endl;
								cout << endl;
								cout << endl;
								cout << "    ------------------------------------------------------------------------" << endl;
								cout << "                                  (IDN)  |  EXIT                             " << endl;
								cout << "    ------------------------------------------------------------------------" << endl;
								cout << "    |Your decision: ";

								getline(cin, UserChoice);
								user_valid_number = is_number(UserChoice);
								user_ID_choice = atoi(UserChoice.c_str());
								transform(UserChoice.begin(), UserChoice.end(),UserChoice.begin(), toupper);

								//----------------------------------------

								ClearScreen();

								if (UserChoice == "EXIT" || UserChoice == "E")
								{
									bExitLoop2 = true;
									break;
								}

								else if (user_valid_number == 1)
								{
									if (user_ID_choice <= (NUMBEROFSKINNER-1))
									{
										if (user_ID_choice == 0)
										{
											error_proof = 0;
										}

										else if (user_ID_choice == 4)
										{
											error_proof = 4;
										}

										else
										{
											error_proof = user_valid_number - 1;
										}

										switch (user_ID_choice)
										{
											// Backpacks
											case 0:
												if ( (number_of_fur >= skinner[user_ID_choice].Fur) && (number_of_hide >= skinner[user_ID_choice].Hide) && (player_glass_marbles >= skinner[user_ID_choice].Buy) && skinner[user_ID_choice].Owned == 0)
												{
													i = 0;
												}
												break;

											case 1:
											case 2:
											case 3:
												if ( (number_of_fur >= skinner[user_ID_choice].Fur) && (number_of_hide >= skinner[user_ID_choice].Hide) && (player_glass_marbles >= skinner[user_ID_choice].Buy) && skinner[user_ID_choice].Owned == 0 && skinner[error_proof].Owned == 1)
												{
													i = user_ID_choice;
												}
												break;

											// Potion Kits	----------------------------------------------------------------
											case 4:
												if ( (number_of_fur >= skinner[user_ID_choice].Fur) && (number_of_hide >= skinner[user_ID_choice].Hide) && (player_glass_marbles >= skinner[user_ID_choice].Buy) && skinner[user_ID_choice].Owned == 0)
												{
													i = 4;
												}
												break;

											case 5:
											case 6:
											case 7:
												if ( (number_of_fur >= skinner[user_ID_choice].Fur) && (number_of_hide >= skinner[user_ID_choice].Hide) && (player_glass_marbles >= skinner[user_ID_choice].Buy) && skinner[user_ID_choice].Owned == 0 && skinner[error_proof].Owned == 1)
												{
													i = user_ID_choice;
												}
												break;
										}
										
										if (i >= 0 && i <= 3)
										{
											holder_upgrade(backpack, skinner[user_ID_choice].Size);

											skinner[user_ID_choice].Owned = 1;

											player_glass_marbles -= skinner[user_ID_choice].Buy;

											for (i = 0 ; i < skinner[user_ID_choice].Fur ; ++i)
											{
												holder_remove_item (backpack, 521);
											}

											for (i = 0 ; i < skinner[user_ID_choice].Hide ; ++i)
											{
												holder_remove_item (backpack, 523);
											}
												
											cout << endl;
											cout << "                                   PURCHASING" << endl;
											cout << "    ------------------------------------------------------------------------" << endl;
											cout << endl;
											cout << "     Successfully purchased: " << skinner[user_ID_choice].Name << endl;
											cout << endl;
											cout << "     Backpack space increased for: " << skinner[user_ID_choice].Size << endl;
											cout << endl;
											cout << "     Glass Marbles left: " << player_glass_marbles << endl;
											cout << endl;
											cout << "    ------------------------------------------------------------------------" << endl;
											cout << "    |Press ENTER to continue..." << endl;
											cout << "    |";
											WaitForEnter();
										}

										else if (i >= 4 && i <= 7)
										{
											holder_upgrade(potion_kit, skinner[user_ID_choice].Size);

											skinner[user_ID_choice].Owned = 1;

											player_glass_marbles -= skinner[user_ID_choice].Buy;

											for (i = 0 ; i < skinner[user_ID_choice].Fur ; ++i)
											{
												holder_remove_item (backpack, 520);
											}
										
											for (i = 0 ; i < skinner[user_ID_choice].Hide ; ++i)
											{
												holder_remove_item (backpack, 522);
											}
												
											cout << endl;
											cout << "                                   PURCHASING" << endl;
											cout << "    ------------------------------------------------------------------------" << endl;
											cout << endl;
											cout << "     Successfully purchased: " << skinner[user_ID_choice].Name << endl;
											cout << endl;
											cout << "     Potion Kit space increased for: " << skinner[user_ID_choice].Size << endl;
											cout << endl;
											cout << "     Glass Marbles left: " << player_glass_marbles << endl;
											cout << endl;
											cout << "    ------------------------------------------------------------------------" << endl;
											cout << "    |Press ENTER to continue..." << endl;
											cout << "    |";
											WaitForEnter();
										}

										else
										{
											cout << endl;
											cout << "                                     FIZZLE" << endl;
											cout << "    ------------------------------------------------------------------------" << endl;
											cout << endl;
											cout << "     Not enough resources to complete this purchase." << endl;
											cout << "     Remember, You have to buy previous upgrade in order to buy newer." << endl;
											cout << endl;
											cout << "    ------------------------------------------------------------------------" << endl;
											cout << "    |Press ENTER to continue..." << endl;
											cout << "    |";
											WaitForEnter();
										}

									} // (user_ID_choice <= (NUMBEROFSKINNER-1)) {} END
									
									else
									{
										cout << endl;
										cout << "                                     FIZZLE" << endl;
										cout << "    ------------------------------------------------------------------------" << endl;
										cout << endl;
										cout << "     Please type in an IDN from 0 - " << NUMBEROFSKINNER-1 << "." << endl;
										cout << endl;
										cout << "    ------------------------------------------------------------------------" << endl;
										cout << "    |Press ENTER to continue..." << endl;
										cout << "    |";
										WaitForEnter();
									}

								}	// (user_valid_number == 1) {} END

								else
								{
									cout << endl;
									cout << "                                     FIZZLE" << endl;
									cout << "    ------------------------------------------------------------------------" << endl;
									cout << endl;
									cout << "     Please type in the correct IDN of Item." << endl;
									cout << endl;
									cout << "    ------------------------------------------------------------------------" << endl;
									cout << "    |Press ENTER to continue..." << endl;
									cout << "    |";
									WaitForEnter();
								}

							}while(!bExitLoop2); // checker of bad commands

						}	// else if "SKINNER" {} End

					//:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:. Ending of Store Loop

					}while (!bExitLoop);	//-------------------------------------Store Looping Ends
				}	//-------------------------------------MARKETPLACE {} END

				//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

				else if (UserChoice == "ACCOUNT" || UserChoice == "A")
				{

					access_account("TOWN", backpack, backpack_names, backpack_free_space, armor, weapon, item, potion, potion_kit, potionkit_names, potionkit_free_space, UserChoice, user_valid_number, user_ID_choice, item_id, i, town_name, player_class_name, player_glass_marbles, player_combined_max_health, player_combined_health, player_combined_max_magic, player_combined_magic, player_combined_strength, player_combined_defense, player_combined_agility, player_armor_stats_name, player_armor_stats_health, player_armor_stats_defense, player_armor_stats_agility, player_shield_stats_name, player_shield_stats_health, player_shield_stats_defense, player_shield_stats_agility, player_offhand_weapon_stats_name, player_offhand_weapon_stats_magic, player_offhand_weapon_stats_agility, player_offhand_weapon_stats_strength, player_main_weapon_stats_name, player_main_weapon_stats_magic, player_main_weapon_stats_agility, player_main_weapon_stats_strength, showcase_required_combat, showcase_required_mining, showcase_required_farming, showcase_required_gambling, overall_player_level, overall_level_progress, player_combat_level, player_combat_experience, player_farming_level, player_farming_experience, player_mining_level, player_mining_experience, player_gambling_level, player_gambling_experience, total_number_of_travels, current_number_of_travels, player_main_weapon_ID_carried, player_offhand_weapon_ID_carried, special_weapon_equiped, player_armor_ID_carried, player_shield_ID_carried);
				
				}	// (UserChoice == "ACCOUNT" || UserChoice == "A") {} END

				//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

				else if (UserChoice == "JOURNEY" || UserChoice == "J")
				{
					location_travel = 1;
					bExitLocation = true;

					cout << endl;
					cout << "                                   TRAVELING" << endl;
					cout << "    ------------------------------------------------------------------------" << endl;
					cout << endl;
					cout << "     Leaving Town " << town_name << ". Setting journey into unknown." << endl;
					cout << "     While traveling, there is a possibility of encountering enemies." << endl;
					cout << "     On other hand, stumbling upon Glass Marbles is also possible." << endl;
					cout << endl;
					cout << "     Keep an eye for Gileeran Bear, their hide is very useful and precious." << endl;
					cout << endl;
					cout << "    ------------------------------------------------------------------------" << endl;
					cout << "    |Press ENTER to continue..." << endl;
					cout << "    |";
					WaitForEnter();
				}	//----------------------------------------else if "JOURNEY" {} END

			}while (!bExitLocation);	// do-while Loop for Location {} END

		}	//----------------------------------------Player Location: TOWN {} END

		//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		////:::::::::::::::::::::---------------------------------------------------------
		//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

		while (location_camp == 1)
		{	//----------------------------------------Player Location: CAMP {} START

			bExitLocation = false;
			location_camp = 0;

			//----------------------------------------

			player_combined_health = static_cast<int>(player_combined_health * 1.15);
			player_combined_magic = static_cast<int>(player_combined_magic * 1.15);

			if (player_combined_magic > player_combined_max_magic)
			{
				player_combined_magic = player_combined_max_magic;
			}

			if (player_combined_health > player_combined_max_health)
			{
				player_combined_health = player_combined_max_health;
			}

			do
			{
				ClearScreen();
				cout << endl;
				cout << "                                    CAMPING" << endl;
				cout << "    ------------------------------------------------------------------------" << endl;
				cout << endl;
				cout << "     Setting up camp. Relaxing near fire." << endl;
				cout << "     Starting self-healing process." << endl;
				cout << endl;
				cout << endl;
				cout << "     Following actions has happened:" << endl;
				cout << "                       +15% Magic has restored." << endl;
				cout << "                       +15% Health has restored." << endl;
				cout << endl;
				cout << endl;
				cout << "    ------------------------------------------------------------------------" << endl;
				cout << "                          TOWN  |  JOURNEY  |  ACCOUNT                      " << endl;
				cout << "    ------------------------------------------------------------------------" << endl;
				cout << "    |Your decision: ";

				getline(cin, UserChoice);
				transform(UserChoice.begin(), UserChoice.end(),UserChoice.begin(), toupper);

				//----------------------------------------

				ClearScreen();

				//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

				if (UserChoice == "ACCOUNT" || UserChoice == "A")
				{
					
					access_account("CAMPING", backpack, backpack_names, backpack_free_space, armor, weapon, item, potion, potion_kit, potionkit_names, potionkit_free_space, UserChoice, user_valid_number, user_ID_choice, item_id, i, town_name, player_class_name, player_glass_marbles, player_combined_max_health, player_combined_health, player_combined_max_magic, player_combined_magic, player_combined_strength, player_combined_defense, player_combined_agility, player_armor_stats_name, player_armor_stats_health, player_armor_stats_defense, player_armor_stats_agility, player_shield_stats_name, player_shield_stats_health, player_shield_stats_defense, player_shield_stats_agility, player_offhand_weapon_stats_name, player_offhand_weapon_stats_magic, player_offhand_weapon_stats_agility, player_offhand_weapon_stats_strength, player_main_weapon_stats_name, player_main_weapon_stats_magic, player_main_weapon_stats_agility, player_main_weapon_stats_strength, showcase_required_combat, showcase_required_mining, showcase_required_farming, showcase_required_gambling, overall_player_level, overall_level_progress, player_combat_level, player_combat_experience, player_farming_level, player_farming_experience, player_mining_level, player_mining_experience, player_gambling_level, player_gambling_experience, total_number_of_travels, current_number_of_travels, player_main_weapon_ID_carried, player_offhand_weapon_ID_carried, special_weapon_equiped, player_armor_ID_carried, player_shield_ID_carried);

				}	// (UserChoice == "ACCOUNT" || UserChoice == "A") {} END
				
				//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

				else if (UserChoice == "TOWN" || UserChoice == "T")
				{	
					if (current_number_of_travels <= 1)
					{
						cout << endl;
						cout << "                               PREPARING FOR TOWN" << endl;
						cout << "    ------------------------------------------------------------------------" << endl;
						cout << endl;
						cout << "     Unfortunately, the town is still too far. Couple more travels until it" << endl;
						cout << "     can be reached." << endl;
						cout << endl;
						cout << "    ------------------------------------------------------------------------" << endl;
						cout << "    |Press ENTER to continue..." << endl;
						cout << "    |";
						WaitForEnter();
					}

					else if (current_number_of_travels <= 2)
					{
						cout << endl;
						cout << "                               PREPARING FOR TOWN" << endl;
						cout << "    ------------------------------------------------------------------------" << endl;
						cout << endl;
						cout << "     Unfortunately, the town is still to far. One more travel until it can" << endl;
						cout << "     be reached." << endl;
						cout << endl;
						cout << "    ------------------------------------------------------------------------" << endl;
						cout << "    |Press ENTER to continue..." << endl;
						cout << "    |";
						WaitForEnter();
					}

					else
					{
						location_town = 1;
						bExitLocation = true;
						
						cout << endl;
						cout << "                               PREPARING FOR TOWN" << endl;
						cout << "    ------------------------------------------------------------------------" << endl;
						cout << endl;
						cout << "     Preparing to travel to near by town" << endl;
						cout << endl;
						cout << "    ------------------------------------------------------------------------" << endl;
						cout << "    |Press ENTER to continue..." << endl;
						cout << "    |";
						WaitForEnter();
					}
				}	//---------------------------------------- else if "TOWN" {} END

				//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

				else if (UserChoice == "JOURNEY" || UserChoice == "J")
				{
					location_travel = 1;
					bExitLocation = true;

					cout << endl;
					cout << "                                   TRAVELING" << endl;
					cout << "    ------------------------------------------------------------------------" << endl;
					cout << endl;
					cout << "     Leaving Campsite. Setting journey into unknown." << endl;
					cout << "     While traveling, there is a possibility of encountering enemies." << endl;
					cout << "     On other hand, stumbling upon Glass Marbles is also possibility." << endl;
					cout << endl;
					cout << "     Keep an eye for Gileeran Bear, their hide is very useful and precious." << endl;
					cout << endl;
					cout << "    ------------------------------------------------------------------------" << endl;
					cout << "    |Press ENTER to continue..." << endl;
					cout << "    |";
					WaitForEnter();
				} //---------------------------------------- else if "JOURNEY" {} END

			}while(!bExitLocation);		// do-while Loop for Location {} END

		}	//----------------------------------------Player Location: CAMP {} END

		//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		////:::::::::::::::::::::---------------------------------------------------------
		//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

		while (location_travel == 1)
		{	//----------------------------------------Player Location: TRAVELING {} START
			h = 0; // To Skip Searching area or not
			j = 0; // Duration of searching area - how many times can u look at places.
			bExitLoop2 = false;
						
			do			
			{
				ClearScreen();
				cout << endl;
				cout << "    TRAVELING -> CROSS SECTION                                              " << endl;
				cout << "    ------------------------------------------------------------------------" << endl;
				cout << endl;
				cout << "     Choose the path you wish to take:" << endl;
				cout << "     camping and player." << endl;
				cout << endl;
				cout << setw(35) << map[0].Region_Name << endl;
				cout << setw(35) << map[1].Region_Name << endl;
				cout << setw(35) << map[2].Region_Name << endl;
				cout << setw(35) << "LUIKACH" << endl;
				cout << endl;
				cout << endl;
				cout << "    ------------------------------------------------------------------------" << endl;
				cout << "                              ACCOUNT  |  CONTINUE                          " << endl;
				cout << "    ------------------------------------------------------------------------" << endl;
				cout << "    |Your decision: ";

				getline(cin, UserChoice);
				transform(UserChoice.begin(), UserChoice.end(),UserChoice.begin(), toupper);

				temp_char = UserChoice[0];
						
				//-------------------------------------

				ClearScreen();

				if (UserChoice == "CONTINUE" || UserChoice == "C")
				{
					h = 1;
					bExitLoop2 = true;
				}	//----------------------------------------(UserChoice == "CONTINUE" || UserChoice == "C") {} END

				else if (UserChoice == "LUIKACH" || UserChoice == "L")
				{
					h = 1;
					bExitLoop2 = true;
					Mining_attempt (backpack, backpack_free_space, armor, weapon, item, UserChoice, item_name, player_mining_level, player_mining_experience);
				}	//----------------------------------------(UserChoice == "LUIKACH" || UserChoice == "L") {} END
								
				else if (UserChoice == map[0].Region_Name || temp_char == map[0].Region_Name[0])
				{
					user_LOOP_choice = 0;
					bExitLoop2 = true;
				}	//----------------------------------------(UserChoice == map[0].Region_Name || temp_char == map[0].Region_Name[0]) {} END
							
				else if (UserChoice == map[1].Region_Name || temp_char == map[1].Region_Name[0])
				{
					user_LOOP_choice = 1;
					bExitLoop2 = true;
				}	//----------------------------------------(UserChoice == map[1].Region_Name || temp_char == map[1].Region_Name[0]) {} END
							
				else if (UserChoice == map[2].Region_Name || temp_char == map[2].Region_Name[0])
				{
					user_LOOP_choice = 2;
					bExitLoop2 = true;
				}	//----------------------------------------(UserChoice == map[2].Region_Name || temp_char == map[2].Region_Name[0]) {} END
				
				else if (UserChoice == "ACCOUNT" || UserChoice == "A")
				{
					access_account("TRAVELING", backpack, backpack_names, backpack_free_space, armor, weapon, item, potion, potion_kit, potionkit_names, potionkit_free_space, UserChoice, user_valid_number, user_ID_choice, item_id, i, town_name, player_class_name, player_glass_marbles, player_combined_max_health, player_combined_health, player_combined_max_magic, player_combined_magic, player_combined_strength, player_combined_defense, player_combined_agility, player_armor_stats_name, player_armor_stats_health, player_armor_stats_defense, player_armor_stats_agility, player_shield_stats_name, player_shield_stats_health, player_shield_stats_defense, player_shield_stats_agility, player_offhand_weapon_stats_name, player_offhand_weapon_stats_magic, player_offhand_weapon_stats_agility, player_offhand_weapon_stats_strength, player_main_weapon_stats_name, player_main_weapon_stats_magic, player_main_weapon_stats_agility, player_main_weapon_stats_strength, showcase_required_combat, showcase_required_mining, showcase_required_farming, showcase_required_gambling, overall_player_level, overall_level_progress, player_combat_level, player_combat_experience, player_farming_level, player_farming_experience, player_mining_level, player_mining_experience, player_gambling_level, player_gambling_experience, total_number_of_travels, current_number_of_travels, player_main_weapon_ID_carried, player_offhand_weapon_ID_carried, special_weapon_equiped, player_armor_ID_carried, player_shield_ID_carried);
				}	// (UserChoice == "ACCOUNT" || UserChoice == "A") {} END

			}while(!bExitLoop2);
						
			//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
			
			// Resolving the duration in Region
			if (h == 0)
			{
				Random_Number(random_x);

				if (random_x <= 33)
				{
					j = 1;
				}
				else if (random_x <= 66)
				{
					j = 2;
				}
				else
				{
					j = 3;
				}

				// this needs to be in a for loop , bcs of duration of searching area
				for (i = 0 ; i < j ; ++i)
				{
					h = 0;
					bExitLoop2 = false;
					statusofloot = "unfavourable";

					do
					{
						Random_Number(random_x);

						ClearScreen();
						cout << endl;
						cout << "    TRAVELING -> CROSS SECTION -> " << map[user_LOOP_choice].Region_Name << endl;
						cout << "    ------------------------------------------------------------------------" << endl;
						cout << endl;
						//Make an little animations here.
						cout << endl;
						cout << "     Select an area to search:" << endl;
						cout << endl;
						cout << setw(35) << map[user_LOOP_choice].Area1 << endl;
						cout << setw(35) << map[user_LOOP_choice].Area2 << endl;
						cout << setw(35) << map[user_LOOP_choice].Area3 << endl;
						cout << endl;
						cout << endl;
						cout << "    ------------------------------------------------------------------------" << endl;
						cout << "                          " << map[user_LOOP_choice].Area1 << "  |  " << map[user_LOOP_choice].Area2 << "  |  " << map[user_LOOP_choice].Area3 << endl;
						cout << "    ------------------------------------------------------------------------" << endl;
						cout << "    |Your decision: ";

						getline(cin, UserChoice);
						transform(UserChoice.begin(), UserChoice.end(),UserChoice.begin(), toupper);

						temp_char = UserChoice[0];

						//-------------------------------------

						ClearScreen();

						if (UserChoice == map[user_LOOP_choice].Area1 || temp_char == map[user_LOOP_choice].Area1[0])
						{
							h = 1;
							bExitLoop2 = true;

							if (random_x <= 10)
							{
								item_id = 999;
							}

							else if (random_x <= 55)
							{
								item_id = map[user_LOOP_choice].Area1_ID1;
							}

							else
							{
								item_id = map[user_LOOP_choice].Area1_ID2;
							}

						} // (UserChoice == map[user_LOOP_choice].Area1 || temp_char == map[user_LOOP_choice].Area1[0]) {} END

						else if (UserChoice == map[user_LOOP_choice].Area2 || temp_char == map[user_LOOP_choice].Area2[0])
						{
							h = 1;
							bExitLoop2 = true;

							if (random_x <= 20)
							{
								item_id = 999;
							}

							else if (random_x <= 85)
							{
								item_id = map[user_LOOP_choice].Area2_ID1;
							}

							else
							{
								item_id = map[user_LOOP_choice].Area2_ID2;
							}

						} // (UserChoice == map[user_LOOP_choice].Area2 || temp_char == map[user_LOOP_choice].Area2[0]) {} END

						else if (UserChoice == map[user_LOOP_choice].Area3 || temp_char == map[user_LOOP_choice].Area3[0])
						{
							h = 1;
							bExitLoop2 = true;

							if (random_x <= 30)
							{
								item_id = 999;
							}

							else if (random_x <= 75)
							{
								item_id = map[user_LOOP_choice].Area3_ID1;
							}

							else if (random_x <= 95)
							{
								item_id = map[user_LOOP_choice].Area3_ID2;
							}

							else
							{
								item_id = map[user_LOOP_choice].Area3_ID3;
							}

						} // (UserChoice == map[user_LOOP_choice].Area3 || temp_char == map[user_LOOP_choice].Area3[0]) {} END

					}while(!bExitLoop2);

					if (h == 1)
					{
						bExitLoop2 = false;

						if (item_id != 999)
						{
							statusofloot = "successfully";
							item_name = item[item_id - 500].Name;
						}
						
						else
						{
							item_name = "nothing";
						}

						do
						{
							cout << endl;
							cout << "                                    SEARCHING                               " << endl;
							cout << "    ------------------------------------------------------------------------" << endl;
							cout << endl;
							cout << "     You have " << statusofloot << " searched area." << endl;
							cout << endl;

							if (item_id == 999)
							{
								bExitLoop2 = true;
								cout << endl;
								cout << endl;
								cout << "    ------------------------------------------------------------------------" << endl;
								cout << "    |Press ENTER to continue..." << endl;
								cout << "    |";
								WaitForEnter();
							}

							else
							{
								cout << "     Would you like to store " << item_name << "?" << endl;
								cout << endl;
								cout << endl;
								cout << "    ------------------------------------------------------------------------" << endl;
								cout << "                                   YES  |  NO                               "<< endl;
								cout << "    ------------------------------------------------------------------------" << endl;
								cout << "    |Your decision: ";

								getline(cin, UserChoice);
								transform(UserChoice.begin(), UserChoice.end(),UserChoice.begin(), toupper);
							
								//-------------------------------------

								ClearScreen();

								if (UserChoice == "YES" || UserChoice == "Y")
								{
									bExitLoop2 = true;
									holder_add_item(backpack, item_id);
								}

								else if (UserChoice == "NO" || UserChoice == "N")
								{
									bExitLoop2 = true;
								}
							}
						}while(!bExitLoop2);

					} // (h == 1) {} END

				} // (i = 0 ; i < j ; ++i) {} END

			}
						
			// glass marble , enemy, and chirraja skombiniraj nes

			///********************************************************"#=)!"#)=!"=)#(!")=#()=!"(#)!"#)=(!"=#(!")=(#)=!"(#)=
			//location_world = 1;
			
			Random_Number(random_x);
			location_travel = 0;
			total_number_of_travels += 1;
			current_number_of_travels += 1;

			//----------------------------------------

			ClearScreen();
			cout << endl;
			cout << "                                   TRAVELING" << endl;
			cout << "    ------------------------------------------------------------------------" << endl;
			cout << endl;
			cout << "     Traveling through World....." << endl;
			cout << "     Something has been spotted, going to check what it is..." << endl;
			cout << endl;
			cout << "    ------------------------------------------------------------------------" << endl;
			cout << "    |Press ENTER to continue..." << endl;
			cout << "    |";
			WaitForEnter();
			
			//----------------------------------------

			ClearScreen();

			if (random_x <= 20)
			{
				cout << endl;
				cout << "                                   TRAVELING" << endl;
				cout << "    ------------------------------------------------------------------------" << endl;
				cout << endl;
				cout << "     Ouh, too bad. It was only some junk." << endl;
				cout << endl;
				cout << "    ------------------------------------------------------------------------" << endl;
				cout << "    |Press ENTER to continue..." << endl;
				cout << "    |";
				WaitForEnter();
			}

			else	//---------------------------------------- Random x from 21 - 100
			{
				Random_Number(random_x);
				
				if (random_x <= 60)
				{
					player_glass_marbles += 1;
					
					cout << endl;
					cout << "                                   TRAVELING" << endl;
					cout << "    ------------------------------------------------------------------------" << endl;
					cout << endl;

					nsk_file.open("CODE-GlassMarblesOne.nsk");

					if (!nsk_file.good())
					{
						cout << "    |ERROR 007 - Missing 'CODE-GlassMarblesOne.nsk' file." << endl;
						exit (EXIT_FAILURE);
					}
	
					while (getline(nsk_file,drawline))
					{
						code_string(drawline);
						cout << drawline << endl;
					}

					nsk_file.close();

					cout << endl;
					cout << endl;
					cout << "     Woah! Found One Glass Marbles." << endl;
					cout << "     Glass Marbles +1 !" << endl;
					cout << "    ------------------------------------------------------------------------" << endl;
					cout << "    |Press ENTER to continue..." << endl;
					cout << "    |";
					WaitForEnter();
				}

				else if (random_x <= 90)
				{
					player_glass_marbles += 2;
					
					cout << endl;
					cout << "                                   TRAVELING" << endl;
					cout << "    ------------------------------------------------------------------------" << endl;
					cout << endl;

					nsk_file.open("CODE-GlassMarblesOne.nsk");	// File of 2 Marbles

					if (!nsk_file.good())
					{
						cout << "    |ERROR 007 - Missing 'CODE-GlassMarblesOne.nsk' file." << endl;
						exit (EXIT_FAILURE);
					}
	
					while (getline(nsk_file,drawline))
					{
						code_string(drawline);
						cout << drawline << endl;
					}

					nsk_file.close();

					cout << endl;
					cout << endl;
					cout << "     Woah! Found a couple of Glass Marbles." << endl;
					cout << "     Glass Marbles +2 !" << endl;
					cout << "    ------------------------------------------------------------------------" << endl;
					cout << "    |Press ENTER to continue..." << endl;
					cout << "    |";
					WaitForEnter();
				}

				else if (random_x <= 98)
				{
					player_glass_marbles += 4;
					
					cout << endl;
					cout << "                                   TRAVELING" << endl;
					cout << "    ------------------------------------------------------------------------" << endl;
					cout << endl;

					nsk_file.open("CODE-GlassMarblesOne.nsk"); // Files of 4 Marbles

					if (!nsk_file.good())
					{
						cout << "    |ERROR 007 - Missing 'CODE-GlassMarblesOne.nsk' file." << endl;
						exit (EXIT_FAILURE);
					}
	
					while (getline(nsk_file,drawline))
					{
						code_string(drawline);
						cout << drawline << endl;
					}

					nsk_file.close();

					cout << endl;
					cout << endl;
					cout << "     Woah! Found several Glass Marbles." << endl;
					cout << "     Glass Marbles +4 !" << endl;
					cout << "    ------------------------------------------------------------------------" << endl;
					cout << "    |Press ENTER to continue..." << endl;
					cout << "    |";
					WaitForEnter();
				}

				else	//---------------------------------------- random_x is 99 or 100
				{
					player_glass_marbles += 8;
					
					cout << endl;
					cout << "                                   TRAVELING" << endl;
					cout << "    ------------------------------------------------------------------------" << endl;
					cout << endl;

					nsk_file.open("CODE-GlassMarblesOne.nsk"); // File of 8 marbles

					if (!nsk_file.good())
					{
						cout << "    |ERROR 007 - Missing 'CODE-GlassMarblesOne.nsk' file." << endl;
						exit (EXIT_FAILURE);
					}
	
					while (getline(nsk_file,drawline))
					{
						code_string(drawline);
						cout << drawline << endl;
					}

					nsk_file.close();

					cout << endl;
					cout << endl;
					cout << "     Woah! Found a stack of Glass Marbles." << endl;
					cout << "     Glass Marbles +8 !" << endl;
					cout << "    ------------------------------------------------------------------------" << endl;
					cout << "    |Press ENTER to continue..." << endl;
					cout << "    |";
					WaitForEnter();
				}
			}	//---------------------------------------- else {} END

			ClearScreen();
			Random_Number(random_x);
			
			cout << endl;
			cout << "                                   TRAVELING" << endl;
			cout << "    ------------------------------------------------------------------------" << endl;
			cout << endl;
			cout << "     Something is making noise...." << endl;
			cout << "     Going to check what it is..." << endl;
			cout << endl;
			cout << "    ------------------------------------------------------------------------" << endl;
			cout << "    |Press ENTER to continue..." << endl;
			cout << "    |";
			WaitForEnter();
			
			//----------------------------------------

			ClearScreen();
			
			if (random_x <= 40)
			{
				bExitLoop = false;
				do
				{
					location_world = 1;
					
					cout << endl;
					cout << "                                   TRAVELING" << endl;
					cout << "    ------------------------------------------------------------------------" << endl;
					cout << endl;
					cout << "     Ouh, it's only silly Chirraja. Harmless animal." << endl;
					cout << endl;
					cout << endl;
					cout << "    ------------------------------------------------------------------------" << endl;
					cout << "                                  EXIT  |  SKIN                             " << endl;
					cout << "    ------------------------------------------------------------------------" << endl;
					cout << "    |Your decision: ";

					getline(cin, UserChoice);
					transform(UserChoice.begin(), UserChoice.end(),UserChoice.begin(), toupper);
					
					//----------------------------------------

					ClearScreen();
					
					if (UserChoice == "SKIN" || UserChoice == "S")
					{
						bExitLoop = true;
						holder_space_counter (backpack,backpack_free_space);

						if (backpack_free_space >= 1)
						{
							item_id = 521; // ITEM ID HERE
							holder_add_item (backpack, item_id);
							cout << endl;
							cout << "                                    SKINNING" << endl;
							cout << "    ------------------------------------------------------------------------" << endl;
							cout << endl;
							cout << "     You have successfully skinned Chirraja." << endl;
							cout << "     You have received 1 Chirraja Fur." << endl;
							cout << endl;
							cout << "    ------------------------------------------------------------------------" << endl;
							cout << "    |Press ENTER to continue..." << endl;
							cout << "    |";
							WaitForEnter();
						}

						else
						{
							cout << endl;
							cout << "                                     FIZZLE" << endl;
							cout << "    ------------------------------------------------------------------------" << endl;
							cout << endl;
							cout << "     You have no free space in backpack." << endl;
							cout << endl;
							cout << "    ------------------------------------------------------------------------" << endl;
							cout << "    |Press ENTER to continue..." << endl;
							cout << "    |";
							WaitForEnter();
						}

					}

					else if (UserChoice == "EXIT" || UserChoice == "E")
					{
						bExitLoop = true;
					}

				}while(!bExitLoop);		// do-while loop for bad user input

			}

			else	//---------------------------------------- random_x is from 41-100 , starting fight
			{
				location_battle = 1;
				total_player_encounter += 1;
				current_player_encounter += 1;
				
				Random_Number(random_x);
				Random_Number(enemy_random_x);
				
				i = random_x / 10;

				//----------------------------------------

				if (i == 0)
				{
					i = 1;
				}

				else if (i >= player_combat_level+2)
				{
					i = player_combat_level +1;
				}

				//----------------------------------------

				switch (i)
				{
				case 1:
					enemy_class_name = "Gileeran Bear";
					Enemy_Stats_Level_1 (enemy_base_max_health, enemy_base_health, enemy_base_max_magic, enemy_base_magic, enemy_base_strength, enemy_base_defense, enemy_base_agility, enemy_level, enemy_potions, enemy_experience, enemy_itemID1, enemy_itemID1chance, enemy_itemID2, enemy_itemID2chance);
					break;

				case 2:
					enemy_class_name = "Yumar Hunter";
					Enemy_Stats_Level_2 (enemy_base_max_health, enemy_base_health, enemy_base_max_magic, enemy_base_magic, enemy_base_strength, enemy_base_defense, enemy_base_agility, enemy_level, enemy_potions, enemy_experience, enemy_itemID1, enemy_itemID1chance, enemy_itemID2, enemy_itemID2chance);
					break;

				case 3:
					enemy_class_name = "Mindless Skeleton";
					Enemy_Stats_Level_3 (enemy_base_max_health, enemy_base_health, enemy_base_max_magic, enemy_base_magic, enemy_base_strength, enemy_base_defense, enemy_base_agility, enemy_level, enemy_potions, enemy_experience, enemy_itemID1, enemy_itemID1chance, enemy_itemID2, enemy_itemID2chance);
					break;

				case 4:
					enemy_class_name = "Sabre-toothed Tiger";
					Enemy_Stats_Level_4 (enemy_base_max_health, enemy_base_health, enemy_base_max_magic, enemy_base_magic, enemy_base_strength, enemy_base_defense, enemy_base_agility, enemy_level, enemy_potions, enemy_experience, enemy_itemID1, enemy_itemID1chance, enemy_itemID2, enemy_itemID2chance);
					break;

				case 5:
					enemy_class_name = "Sparklefree Vampire";
					Enemy_Stats_Level_5 (enemy_base_max_health, enemy_base_health, enemy_base_max_magic, enemy_base_magic, enemy_base_strength, enemy_base_defense, enemy_base_agility, enemy_level, enemy_potions, enemy_experience, enemy_itemID1, enemy_itemID1chance, enemy_itemID2, enemy_itemID2chance);
					break;

				case 6:
					enemy_class_name = "Vermoor Assassin";
					Enemy_Stats_Level_6 (enemy_base_max_health, enemy_base_health, enemy_base_max_magic, enemy_base_magic, enemy_base_strength, enemy_base_defense, enemy_base_agility, enemy_level, enemy_potions, enemy_experience, enemy_itemID1, enemy_itemID1chance, enemy_itemID2, enemy_itemID2chance);
					break;

				case 7:
					enemy_class_name = "Sharawin Spellbinder";
					Enemy_Stats_Level_7 (enemy_base_max_health, enemy_base_health, enemy_base_max_magic, enemy_base_magic, enemy_base_strength, enemy_base_defense, enemy_base_agility, enemy_level, enemy_potions, enemy_experience, enemy_itemID1, enemy_itemID1chance, enemy_itemID2, enemy_itemID2chance);
					break;

				case 8:
					enemy_class_name = "Teddycaring Giant";
					Enemy_Stats_Level_8 (enemy_base_max_health, enemy_base_health, enemy_base_max_magic, enemy_base_magic, enemy_base_strength, enemy_base_defense, enemy_base_agility, enemy_level, enemy_potions, enemy_experience, enemy_itemID1, enemy_itemID1chance, enemy_itemID2, enemy_itemID2chance);
					break;

				case 9:
					enemy_class_name = "Earthling Scout";
					Enemy_Stats_Level_9 (enemy_base_max_health, enemy_base_health, enemy_base_max_magic, enemy_base_magic, enemy_base_strength, enemy_base_defense, enemy_base_agility, enemy_level, enemy_potions, enemy_experience, enemy_itemID1, enemy_itemID1chance, enemy_itemID2, enemy_itemID2chance);
					break;

				case 10:
					enemy_class_name = "Enchanted Bunny";
					Enemy_Stats_Level_10 (enemy_base_max_health, enemy_base_health, enemy_base_max_magic, enemy_base_magic, enemy_base_strength, enemy_base_defense, enemy_base_agility, enemy_level, enemy_potions, enemy_experience, enemy_itemID1, enemy_itemID1chance, enemy_itemID2, enemy_itemID2chance);
					break;
				}	//switch statment of (i) END

				//----------------------------------------

				cout << endl;
				cout << "                                   TRAVELING" << endl;
				cout << "    ------------------------------------------------------------------------" << endl;
				cout << endl;
				cout << "     Uh-Oh! Wild " << enemy_class_name << " has appeared !" << endl;
				cout << "     Entering combat..." << endl;
				cout << endl;
				cout << "    ------------------------------------------------------------------------" << endl;
				cout << "    |Press ENTER to continue..." << endl;
				cout << "    |";
				WaitForEnter();

				//----------------------------------------

				//----Apply Random Weapon / Armor To Enemy
				//Function here, random number, ID and read from DB armor and Weapon
				//
				
				//----------------------------------------

				Refresh_Equipment_Stats(enemy_combined_health, enemy_combined_max_health, enemy_combined_magic, enemy_combined_max_magic, enemy_combined_strength, enemy_combined_defense, enemy_combined_agility, enemy_base_health, enemy_base_magic, enemy_base_strength, enemy_base_defense, enemy_base_agility, enemy_armor_stats_health,enemy_armor_stats_defense, enemy_armor_stats_agility, 0, 0, 0, enemy_weapon_stats_magic,enemy_weapon_stats_strength, enemy_weapon_stats_agility, 0, 0, 0);

			}	//---------------------------------------- else {} END	

		}	//----------------------------------------Player Location: TRAVELING {} END

		//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		////:::::::::::::::::::::---------------------------------------------------------
		//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

		while (location_battle == 1)
		{	//----------------------------------------Player Location: BATTLE {} START

			enemy_turn = 0;
			player_turn = 1;
			calculation_turn = 0;
			player_taunted = 0;
			enemy_taunted = 0;
			tried_to_escape_turn = 0;
			player_escape_successful = 0;
			bExitLocation = false;
			total_damage_output = 0;
			total_damage_recived = 0;
			location_battle = 0;

			do
			{	
				ClearScreen();

				//---------------------------------------- Resetting Choice Tracker
				player_escape = 0;
				player_attack_super = 0;
				player_attack_normal = 0;

				enemy_escape = 0;
				enemy_attack_super = 0;
				enemy_attack_normal = 0;

				//---------------------------------------- Resetting Variables
				total_damage_output += player_damage;
				total_damage_recived += enemy_damage;

				enemy_damage = 0;
				player_damage = 0;

				//----------------------------------------

				if (player_combined_health > 0)
				{
					if (enemy_combined_health > 0)
					{
						if (player_escape_successful == 0)
						{
							if (tried_to_escape_turn == 0)
							{
								while (player_turn == 1)
								{
									player_turn = 0;
									enemy_turn = 1;
									bExitLoop = false;

									do
									{
										Random_Number(enemy_random_x);
	
										ClearScreen();
										cout << endl;
										cout << "                                     BATTLE" << endl;
										cout << "    ------------------------------------------------------------------------" << endl;
										cout << "            " << setw(9) << player_class_name << setw(22) << "VERSUS" << setw(16) << ' ' << enemy_class_name << endl;
										cout << "                      ------------------------------------" << endl;
										cout << setw(15) << player_combined_health << " / " << setw(3) <<  player_combined_max_health << setw(22) << "HEALTH" << setw(16) << ' ' << enemy_combined_health << " / " << enemy_combined_max_health << endl;
										cout << setw(15) << player_combined_magic << " / " << setw(3) <<  player_combined_max_magic << setw(22) << "MAGIC" << setw(16) << ' ' << enemy_combined_magic << " / " << enemy_combined_max_magic << endl;
										cout << "               " << setw(6) << player_combined_strength << setw(22) << "STRENGTH" << setw(16) << ' ' << enemy_combined_strength << endl;
										cout << "               " << setw(6) << player_combined_defense  << setw(22) << "DEFENSE" << setw(16) << ' ' << enemy_combined_defense << endl;
										cout << "               " << setw(6) << player_combined_agility << setw(22) << "AGILITY" << setw(16) << ' ' << enemy_combined_agility << endl;
										cout << "                      ------------------------------------" << endl;
										cout << "               " << setw(6) << player_potions << setw(22) << "POTIONS" << setw(16) << ' ' << enemy_potions << endl;
										cout << "                      ------------------------------------" << endl;
										cout << endl;
										cout << endl;
										cout << "    ------------------------------------------------------------------------" << endl;
										cout << "                      ATTACK  |  " << special_attack << "  |  ESCAPE  |  POTION" << endl;
										cout << "    ------------------------------------------------------------------------" << endl;
										cout << "    |Your decision: ";
											
										getline(cin, UserChoice);
										transform(UserChoice.begin(), UserChoice.end(),UserChoice.begin(), toupper);

										//----------------------------------------

										if (UserChoice == "POTION" || UserChoice == "P")
										{
											bExitLoop2 = false;
	
											do
											{
												ClearScreen();
												cout << endl;
												cout << "    BATTLE -> SELECT A POTION                                               " << endl;
												cout << "    ------------------------------------------------------------------------" << endl;

												combine_potion_kit (potion_kit, potionkit_names, potion);
												potion_kit_open(potionkit_names);

												for (i = 0 ; i < (int) potion_kit.size() ; ++i)
												{
													if (potion_kit[i].ID == 999)
													{
														break;
													}
												}

												cout << "    ------------------------------------------------------------------------" << endl;
												cout << setw (27) << "HEALTH: " << setw(3) << player_combined_health << "/" << setw(3) << player_combined_max_health << setw(20) << "MAGIC: " << setw(3) << player_combined_magic << "/" << setw(3) << player_combined_max_magic << endl;
												cout << "    ------------------------------------------------------------------------" << endl;
												cout << "                                  (IDN)  |  EXIT                             " << endl;
												cout << "    ------------------------------------------------------------------------" << endl;
												cout << "    |Your decision: ";
							
												getline(cin, UserChoice);
												user_valid_number = is_number(UserChoice);
												user_ID_choice = atoi(UserChoice.c_str());
												transform(UserChoice.begin(), UserChoice.end(),UserChoice.begin(), toupper);
						
												ClearScreen();
	
												if (UserChoice == "EXIT" || UserChoice == "E")
												{
													bExitLoop2 = true;
												}

												else if (user_valid_number == 1)	
												{
													if (user_ID_choice < i)
													{
														item_id = potion_kit[user_ID_choice].ID;
														Use_Restoration_Potion (player_combined_health, player_combined_max_health, player_combined_magic, player_combined_max_magic, item_id, potion_kit, potion);
													}

													else if (user_ID_choice < (int) potion_kit.size())
													{
														cout << endl;
														cout << "                                     FIZZLE" << endl;
														cout << "    ------------------------------------------------------------------------" << endl;
														cout << endl;
														cout << "     No item in this potion kit slot." << endl;
														cout << endl;
														cout << "    ------------------------------------------------------------------------" << endl;
														cout << "    |Press ENTER to continue..." << endl;
														cout << "    |";
														WaitForEnter();
													}

													else if (user_ID_choice >= (int) potion_kit.size())
													{
														cout << endl;
														cout << "                                     FIZZLE" << endl;
														cout << "    ------------------------------------------------------------------------" << endl;
														cout << endl;
														cout << "     Your potion kit does not have so much slots." << endl;
														cout << endl;
														cout << "    ------------------------------------------------------------------------" << endl;
														cout << "    |Press ENTER to continue..." << endl;
														cout << "    |";
														WaitForEnter();
													}

												}	// Valid Number Checker  {} END

												else
												{
													cout << endl;
													cout << "                                     FIZZLE" << endl;
													cout << "    ------------------------------------------------------------------------" << endl;
													cout << endl;
													cout << "     Please type in the correct IDN of Potion." << endl;
													cout << endl;
													cout << "    ------------------------------------------------------------------------" << endl;
													cout << "    |Press ENTER to continue..." << endl;
													cout << "    |";
													WaitForEnter();
												}

											} while(!bExitLoop2);	// do-while loop for Bad Commands

										}	//---------------------------------------- if "POTION" {} END
					
										//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

										else if (UserChoice == "ATTACK" || UserChoice == "A")
										{
											bExitLoop = true;
											player_attack_normal = 1;
										}	//---------------------------------------- else if "ATTACK" {} END

										//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

										else if (UserChoice == "CAST" || UserChoice == "C" || UserChoice == "FURY" || UserChoice == "F" || UserChoice == "RAGE" || UserChoice == "R")
										{
											if (player_combined_magic >= (90 * (0.5 + (0.05 * player_combat_level))) )
											{
												bExitLoop = true;
												player_attack_super = 1;
												player_combined_magic -= static_cast<int>(90 * (0.5 + (0.05 * player_combat_level)));
											}

											else
											{
												cout << endl << endl << endl;
												cout << "                                     FIZZLE" << endl;
												cout << "    ------------------------------------------------------------------------" << endl;
												cout << endl;
												cout << "     Not enough Magic to cast special attack. You need " << (90 * (0.5 + (0.05 * player_combat_level))) << " Magic to cast it." << endl;
												cout << endl;
												cout << "    ------------------------------------------------------------------------" << endl;
												cout << "    |Press ENTER to continue..." << endl;
												cout << "    |";
												WaitForEnter();
											}
										}	//---------------------------------------- else if "SPECIALATTACK" {} END

										//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

										else if (UserChoice == "ESCAPE" || UserChoice == "E")
										{
											if (player_taunted == 0)
											{
												bExitLoop = true;
												player_escape = 1;
											}

											else
											{
												cout << endl << endl << endl << endl << endl << endl << endl << endl;
												cout << "                                 ATTEMPT TO ESCAPE                            " << endl;
												cout << "    ------------------------------------------------------------------------" << endl;
												cout << endl;
												cout << "     " << enemy_class_name << " saw that you were planning to escape again" << endl;
												cout << "     and gently shamed his head. " << endl;
												cout << endl;
												cout << "     You know You have no chance of doing that again now..." << endl;
												cout << endl;
												cout << "    ------------------------------------------------------------------------" << endl;
												cout << "    |Press ENTER to continue..." << endl;
												cout << "    |";
												WaitForEnter();
											}
										}

										//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

									}while(!bExitLoop);	// do-while loop for bad commands

									player_taunted -= 1;

									if (player_taunted <= 0)
									{
										player_taunted = 0;
									}

								}	//----------------------------------------PLAYER_TURN {} END


								//::::::::::::::::::::::::::::::
								//::::::::::::::::::::::::::::::
								//::::::::::::::::::::::::::::::

								while (enemy_turn == 1)
								{
									enemy_turn = 0;

									//----------------------------------------Enemy uses Potion

									if (enemy_random_x <= 33 && enemy_combined_health <= (enemy_combined_max_health/2))
									{
										enemy_turn = 1;
										Random_Number(enemy_random_x);

										if(enemy_potions >= 1)
										{
											enemy_potions -= 1;
											enemy_combined_health += 20;

											if(enemy_combined_health > enemy_combined_max_health)
											{
												enemy_combined_health = enemy_combined_max_health;
											}
										}

										else
										{
											continue;
										}
									}

									//----------------------------------------Enemy Uses Attack

									else if (enemy_random_x <= 100) // enemy use attack
									{
										calculation_turn = 1;
										enemy_attack_normal = 1;
									}
								}	//---------------------------------------- ENEMY_TURN {} END

								//::::::::::::::::::::::::::::::
								//::::::::::::::::::::::::::::::
								//::::::::::::::::::::::::::::::

								while (calculation_turn == 1)
								{
									player_turn = 1; // CAN BE PROBLEM ***************************************************************************
									calculation_turn = 0;

									cout << endl << endl << endl << endl << endl << endl << endl << endl;
									cout << "                                 COMBAT RESULTS" << endl;
									cout << "    ------------------------------------------------------------------------" << endl;
									cout << endl;
									cout << "            " << setw(9) << player_class_name << setw(22) << "      " << setw(16) << ' ' << enemy_class_name << endl;
									cout << "          " << setw(9);

									//----------------------------------------

									// DODAJ IF TAUNTED DA NE RADI NIS OVU RUNDU!!!

									if (player_escape == 1 && enemy_escape == 1)
									{
										Escape_Attempt (player_combat_level, enemy_level, player_combined_defense, player_escape_successful, player_taunted);

										if (player_escape_successful == 0)
										{
											tried_to_escape_turn = 1;
											cout << "TAUNTED" << "      " << setw(32) << ' ';
											Super_Damage_Calculator (enemy_damage, enemy_combined_max_health, enemy_combined_max_magic, enemy_combined_agility, enemy_level, player_combined_health);
										}

										// Enemy Escape Required
									}

									else if (player_escape == 1 && enemy_attack_normal == 1)
									{
										Escape_Attempt (player_combat_level, enemy_level, player_combined_defense, player_escape_successful, player_taunted);

										if (player_escape_successful == 0)
										{
											tried_to_escape_turn = 1;
											cout << "TAUNTED" << "      " << setw(32) << ' ';
											Super_Damage_Calculator (enemy_damage, enemy_combined_max_health, enemy_combined_max_magic, enemy_combined_agility, enemy_level, player_combined_health);
										}

									}

									else if (player_escape == 1 && enemy_attack_super == 1)
									{
										Escape_Attempt (player_combat_level, enemy_level, player_combined_defense, player_escape_successful, player_taunted);

										if (player_escape_successful == 0)
										{
											tried_to_escape_turn = 1;
											cout << "TAUNTED" << "      " << setw(32) << ' ';
											Super_Damage_Calculator (enemy_damage, enemy_combined_max_health, enemy_combined_max_magic, enemy_combined_agility, enemy_level, player_combined_health);
										}

									}

									else if (player_attack_normal == 1 && enemy_escape == 1)
									{
										// Enemy Escape Required
									}

									else if (player_attack_normal == 1 && enemy_attack_normal == 1)
									{
										Damage_Calculator (player_damage, player_combined_strength, player_combined_agility, player_combat_level, enemy_combined_health, enemy_combined_defense, enemy_combined_agility);
										cout << "      " << setw(32) << ' ';
										Damage_Calculator (enemy_damage, enemy_combined_strength, enemy_combined_agility, enemy_level, player_combined_health, player_combined_defense, player_combined_agility);
									}

									else if (player_attack_normal == 1 && enemy_attack_super == 1)
									{
										Damage_Calculator (player_damage, player_combined_strength, player_combined_agility, player_combat_level, enemy_combined_health, enemy_combined_defense, enemy_combined_agility);
									
										cout << "      " << setw(32) << ' ';
										Super_Damage_Calculator (enemy_damage, enemy_combined_max_health, enemy_combined_max_magic, enemy_combined_agility, enemy_level, player_combined_health);
									}

									else if (player_attack_super == 1 && enemy_escape == 1)
									{
										// Enemy Escape Required
									}

									else if (player_attack_super == 1 && enemy_attack_normal == 1)
									{
										Super_Damage_Calculator (player_damage, player_combined_max_health, player_combined_max_magic, player_combined_agility, player_combat_level, enemy_combined_health);
										cout << "      " << setw(32) << ' ';
										Damage_Calculator (enemy_damage, enemy_combined_strength, enemy_combined_agility, enemy_level, player_combined_health, player_combined_defense, player_combined_agility);
									}

									else if (player_attack_super == 1 && enemy_attack_super == 1)
									{
										Super_Damage_Calculator (player_damage, player_combined_max_health, player_combined_max_magic, player_combined_agility, player_combat_level, enemy_combined_health);
										cout << "      " << setw(32) << ' ';
										Super_Damage_Calculator (enemy_damage, enemy_combined_max_health, enemy_combined_max_magic, enemy_combined_agility, enemy_level, player_combined_health);
									}

									else
									{
										ClearScreen();
										cout << "    |ERROR 110 - Battle Engine Malfunction. Quit Game!" << endl;
									}

									//----------------------------------------
									if (player_escape_successful == 0)
									{
										cout << endl;
										cout << endl;
										cout << "    ------------------------------------------------------------------------" << endl;
										cout << "    |Press ENTER to continue..." << endl;
										cout << "    |";
										WaitForEnter();
									}

									else
									{
										continue;
									}

								}	//---------------------------------------- CALCULATION OF BATTLE {} END

							}

							else //----------------------------------------Player tried to run away
							{
								tried_to_escape_turn = 0;

								cout << endl;
								cout << "                                 ATTEMPT TO ESCAPE                            " << endl;
								cout << "    ------------------------------------------------------------------------" << endl;
								cout << endl;
								cout << "     You have attempted to escape the battle versus " << enemy_class_name << "." << endl;
								cout << endl;
								cout << "     As You turned back to run, " << enemy_class_name << " has attacked" << endl;
								cout << "     You on Your back, and you failed to escape." << endl;
								cout << endl;
								cout << "    ------------------------------------------------------------------------" << endl;
								cout << "    |Press ENTER to continue..." << endl;
								cout << "    |";
								WaitForEnter();
							}

						} // Escaped failed end
						
						else
						{
							location_world = 1;
							bExitLocation = true;

							Combat_Level_Checkup(player_class_name, int(enemy_experience/3), player_combat_level, player_combat_experience, player_base_max_health, player_base_health, player_base_max_magic, player_base_magic, player_base_strength, player_base_defense, player_base_agility);

							ClearScreen();
							cout << endl;
							cout << "                                     ESCAPED                                " << endl;
							cout << "    ------------------------------------------------------------------------" << endl;
							cout << endl;
							cout << "     You have escaped the battle versus " << enemy_class_name << "." << endl;
							cout << endl;
							cout << "	                           BATTLE REPORT" << endl;
							cout << "     Total damage done: " << total_damage_output << endl;
							cout << "     Total damage received: " << total_damage_recived << endl;
							cout << endl;
							cout << "    ------------------------------------------------------------------------" << endl;
							cout << "    |Press ENTER to continue..." << endl;
							cout << "    |";
							WaitForEnter();
						}

					} // Enemy HP above 0 end

					//----------------------------------------

					else //---------------------------------------- Enemy HP 0 or Less = Won Fight
					{
						Combat_Level_Checkup(player_class_name, enemy_experience, player_combat_level, player_combat_experience, player_base_max_health, player_base_health, player_base_max_magic, player_base_magic, player_base_strength, player_base_defense, player_base_agility);
						location_world = 1;
						bExitLocation = true;

						bExitLoop = false;

						do
						{
							ClearScreen();
							cout << endl;
							cout << "                                     VICTORY                                " << endl;
							cout << "    ------------------------------------------------------------------------" << endl;
							cout << endl;
							cout << "     You have won the battle versus " << enemy_class_name << "." << endl;
							cout << endl;
							cout << endl;
							cout << "	                           BATTLE REPORT" << endl;
							cout << "     Total damage done: " << total_damage_output << endl;
							cout << "     Total damage received: " << total_damage_recived << endl;
							cout << endl;
							cout << endl;
							cout << "    ------------------------------------------------------------------------" << endl;
							cout << "                                  EXIT  |  LOOT                             " << endl;
							cout << "    ------------------------------------------------------------------------" << endl;
							cout << "    |Your decision: ";

							getline(cin, UserChoice);
							transform(UserChoice.begin(), UserChoice.end(),UserChoice.begin(), toupper);

							//----------------------------------------

							ClearScreen();

							if (UserChoice == "EXIT" || UserChoice == "E")
							{
								bExitLoop = true;
							}

							else if (UserChoice == "LOOT" || UserChoice == "L")
							{
								bExitLoop = true;
								holder_space_counter (backpack,backpack_free_space);
								
								Random_Number (random_x);
								Random_Number (enemy_random_x);
								statusofloot = "unfavourable";
								
								if ((random_x <= enemy_itemID1chance || enemy_random_x <= enemy_itemID2chance) && backpack_free_space >= 1)
								{
									statusofloot = "successfully";
								}
								
								cout << endl;
								cout << "                                     LOOTING                                " << endl;
								cout << "    ------------------------------------------------------------------------" << endl;
								cout << endl;
								cout << "     You have " << statusofloot << " looted " << enemy_class_name << endl;
								cout << endl;
								
								if (statusofloot == "successfully")
								{
									cout << "     You have received the following: " << endl;
									cout << "                                    " << endl;
									
									if (backpack_free_space >= 2)
									{
										if (random_x <= enemy_itemID1chance)
										{
											find_item_name_by_id (enemy_itemID1, item_name, armor, weapon, item);
											holder_add_item (backpack, enemy_itemID1);
											cout << "                                    -" << item_name << endl;
											
										}

										if (enemy_random_x <= enemy_itemID2chance)
										{
											find_item_name_by_id (enemy_itemID2, item_name, armor, weapon, item);
											holder_add_item (backpack, enemy_itemID2);
											cout << "                                    -" << item_name << endl;
										}
									}
									else if (backpack_free_space == 1)
									{
										if (random_x <= 50)
										{
											find_item_name_by_id (enemy_itemID1, item_name, armor, weapon, item);
											holder_add_item (backpack, enemy_itemID1);
											cout << "                                    -" << item_name << endl;
										}
										else
										{
											find_item_name_by_id (enemy_itemID2, item_name, armor, weapon, item);
											holder_add_item (backpack, enemy_itemID2);
											cout << "                                    -" << item_name << endl;
										}
									}
									
									cout << endl;
								}
								
								cout << "    ------------------------------------------------------------------------" << endl;
								cout << "    |Press ENTER to continue..." << endl;
								cout << "    |";
								WaitForEnter();
							}

						}while(!bExitLoop);		// do-while loop for bad user inputs

					}	// Won Fight {} END

				}	// Player Health above 0 {} END

				//----------------------------------------

				else //---------------------------------------- Player HP 0 or Less = GAME OVER
				{
					bExitLocation = true;
					location_gameover = 1;
				}

			}while (!bExitLocation);	// do-while Loop for Location {} END

		}	//----------------------------------------Player Location: BATTLE {} END

		//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		////:::::::::::::::::::::---------------------------------------------------------
		//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

		while (location_world == 1)
		{	//----------------------------------------Player Location: WORLD {} START
			bExitLocation = false;
			location_world = 0;

			do
			{
				ClearScreen();
				cout << endl;
				cout << "    WORLD" << endl;
				cout << "    ------------------------------------------------------------------------" << endl;
				cout << endl;
				cout << "    From the highest point of the mountains of Karst one could see for many" << endl;
				cout << "    leagues. To the north, Opimaesta was a verdant green tapestry. From" << endl;
				cout << "    east to west one could trace the flow of a mighty river, the tops of" << endl;
				cout << "    the trees formed a thick canopy in many places." << endl;
				cout << endl;
				cout << "    What could these teeming jungles and forests hide?" << endl;
				cout << endl;
				cout << "    The plants grew in clay soil and in the mountains, rugged bushed grew" << endl;
				cout << "    from shale. Below the treetops adventurers and travellers fought over" << endl;
				cout << "    deposits of precious volcanic sand." << endl;
				cout << endl;
				cout << "    To the south of the great mountains, was the sea. Bright and turquoise," << endl;
				cout << "    the ocean teemed with life. There were scattered islands where strange" << endl;
				cout << "    beings lived." << endl;
				cout << endl;
				cout << "    To the north of the jungle, the land became rolling hills and valleys." << endl;
				cout << endl;
				cout << "    Overall Opimaesta was home to many peoples and creatures. Humans were" << endl;
				cout << "    by far the most numerous. In the deep forests there were the green" << endl;
				cout << "    elves and dryads, they mostly kept to themselves. The meadows had all" << endl;
				cout << "    kinds of races occupying the fertile farmland area. Nestled within each" << endl;
				cout << "    valley there were towns, some larger than others. They were places of" << endl;
				cout << "    trade and commerce where many of the races of Opimaesta came together."  << endl;
				cout << "    One could find warriors and adventurers that came to buy from the" << endl;
				cout << "    crafters found at such settlements." << endl;
				cout << endl;
				cout << endl;
				cout << "    ------------------------------------------------------------------------" << endl;
				cout << "                        CAMP | TOWN | JOURNEY | ACCOUNT                     " << endl;
				cout << "    ------------------------------------------------------------------------" << endl;
				cout << "    |Your decision: ";

				getline(cin, UserChoice);
				transform(UserChoice.begin(), UserChoice.end(),UserChoice.begin(), toupper);

				//----------------------------------------

				ClearScreen();

				if (UserChoice == "ACCOUNT" || UserChoice == "A")
				{
					
					access_account("WORLD", backpack, backpack_names, backpack_free_space, armor, weapon, item, potion, potion_kit, potionkit_names, potionkit_free_space, UserChoice, user_valid_number, user_ID_choice, item_id, i, town_name, player_class_name, player_glass_marbles, player_combined_max_health, player_combined_health, player_combined_max_magic, player_combined_magic, player_combined_strength, player_combined_defense, player_combined_agility, player_armor_stats_name, player_armor_stats_health, player_armor_stats_defense, player_armor_stats_agility, player_shield_stats_name, player_shield_stats_health, player_shield_stats_defense, player_shield_stats_agility, player_offhand_weapon_stats_name, player_offhand_weapon_stats_magic, player_offhand_weapon_stats_agility, player_offhand_weapon_stats_strength, player_main_weapon_stats_name, player_main_weapon_stats_magic, player_main_weapon_stats_agility, player_main_weapon_stats_strength, showcase_required_combat, showcase_required_mining, showcase_required_farming, showcase_required_gambling, overall_player_level, overall_level_progress, player_combat_level, player_combat_experience, player_farming_level, player_farming_experience, player_mining_level, player_mining_experience, player_gambling_level, player_gambling_experience, total_number_of_travels, current_number_of_travels, player_main_weapon_ID_carried, player_offhand_weapon_ID_carried, special_weapon_equiped, player_armor_ID_carried, player_shield_ID_carried);
				
				}	// (UserChoice == "ACCOUNT" || UserChoice == "A") {} END

				//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

				else if (UserChoice == "CAMP" || UserChoice == "C")
				{
					bExitLocation = true;
					location_camp = 1;

					cout << endl;
					cout << "                                   CAMP" << endl;
					cout << "    ------------------------------------------------------------------------" << endl;
					cout << endl;
					cout << "     Preparing current area for camping. On a lookout for nearby activity." << endl;
					cout << endl;
					cout << "    ------------------------------------------------------------------------" << endl;
					cout << "    |Press ENTER to continue..." << endl;
					cout << "    |";
					WaitForEnter();
				}	//---------------------------------------- else if "CAMP" {} END

				//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

				else if (UserChoice == "TOWN" || UserChoice == "T")
				{	
					if (current_number_of_travels <= 1)
					{
						cout << endl;
						cout << "                               PREPARING FOR TOWN" << endl;
						cout << "    ------------------------------------------------------------------------" << endl;
						cout << endl;
						cout << "     Unfortunately, the town is still too far. Couple more travels until it" << endl;
						cout << "     can be reached." << endl;
						cout << endl;
						cout << "    ------------------------------------------------------------------------" << endl;
						cout << "    |Press ENTER to continue..." << endl;
						cout << "    |";
						WaitForEnter();
					}

					else if (current_number_of_travels <= 2 )
					{
						cout << endl;
						cout << "                               PREPARING FOR TOWN" << endl;
						cout << "    ------------------------------------------------------------------------" << endl;
						cout << endl;
						cout << "     Unfortunately, the town is still to far. One more travel until it can" << endl;
						cout << "     be reached." << endl;
						cout << endl;
						cout << "    ------------------------------------------------------------------------" << endl;
						cout << "    |Press ENTER to continue..." << endl;
						cout << "    |";
						WaitForEnter();
					}

					else
					{
						bExitLocation = true;
						location_town = 1;
						
						cout << endl;
						cout << "                               PREPARING FOR TOWN" << endl;
						cout << "    ------------------------------------------------------------------------" << endl;
						cout << endl;
						cout << "     Preparing to travel to near by town" << endl;
						cout << endl;
						cout << "    ------------------------------------------------------------------------" << endl;
						cout << "    |Press ENTER to continue..." << endl;
						cout << "    |";
						WaitForEnter();
					}

				}	//---------------------------------------- else if "TOWN" {} END

				//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

				else if (UserChoice == "JOURNEY" || UserChoice == "J")
				{
					bExitLocation = true;
					location_travel = 1;
					
					cout << endl;
					cout << "                                   TRAVELING" << endl;
					cout << "    ------------------------------------------------------------------------" << endl;
					cout << endl;
					cout << "     Leaving current area. Setting journey into unknown." << endl;
					cout << "     While traveling, there is a possibility of encountering enemies." << endl;
					cout << "     On other hand, stumbling upon Glass Marbles is also possibility." << endl;
					cout << endl;
					cout << "     Keep an eye for Gileeran Bear, their hide is very useful and precious." << endl;
					cout << endl;
					cout << "    ------------------------------------------------------------------------" << endl;
					cout << "    |Press ENTER to continue..." << endl;
					cout << "    |";
					WaitForEnter();
				}	//---------------------------------------- else if "JOURNEY" {} END

				//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

			}while(!bExitLocation);		// do-while Loop for Location {} END

		}	//----------------------------------------Player Location: WORLD {} END

		//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		////:::::::::::::::::::::---------------------------------------------------------
		//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

		while (location_gameover == 1)
		{	//----------------------------------------Player Location: GAMEOVER {} START

			location_checkup = 0;
			location_gameover = 0;
			//-------------------------------------

			ClearScreen();
			cout << endl;
			cout << "                                   GAME-OVER" << endl;
			cout << "    ------------------------------------------------------------------------" << endl;
			cout << endl;
			cout << "		From here, and now the adventure of " << player_class_name << " ends." << endl;
			cout << endl;
			cout << "                                     R.I.P." << endl;
			cout << endl;
			cout << "                                   Score: 0" << endl;
			cout << endl << endl << endl;
			cout << "    ------------------------------------------------------------------------" << endl;
			cout << "    |Press ENTER to continue..." << endl;
			cout << "    |";
			WaitForEnter();

			Thread_Date_System.join(); // Killing a Time-Date Thread
			exit (EXIT_SUCCESS);	// Shut Down Application
		}	//----------------------------------------Player Location: GAMEOVER {} END

		//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		////:::::::::::::::::::::---------------------------------------------------------
		//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	} while (location_checkup == 1);	//-------------------------------------Player Location Game Loop {} END

	return 0;

}	//-------------------------------------main Function {} END

//-------------------------------------Functions

void ClearScreen()
{
	HANDLE                     hStdOut;
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	DWORD                      count;
	DWORD                      cellCount;
	COORD                      homeCoords = { 0, 0 };
	
	hStdOut = GetStdHandle( STD_OUTPUT_HANDLE );
	if (hStdOut == INVALID_HANDLE_VALUE) return;

	//	Get the number of cells in the current buffer
	if (!GetConsoleScreenBufferInfo( hStdOut, &csbi )) return;
	cellCount = csbi.dwSize.X *csbi.dwSize.Y;

	//	Fill the entire buffer with spaces
	if (!FillConsoleOutputCharacter(
		hStdOut,
		(TCHAR) ' ',
		cellCount,
		homeCoords,
		&count
		)) return;

	//	Fill the entire buffer with the current colors and attributes
	if (!FillConsoleOutputAttribute(
		hStdOut,
		csbi.wAttributes,
		cellCount,
		homeCoords,
		&count
		)) return;

	//	Move the cursor home

	SetConsoleCursorPosition( hStdOut, homeCoords );
}

void WaitForEnter()
{
	int wait;
	wait = getchar();
	if (wait == 0 || wait == 224)
	{
		getchar();
	}
}

void Is_Positive (int number)
{
	if (number > 0)
	{
		cout << "+";
	}
}

int Random_Number(int& number)
{
	number = rand() % 100+1;
	return number;
}

bool is_number(const string& s)
{
    string::const_iterator it = s.begin();
    while (it != s.end() && isdigit(*it)) ++it;
    return !s.empty() && it == s.end();
}

bool SetWindow(int Width, int Height)
{
	_COORD coord;
	coord.X = (short)Width;
	coord.Y = (short)Height;

	_SMALL_RECT Rect;
	Rect.Top = 0;
	Rect.Left = 0;
	Rect.Bottom = (short)Height - 1;
	Rect.Right = (short)Width - 1;
	
	// Get handle of the standard output
	HANDLE Handle = GetStdHandle(STD_OUTPUT_HANDLE);
	if (Handle == NULL)
	{
		cout << "    |ERROR 900 - Failure in getting the handle" << endl << GetLastError();
		return FALSE;
	}
	
	// Set screen buffer size to that specified in coord
	if(!SetConsoleScreenBufferSize(Handle, coord))
	{
		cout << "    |ERROR 901 - Failure in setting buffer size." << endl << GetLastError();
		return FALSE;
	}
	
	// Set the window size to that specified in Rect
	if(!SetConsoleWindowInfo(Handle, TRUE, &Rect))
	{
		cout << "    |ERROR 902 - Failure in setting window size." << endl << GetLastError();
		return FALSE;
	}

	return TRUE;
} 
	
//-------------------------------------Potion Kit Functions

void combine_potion_kit (vector<holder>& pkit, vector<string>& kitNames, potions *potion)
{
	size_t index = 0;
	size_t temp_id = 0;

	sort(pkit.begin(), pkit.end(), compareByID);

	for (index = 0 ; index < (int) pkit.size() ; ++index)
	{
		if (pkit[index].ID != 999)
		{
			if (pkit[index].ID >= 800 && pkit[index].ID <= 900) // Potions DB Search
			{
				temp_id = pkit[index].ID - 800;
				pkit[index].Name = potion[temp_id].Name;
				pkit[index].Value = potion[temp_id].Sell;
				kitNames[index] = potion[temp_id].Name;
			}
		}
		else
		{
			kitNames[index] = "        EMPTY    SPACE        ";
			pkit[index].Name = "        EMPTY    SPACE        ";
			pkit[index].Value = 0;
		}
	}
}

void potion_kit_open (vector<string>& pkit)
{
	cout << endl;
	cout << endl;
	cout << "    ---==============00==============--##--==============01==============---" << endl;
	cout << setw(37) << pkit[0] << setw(4) << "||" << setw(32) << pkit[1] << endl;
	cout << "    ---==============================--##--==============================---" << endl;
	cout << endl;
	cout << "    ---==============02==============--##--==============03==============---" << endl;
	cout << setw(37) << pkit[2] << setw(4) << "||" << setw(32) << pkit[3] << endl;
	cout << "    ---==============================--##--==============================---" << endl;
	cout << endl;
	cout << "    ---==============04==============--##--==============05==============---" << endl;
	cout << setw(37) << pkit[4] << setw(4) << "||" << setw(32) << pkit[5] << endl;
	cout << "    ---==============================--##--==============================---" << endl;
	cout << endl;
	cout << "    ---==============06==============--##--==============07==============---" << endl;
	cout << setw(37) << pkit[6] << setw(4) << "||" << setw(32) << pkit[7] << endl;
	cout << "    ---==============================--##--==============================---" << endl;
	cout << endl;
	cout << "    ---==============08==============--##--==============09==============---" << endl;
	cout << setw(37) << pkit[8] << setw(4) << "||" << setw(32) << pkit[9] << endl;
	cout << "    ---==============================--##--==============================---" << endl;
	cout << endl;
	cout << "    ---==============10==============--##--==============11==============---" << endl;
	cout << setw(37) << pkit[10] << setw(4) << "||" << setw(32) << pkit[11] << endl;
	cout << "    ---==============================--##--==============================---" << endl;
	cout << endl;
	cout << "    ---==============12==============--##--==============13==============---" << endl;
	cout << setw(37) << pkit[12] << setw(4) << "||" << setw(32) << pkit[13] << endl;
	cout << "    ---==============================--##--==============================---" << endl;
	cout << endl;
	cout << "    ---==============14==============--##--==============15==============---" << endl;
	cout << setw(37) << pkit[14] << setw(4) << "||" << setw(32) << pkit[15] << endl;
	cout << "    ---==============================--##--==============================---" << endl;
	cout << endl;
	cout << endl;
}

//-------------------------------------Backpack Functions

void combine_rucksack (vector<holder>& backpack, vector<string>& ruckNames, armors *armor, weapons *weapon, items *item)
{
	size_t index = 0;
	size_t temp_id = 0;

	sort(backpack.begin(), backpack.end(), compareByID);

	for (index = 0 ; index < (int) backpack.size() ; ++index)
	{
		if (backpack[index].ID != 999)
		{
			if (backpack[index].ID >= 100 && backpack[index].ID <= 200) // Weapon DB Search
			{
				temp_id = backpack[index].ID - 100;
				backpack[index].Name = weapon[temp_id].Name;
				backpack[index].Value = weapon[temp_id].Sell;
				ruckNames[index] = weapon[temp_id].Name;

			}

			else if (backpack[index].ID >= 300 && backpack[index].ID <= 400) // Armor DB Search
			{
				temp_id = backpack[index].ID - 300;
				backpack[index].Name = armor[temp_id].Name;
				backpack[index].Value = armor[temp_id].Sell;
				ruckNames[index] = armor[temp_id].Name;
			}

			else if (backpack[index].ID >= 500 && backpack[index].ID <= 700) // Items DB Search
			{
				temp_id = backpack[index].ID - 500;
				backpack[index].Name = item[temp_id].Name;
				backpack[index].Value = item[temp_id].Sell;
				ruckNames[index] = item[temp_id].Name;
			}

		}

		else
		{
			ruckNames[index] = "        EMPTY    SPACE        ";
			backpack[index].Name = "        EMPTY    SPACE        ";
			backpack[index].Value = 0;
		}

	}

}

void rucksack_open (vector<string>& slot)
{
	cout << endl;
	cout << endl;
	cout << "    ---==============================--##--==============================---" << endl;
	cout << setw(37) << slot[0] << setw(4) << "||" << setw(32) << slot[1] << endl;
	cout << "    ---==============================--##--==============================---" << endl;
	cout << setw(37) << slot[2] << setw(4) << "||" << setw(32) << slot[3] << endl;
	cout << "    ---==============================--##--==============================---" << endl;
	cout << setw(37) << slot[4] << setw(4) << "||" << setw(32) << slot[5] << endl;
	cout << "    ---==============================--##--==============================---" << endl;
	cout << setw(37) << slot[6] << setw(4) << "||" << setw(32) << slot[7] << endl;
	cout << "    ---==============================--##--==============================---" << endl;
	cout << setw(37) << slot[8] << setw(4) << "||" << setw(32) << slot[9] << endl;
	cout << "    ---==============================--##--==============================---" << endl;
	cout << setw(37) << slot[10] << setw(4) << "||" << setw(32) << slot[11] << endl;
	cout << "    ---==============================--##--==============================---" << endl;
	cout << setw(37) << slot[12] << setw(4) << "||" << setw(32) << slot[13] << endl;
	cout << "    ---==============================--##--==============================---" << endl;
	cout << setw(37) << slot[14] << setw(4) << "||" << setw(32) << slot[15] << endl;
	cout << "    ---==============================--##--==============================---" << endl;
	cout << setw(37) << slot[16] << setw(4) << "||" << setw(32) << slot[17] << endl;
	cout << "    ---==============================--##--==============================---" << endl;
	cout << setw(37) << slot[18] << setw(4) << "||" << setw(32) << slot[19] << endl;
	cout << "    ---==============================--##--==============================---" << endl;
	cout << setw(37) << slot[20] << setw(4) << "||" << setw(32) << slot[21] << endl;
	cout << "    ---==============================--##--==============================---" << endl;
	cout << setw(37) << slot[22] << setw(4) << "||" << setw(32) << slot[23] << endl;
	cout << "    ---==============================--##--==============================---" << endl;
	cout << setw(37) << slot[24] << setw(4) << "||" << setw(32) << slot[25] << endl;
	cout << "    ---==============================--##--==============================---" << endl;
	cout << setw(37) << slot[26] << setw(4) << "||" << setw(32) << slot[27] << endl;
	cout << "    ---==============================--##--==============================---" << endl;
	cout << setw(37) << slot[28] << setw(4) << "||" << setw(32) << slot[29] << endl;
	cout << "    ---==============================--##--==============================---" << endl;
	cout << endl;
	cout << endl;
}

void find_item_name_by_id (int item_id, string& item_name, armors *armor, weapons *weapon, items *item)
{
	size_t temp_id = 0;

	if (item_id != 999)
	{
		if (item_id >= 100 && item_id <= 200) // Weapon DB Search
		{
			temp_id = item_id - 100;
			item_name = weapon[temp_id].Name;
		}

		else if (item_id >= 300 && item_id <= 400) // Armor DB Search
		{
			temp_id = item_id - 300;
			item_name = armor[temp_id].Name;
		}

		else if (item_id >= 500 && item_id <= 700) // Items DB Search
		{
			temp_id = item_id - 500;
			item_name = item[temp_id].Name;
		}

		else
		{
			item_name = "INTERNAL ERROR";
		}
	}

	else
	{
		item_name = "Emptiness";
	}

}

//-------------------------------------Holder Functions

bool compareByID (const holder &a, const holder &b)
{
    return a.ID < b.ID;
}

void holder_reset (vector<holder>& backpack)
{
	size_t index = 0;

	for (index = 0 ; index < (int) backpack.size() ; ++index)
	{
		backpack[index].ID = 999;
		backpack[index].Name = "   Empty Slot   ";
		backpack[index].Value = 0;
	}

}

void holder_upgrade (vector<holder>& backpack, int increase)
{
	// int index;
	int new_size;

	new_size = backpack.size() + increase;

	backpack.resize(new_size);

	/*for (index = backpack.size() - increase ; index <= (int) backpack.size() - 1 ; ++index)	//If Constructs is deleted restore this
	{
		backpack[index].ID = 999;
		backpack[index].Name = "   Empty Slot   ";
		backpack[index].Value = 0;
	}*/

	sort(backpack.begin(), backpack.end(), compareByID);
}

void holder_add_item (vector<holder>& backpack, int item_id)
{
	size_t index = 0;
	int freespace = 0;
	string foo = "No Free Space";

	holder_space_counter (backpack,freespace);

	if (freespace > 0)
	{
		for (index = 0 ; index < (int) backpack.size() ; ++index)
		{
			if (backpack[index].ID == 999)
			{
				backpack[index].ID = item_id;
				break;
			}

		}

	}

	else
	{
		cout << foo << endl;
	}

	sort(backpack.begin(), backpack.end(), compareByID);
}

void holder_remove_item (vector<holder>& backpack, int item_id)
{
	size_t index = 0;

	for (index = 0 ; index < (int) backpack.size() ; ++index)
	{
		if (backpack[index].ID == item_id)
		{
			backpack[index].ID = 999;
			break;
		}

	}

	sort(backpack.begin(), backpack.end(), compareByID);
}

void holder_space_counter (vector<holder>& backpack, int& freespace)
{
	freespace = 0;
	size_t index = 0;

	for (index = 0 ; index < (int) backpack.size() ; ++index)
	{
		if (backpack[index].ID == 999)
		{
			freespace += 1;
		}

	}

}

void holder_furandhide_counter (vector<holder>& backpack, int& fur, int& hide)
{
	fur = 0;
	hide = 0;
	size_t index = 0;

	for (index = 0 ; index < (int) backpack.size() ; ++index)
	{
		if (backpack[index].ID == 521)
		{
			fur += 1;
		}

		else if (backpack[index].ID == 523)
		{
			hide += 1;
		}

	}

}

//-------------------------------------Leveling Functions

void Combat_Level_Checkup(string player_class, int got_exp, int& level, int& experience, int& max_health, int& health, int&max_magic, int& magic, int& strength, int& defense, int& agility)
{
	experience += got_exp;

	if (level < 10)
	{
		if ( (3 + 2 * (level - 1)) * (100 * level)  <= experience )
		{
			level++;

			ClearScreen();
			cout << endl;
			cout << "    NOTIFICATION -> COMBAT SKILL -> LEVEL UP" << endl;
			cout << "    ------------------------------------------------------------------------" << endl;
			cout << endl;
			cout << "     Congratulations! You have leveled up Combat skill. " << endl;
			cout << endl;
			cout << "                                   " << level-1 << " -----> " << level << endl;
			cout << endl;
			cout << endl;

			if (player_class == "Barbarian")
			{
				max_health += 10;
				health += 10;
				max_magic += 6;
				magic += 6;
				strength += 9;
				defense += 10;
				agility += 6;
			}

			else if (player_class == "Rogue")
			{
				max_health += 6;
				health += 6;
				max_magic += 10;
				magic += 10;
				strength += 5;
				defense += 3;
				agility += 5;
			}

			else if (player_class == "Sorceress")
			{
				max_health += 9;
				health += 9;
				max_magic += 8;
				magic += 8;
				strength += 10;
				defense += 5;
				agility += 10;
			}

			else
			{
				cout << "    |ERROR 200 - Critical Error. Quit Game." << endl;
			}

			cout << endl;
			cout << "    ------------------------------------------------------------------------" << endl;
			cout << "    |Press ENTER to continue..." << endl;
			cout << "    |";
			WaitForEnter();
			ClearScreen();
		}
	}
}

void Mining_Level_Checkup(int got_exp, int& level, int& experience)
{
	experience += got_exp;

	if (level < 10)
	{
		if ( (3 + 2 * (level - 1)) * (100 * level)  <= experience )
		{
			level++;

			ClearScreen();
			cout << endl;
			cout << "    NOTIFICATION -> MINING SKILL -> LEVEL UP" << endl;
			cout << "    ------------------------------------------------------------------------" << endl;
			cout << endl;
			cout << "     Congratulations! You have leveled up Mining skill. " << endl;
			cout << endl;
			cout << "                                   " << level-1 << " -----> " << level << endl;
			cout << endl;
			cout << endl;
			cout << endl;
			cout << "    ------------------------------------------------------------------------" << endl;
			cout << "    |Press ENTER to continue..." << endl;
			cout << "    |";
			WaitForEnter();
			ClearScreen();
		}

	}

}

void Farming_Level_Checkup(int got_exp, int& level, int& experience)
{
	experience += got_exp;

	if (level < 10)
	{
		if ( (3 + 2 * (level - 1)) * (100 * level)  <= experience )
		{
			level++;

			ClearScreen();
			cout << endl;
			cout << "    NOTIFICATION -> FARMING SKILL -> LEVEL UP" << endl;
			cout << "    ------------------------------------------------------------------------" << endl;
			cout << endl;
			cout << "     Congratulations! You have leveled up Farming skill. " << endl;
			cout << endl;
			cout << "                                   " << level-1 << " -----> " << level << endl;
			cout << endl;
			cout << endl;
			cout << endl;
			cout << "    ------------------------------------------------------------------------" << endl;
			cout << "    |Press ENTER to continue..." << endl;
			cout << "    |";
			WaitForEnter();
		}

	}

}

void Gambling_Level_Checkup(int got_exp, int& level, int& experience)
{
	experience += got_exp;

	if (level < 10)
	{
		if ( (3 + 2 * (level - 1)) * (100 * level)  <= experience )
		{
			level++;

			ClearScreen();
			cout << endl;
			cout << "    NOTIFICATION -> GAMBLING SKILL -> LEVEL UP" << endl;
			cout << "    ------------------------------------------------------------------------" << endl;
			cout << endl;
			cout << "     Congratulations! You have leveled up Gambling skill. " << endl;
			cout << endl;
			cout << "                                   " << level-1 << " -----> " << level << endl;
			cout << endl;
			cout << endl;
			cout << endl;
			cout << "    ------------------------------------------------------------------------" << endl;
			cout << "    |Press ENTER to continue..." << endl;
			cout << "    |";
			WaitForEnter();
		}

	}

}

void Overall_Level_Checkup(double& overall, int combat, int mining, int farming, int gambling)
{
	overall = sqrt( pow(combat,2) + pow(mining,2) + pow(farming,2) + pow(gambling,2) ) / 2;
}


//-------------------------------------Useful Functions

void Escape_Attempt (int plevel, int elevel, int pdefense, int& status, int& duration)
{
	int number_x = 0;
	int number_y = 0;
	int chance_to_escape = 0;

	Random_Number(number_x);
	Random_Number(number_y);

	chance_to_escape = static_cast <int> (50 - ( static_cast <double> (pdefense) / 7.5 ) + ( ( elevel - plevel ) * 5));

	if (number_x <= chance_to_escape)
	{
		// Sucess
		status = 1;
		duration = 0;
	}

	else
	{
		status = 0;

		if (number_y <= 50)
		{
			duration = 1;
		}

		else
		{
			duration = 2;
		}

	}

}

void Use_Restoration_Potion (int& health, int& max_health, int& magic, int& max_magic, int choice_ID, vector<holder>& pkit, potions *potion)
{
	int temp_id = 0;
	size_t index = 0;
	string UserChoice;
	bool bExitLoop = false;

	for (index = 0 ; index < NUMBEROFPOTIONS ; ++index)
	{
		if (choice_ID == potion[index].ID)
		{
			do
			{
				temp_id = choice_ID - 800;

				cout << endl;
				cout << "                                 USING A POTION" << endl;
				cout << "    ------------------------------------------------------------------------" << endl;
				cout << endl;
				cout << "     Are you sure you want to use " << potion[temp_id].Name << " ?" << endl;
				cout << endl;
				cout << "     The following things will change:" << endl; 
				cout << endl;

				cout << setw(20) << "Health = ";
												Is_Positive (potion[temp_id].Health);
												cout << potion[temp_id].Health << endl;
				cout << setw(20) << "Magic = ";
												Is_Positive (potion[temp_id].Magic);
												cout << potion[temp_id].Magic << endl;
				cout << endl;
				cout << endl;
				cout << "     Please note that using restoration potion will not over extend from" << endl;
				cout << "     from current maximum of health or magic. So use it wisely!" << endl;
				cout << endl;
				cout << endl;
				cout << setw (27) << "HEALTH: " << setw(3) << health << "/" << setw(3) << max_health << setw(20) << "MAGIC: " << setw(3) << magic << "/" << setw(3) << max_magic << endl;
				cout << "    ------------------------------------------------------------------------" << endl;
				cout << "                                   YES  |  NO                               "<< endl;
				cout << "    ------------------------------------------------------------------------" << endl;
				cout << "    |Your decision: ";

				getline(cin, UserChoice);
				transform(UserChoice.begin(), UserChoice.end(),UserChoice.begin(), toupper);
							
				//-------------------------------------

				ClearScreen();

				if (UserChoice == "YES" || UserChoice == "Y")
				{
					bExitLoop = true;

					if ((health == max_health) && (potion[temp_id].Health > 0))
					{
						cout << endl;
						cout << "                                     FIZZLE" << endl;
						cout << "    ------------------------------------------------------------------------" << endl;
						cout << endl;
						cout << "     Your health is already at maximum." << endl;
						cout << endl;
						cout << "    ------------------------------------------------------------------------" << endl;
						cout << "    |Press ENTER to continue..." << endl;
						cout << "    |";
						WaitForEnter();
					}

					else if ((magic == max_magic) && (potion[temp_id].Magic > 0))
					{
						cout << endl;
						cout << "                                     FIZZLE" << endl;
						cout << "    ------------------------------------------------------------------------" << endl;
						cout << endl;
						cout << "     Your magic is already at maximum." << endl;
						cout << endl;
						cout << "    ------------------------------------------------------------------------" << endl;
						cout << "    |Press ENTER to continue..." << endl;
						cout << "    |";
						WaitForEnter();
					}

					else
					{
						health += potion[temp_id].Health;
						magic += potion[temp_id].Magic;

						holder_remove_item (pkit, choice_ID);

						if (health > max_health)
						{
							health = max_health;
						}

						if (magic > max_magic)
						{
							magic = max_magic;
						}

						cout << endl;
						cout << "                                 USING A POTION" << endl;
						cout << "    ------------------------------------------------------------------------" << endl;
						cout << endl;
						cout << "     " << potion[temp_id].Name << " has been used successfully." << endl;
						cout << endl;
						cout << "    ------------------------------------------------------------------------" << endl;
						cout << "    |Press ENTER to continue..." << endl;
						cout << "    |";
						WaitForEnter();
					}
				}

				else if (UserChoice == "NO" || UserChoice == "N")
				{
					bExitLoop = true;
				}

			}while(!bExitLoop);	//---------------------------------------- do-while {} END

		}	//---------------------------------------- if (choice_ID == potion[index].ID) {} END

	}	//---------------------------------------- for {} END

}

void Damage_Calculator (double& adamage, int astrength, int aagility, int alevel, int& dhealth, int ddefense, int dagility)
{
	int luck = 0;
	int number = 0;
	Random_Number(luck);
	Random_Number(number);
	number *= 15;

	if (number <= (375 - (aagility + dagility)))
	{
		adamage = 0;
		cout << "MISS = ";
	}

	else if (number >= (1331 - (aagility/2)))
	{
		adamage  = 100 * (static_cast <double> (astrength) / 300) * (0.5 + (0.05 * alevel)) * (1.4 - (static_cast <double> (ddefense) / 750)) * (0.6 + (static_cast<double> (aagility) / 750)) + 0.1 * (50 - (luck));
		cout << "CRITIAL HIT = ";
	}

	else
	{
		adamage = 60 * (static_cast <double> (astrength) / 300) * (0.5 + (0.05 * alevel)) * (1.4 - ((static_cast <double> (ddefense)) / 750)) + 0.1 * (50 - (luck));
		cout << "HIT = ";
	}

	if (adamage < 0)
	{
		adamage = 0;
	}

	adamage = floor(adamage + 0.5);
	cout << adamage;

	dhealth -= static_cast<int>(adamage);
}

void Super_Damage_Calculator (double& damage, int health, int magic, int agility, int level, int& enemy_health)
{
	int luck = 0;
	Random_Number(luck);

	damage = 200 * ( ( (static_cast <double> (health) / 500) + (static_cast <double> (magic) / 500) + (static_cast <double> (agility) / 300) ) / 3) * (0.5 + (0.05 * static_cast <double> (level))) + 0.1 * (50 - (luck));

	if (damage < 0)
	{
		damage = 0;
	}

	damage = floor(damage + 0.5);
	cout << "SPECIAL = " << damage;

	enemy_health -= static_cast<int>(damage);
}

void Refresh_Equipment_Stats(int& cHea, int& cMHea, int& cMag, int& cMMag, int& cStr, int& cDef, int& cAgi, int bHea, int bMag, int bStr, int bDef, int bAgi, int aHea, int aDef, int aAgi, int sHea, int sDef, int sAgi, int mwMag, int mwStr, int mwAgi, int ofwMag, int ofwStr, int ofwAgi)
{
	cMHea = ( bHea + aHea + sHea);
	cMMag = ( bMag + mwMag + ofwMag );

	cHea = cMHea;
	cMag = cMMag;

	cStr = ( bStr + mwStr + ofwStr ); 
	cDef = ( bDef + aDef + sDef ); 
	cAgi = ( bAgi + aAgi + sAgi + mwAgi  + ofwAgi);
}

//-------------------------------------Player Classes

void Barbarian_Stats(int& max_health, int& health, int&max_magic, int& magic, int& strength, int& defense, int& agility)
{
	max_health = 150;
	health = 150;
	max_magic = 90;
	magic = 90;
	strength = 80;
	defense = 75;
	agility = 40;
}

void Rogue_Stats(int& max_health, int& health, int&max_magic, int& magic, int& strength, int& defense, int& agility)
{
	max_health = 125;
	health = 125;
	max_magic = 100;
	magic = 100;
	strength = 75;
	defense = 40;
	agility = 75;
}

void Sorceress_Stats(int& max_health, int& health, int&max_magic, int& magic, int& strength, int& defense, int& agility)
{
	max_health = 90;
	health = 90;
	max_magic = 150;
	magic = 150;
	strength = 50;
	defense = 20;
	agility = 70;
}

//-------------------------------------Enemy Classes

void Enemy_Stats_Level_1 (int& max_health, int& health, int&max_magic, int& magic, int& strength, int& defense, int& agility, int& level, int& potions, int& experience, int& itemID1, int& itemID1chance, int& itemID2, int& itemID2chance)
{
	max_health = 50;
	health = 50;
	max_magic = 0;
	magic = 0;
	strength = 20;
	defense = 20;
	agility = 0;
	level = 1;
	potions = 0;
	experience = 100;
	itemID1 = 523;
	itemID1chance = 80;
	itemID2 = 524;
	itemID2chance = 80;
}

void Enemy_Stats_Level_2 (int& max_health, int& health, int&max_magic, int& magic, int& strength, int& defense, int& agility, int& level, int& potions, int& experience, int& itemID1, int& itemID1chance, int& itemID2, int& itemID2chance)
{
	max_health = 60;
	health = 60;
	max_magic = 40;
	magic = 40;
	strength = 50;
	defense = 0;
	agility = 40;
	level = 2;
	potions = 0;
	experience = 200;
	itemID1 = 529;
	itemID1chance = 90;
	itemID2 = 530;
	itemID2chance = 80;
}

void Enemy_Stats_Level_3 (int& max_health, int& health, int&max_magic, int& magic, int& strength, int& defense, int& agility, int& level, int& potions, int& experience, int& itemID1, int& itemID1chance, int& itemID2, int& itemID2chance)
{
	max_health = 90;
	health = 90;
	max_magic = 100;
	magic = 100;
	strength = 60;
	defense = 60;
	agility = 20;
	level = 3;
	potions = 0;
	experience = 300;
	itemID1 = 531;
	itemID1chance = 100;
	itemID2 = 532;
	itemID2chance = 90;
}

void Enemy_Stats_Level_4 (int& max_health, int& health, int&max_magic, int& magic, int& strength, int& defense, int& agility, int& level, int& potions, int& experience, int& itemID1, int& itemID1chance, int& itemID2, int& itemID2chance)
{
	max_health = 120;
	health = 120;
	max_magic = 90;
	magic = 90;
	strength = 100;
	defense = 80;
	agility = 80;
	level = 4;
	potions = 0;
	experience = 400;
	itemID1 = 525;
	itemID1chance = 100;
	itemID2 = 526;
	itemID2chance = 90;
}

void Enemy_Stats_Level_5 (int& max_health, int& health, int&max_magic, int& magic, int& strength, int& defense, int& agility, int& level, int& potions, int& experience, int& itemID1, int& itemID1chance, int& itemID2, int& itemID2chance)
{
	max_health = 160;
	health = 160;
	max_magic = 140;
	magic = 140;
	strength = 180;
	defense = 60;
	agility = 100;
	level = 5;
	potions = 0;
	experience = 500;
	itemID1 = 533;
	itemID1chance = 100;
	itemID2 = 534;
	itemID2chance = 90;
}

void Enemy_Stats_Level_6 (int& max_health, int& health, int&max_magic, int& magic, int& strength, int& defense, int& agility, int& level, int& potions, int& experience, int& itemID1, int& itemID1chance, int& itemID2, int& itemID2chance)
{
	max_health = 170;
	health = 170;
	max_magic = 140;
	magic = 140;
	strength = 160;
	defense = 300;
	agility = 50;
	level = 6;
	potions = 0;
	experience = 600;
	itemID1 = 535;
	itemID1chance = 100;
	itemID2 = 536;
	itemID2chance = 90;
}

void Enemy_Stats_Level_7 (int& max_health, int& health, int&max_magic, int& magic, int& strength, int& defense, int& agility, int& level, int& potions, int& experience, int& itemID1, int& itemID1chance, int& itemID2, int& itemID2chance)
{
	max_health = 200;
	health = 200;
	max_magic = 350;
	magic = 350;
	strength = 140;
	defense = 160;
	agility = 300;
	level = 7;
	potions = 0;
	experience = 700;
	itemID1 = 537;
	itemID1chance = 100;
	itemID2 = 538;
	itemID2chance = 90;
}

void Enemy_Stats_Level_8 (int& max_health, int& health, int&max_magic, int& magic, int& strength, int& defense, int& agility, int& level, int& potions, int& experience, int& itemID1, int& itemID1chance, int& itemID2, int& itemID2chance)
{
	max_health = 350;
	health = 350;
	max_magic = 0;
	magic = 0;
	strength = 170;
	defense = 210;
	agility = 190;
	level = 8;
	potions = 0;
	experience = 800;
	itemID1 = 539;
	itemID1chance = 100;
	itemID2 = 540;
	itemID2chance = 90;
}

void Enemy_Stats_Level_9 (int& max_health, int& health, int&max_magic, int& magic, int& strength, int& defense, int& agility, int& level, int& potions, int& experience, int& itemID1, int& itemID1chance, int& itemID2, int& itemID2chance)
{
	max_health = 200;
	health = 200;
	max_magic = 450;
	magic = 450;
	strength = 180;
	defense = 200;
	agility = 200;
	level = 9;
	potions = 0;
	experience = 900;
	itemID1 = 541;
	itemID1chance = 100;
	itemID2 = 542;
	itemID2chance = 90;
}

void Enemy_Stats_Level_10 (int& max_health, int& health, int&max_magic, int& magic, int& strength, int& defense, int& agility, int& level, int& potions, int& experience, int& itemID1, int& itemID1chance, int& itemID2, int& itemID2chance)
{
	max_health = 500;
	health = 500;
	max_magic = 190;
	magic = 190;
	strength = 150;
	defense = 100;
	agility = 150;
	level = 10;
	potions = 0;
	experience = 1000;
	itemID1 = 543;
	itemID1chance = 100;
	itemID2 = 544;
	itemID2chance = 90;
}

void Final_Boss (int& max_health, int& health, int&max_magic, int& magic, int& strength, int& defense, int& agility, int& level, int& potions, int& experience, int& itemID1, int& itemID1chance, int& itemID2, int& itemID2chance)
{
	max_health = 750;
	health = 750;
	max_magic = 350;
	magic = 350;
	strength = 240;
	defense = 300;
	agility = 200;
	level = 10;
	potions = 0;
	experience = 5000;
	itemID1 = 999;
	itemID1chance = 0;
	itemID2 = 999;
	itemID2chance = 0;
}

void Mining_attempt (vector<holder>& backpack, int& backpack_free_space, armors *armor, weapons *weapon, items *item, string& UserChoice, string& item_name, int& player_mining_level, int& player_mining_experience)
{
	int path_choice = 0;
	int random_number = 0;
	bool bExitLoop1 = false;

	do
	{
		path_choice = 0;
		random_number = 0;

		ClearScreen();
		cout << endl;
		cout << "                                     MINING                                 " << endl;
		cout << "    ------------------------------------------------------------------------" << endl;
		cout << endl;
		cout << "     You have entered Luikach Mine, and decided to do some mining." << endl;
		cout << endl;
		cout << "     While holding your pickaxe, you can decide from which side would you like " << endl;
		cout << "     to start mining." << endl;
		cout << endl;
		cout << endl;
		cout << "    ------------------------------------------------------------------------" << endl;
		cout << "                  EXIT  |  LEFT  | STRAIGHT  |  DOWN  |  RIGHT              " << endl;
		cout << "    ------------------------------------------------------------------------" << endl;
		cout << "    |Your decision: ";

		getline(cin, UserChoice);
		transform(UserChoice.begin(), UserChoice.end(),UserChoice.begin(), toupper);

		if (UserChoice == "EXIT" || UserChoice == "E")
		{
			bExitLoop1 = true;
		}
	
		else if (UserChoice == "LEFT" || UserChoice == "L")
		{
			path_choice = 1;
		}

		else if (UserChoice == "STRAIGHT" || UserChoice == "S")
		{
			path_choice = 2;
		}

		else if (UserChoice == "DOWN" || UserChoice == "D")
		{
			path_choice = 3;
		}

		else if (UserChoice == "RIGHT" || UserChoice == "R")
		{
			path_choice = 4;
		}

		if (path_choice != 0)
		{
			if (mining_points > 0)
			{
				mining_points--;
				Random_Number(random_number);

				if (random_number <= 25)
				{
					random_number = 1;
				}

				else if (random_number <= 50)
				{
					random_number = 2;
				}

				else if (random_number <= 75)
				{
					random_number = 3;
				}

				else
				{
					random_number = 4;
				}

				if (random_number == path_choice)
				{
					Random_Number(random_number);

					if (random_number <= 23)
					{
						random_number = 556;
					}

					else if (random_number <= 42)
					{
						random_number = 555;
					}

					else if (random_number <= 58)
					{
						random_number = 551;
					}

					else if (random_number <= 73)
					{
						random_number = 552;
					}

					else if (random_number <= 87)
					{
						random_number = 553;
					}

					else
					{
						random_number = 554;
					}

					holder_space_counter (backpack,backpack_free_space);

					if (backpack_free_space >= 1)
					{
						holder_add_item (backpack, random_number);
						find_item_name_by_id (random_number, item_name, armor, weapon, item);
						Mining_Level_Checkup(50, player_mining_level, player_mining_experience);
						//FIXME NUM of exsp received for mining

						ClearScreen();
						cout << endl;
						cout << "                                     MINING                                 " << endl;
						cout << "    ------------------------------------------------------------------------" << endl;
						cout << endl;
						cout << "     After exhausting mining, you found " << item_name << "." << endl;
						cout << endl;
						cout << "    ------------------------------------------------------------------------" << endl;
						cout << "    |Press ENTER to continue..." << endl;
						cout << "    |";
						WaitForEnter();
					}

					else
					{
						cout << endl;
						cout << "                                     FIZZLE" << endl;
						cout << "    ------------------------------------------------------------------------" << endl;
						cout << endl;
						cout << "     You have no free space in backpack." << endl;
						cout << endl;
						cout << "    ------------------------------------------------------------------------" << endl;
						cout << "    |Press ENTER to continue..." << endl;
						cout << "    |";
						WaitForEnter();
					}

				}	//----------------------------------------(random_number == path_choice) {} END

				else
				{
					ClearScreen();
					cout << endl;
					cout << "                                     FIZZLE" << endl;
					cout << "    ------------------------------------------------------------------------" << endl;
					cout << endl;
					cout << "     After exhausting mining, you had no luck finding something valuable." << endl;
					cout << endl;
					cout << "    ------------------------------------------------------------------------" << endl;
					cout << "    |Press ENTER to continue..." << endl;
					cout << "    |";
					WaitForEnter();
				}	//----------------------------------------else {} END

			}	//----------------------------------------(mining_points > 0) {} END

			else
			{
				ClearScreen();
				cout << endl;
				cout << "                                     FIZZLE" << endl;
				cout << "    ------------------------------------------------------------------------" << endl;
				cout << endl;
				cout << "     Too tired to mine today. Try again tomorrow." << endl;
				cout << endl;
				cout << "    ------------------------------------------------------------------------" << endl;
				cout << "    |Press ENTER to continue..." << endl;
				cout << "    |";
				WaitForEnter();
			}	//----------------------------------------else {} END

		}	//----------------------------------------(path_choice != 0) {} END

	}while(!bExitLoop1);

}

void access_account (string location_access, vector<holder>& backpack, vector<string>& backpack_names, int& backpack_free_space, armors *armor, weapons *weapon, items *item, potions *potion, vector<holder>& potion_kit, vector<string>& potionkit_names, int& potionkit_free_space, string& UserChoice, int& user_valid_number, int& user_ID_choice, int& item_id, int& i, string& town_name, string& player_class_name, int& player_glass_marbles, int& player_combined_max_health, int& player_combined_health, int& player_combined_max_magic, int& player_combined_magic, int& player_combined_strength, int& player_combined_defense, int& player_combined_agility, string& player_armor_stats_name, int& player_armor_stats_health, int& player_armor_stats_defense, int& player_armor_stats_agility, string& player_shield_stats_name, int& player_shield_stats_health, int& player_shield_stats_defense, int& player_shield_stats_agility, string& player_offhand_weapon_stats_name, int& player_offhand_weapon_stats_magic, int& player_offhand_weapon_stats_agility, int& player_offhand_weapon_stats_strength, string& player_main_weapon_stats_name, int& player_main_weapon_stats_magic, int& player_main_weapon_stats_agility, int& player_main_weapon_stats_strength, int& showcase_required_combat, int& showcase_required_mining, int& showcase_required_farming, int& showcase_required_gambling, double& overall_player_level, int& overall_level_progress, int& player_combat_level, int& player_combat_experience, int& player_farming_level, int& player_farming_experience, int& player_mining_level, int& player_mining_experience, int& player_gambling_level, int& player_gambling_experience, int& total_number_of_travels, int& current_number_of_travels, int& player_main_weapon_ID_carried, int& player_offhand_weapon_ID_carried, int& special_weapon_equiped, int& player_armor_ID_carried, int& player_shield_ID_carried)
{
	bool bExitLoop1 = false;
	bool bExitLoop2 = false;
	bool bExitLoop3 = false;

	if (location_access != "TOWN")
	{
		town_name = location_access;
	}	//----------------------------------------(location_access != "TOWN") {} END

	do
	{
		ClearScreen();
		cout << endl;
		cout << "    " << location_access << " -> ACCOUNT" << endl;
		cout << "    ------------------------------------------------------------------------" << endl;
		cout << endl;
		cout << "     This menu contains general informations and statistics about current" << endl;
		cout << "     location and yourself." << endl;
		cout << endl;
		cout << endl;
		cout << "      Time: " << setw (2) << game_time_hours << ":" << setw (2) << game_time_minutes << endl;
		cout << endl;
		cout << "       Day: " << game_time_day << endl;
		cout << "      Week: " << game_time_week << endl;
		cout << "     Month: " << game_time_month << endl;
		cout << "      Year: " << game_time_year << endl;
		cout << endl;
		cout << endl;
		cout << "    ------------------------------------------------------------------------" << endl;
		cout << "           EXIT  |  HELP  |  GEAR  |  BACKPACK  | POTION  | STATISTICS      " << endl;
		cout << "    ------------------------------------------------------------------------" << endl;
		cout << "    |Your decision: ";

		getline(cin, UserChoice);
		transform(UserChoice.begin(), UserChoice.end(),UserChoice.begin(), toupper);
	
		//-------------------------------------

		ClearScreen();

		if (UserChoice == "EXIT" || UserChoice == "E")
		{
			bExitLoop1 = true;
		}	//----------------------------------------(UserChoice == "EXIT" || UserChoice == "E") {} END
	
		else if (UserChoice == "HELP" || UserChoice == "H")
		{
			cout << endl;
			cout << "    " << location_access << " -> ACCOUNT -> HELP" << endl;
			cout << "    ------------------------------------------------------------------------" << endl;
			cout << endl;

			if (location_access == "TOWN")
			{
				cout << "     While in town, one of important thing is marketplace. At marketplace" << endl;
				cout << "     you can improve the size of your backpack, and potion kit. Purchase new " << endl;
				cout << "     and better Armor and Shields. Do not forget to check Weaponsmith for " << endl;
				cout << "     a better weapon upgrade." << endl;
				cout << endl;
				cout << "     You can also enter Inn for some relaxation from dangerous adventures," << endl;
				cout << "     and try your luck in cards. Remember to take some money with you." << endl;
				cout << endl;
				cout << "     Do not forget, instead of using whole words, for shortcut just" << endl;
				cout << "     type first letter of CAPITAL WORDS. Example EXIT or just E" << endl;
			}	//----------------------------------------(location_access == "TOWN") {} END

			else if (location_access == "CAMPING")
			{
				cout << "     While camping, You regain 15% of current health and magic." << endl;
				cout << endl;
				cout << "     Before you can reach a near by town, you must travel few times" << endl;
				cout << "     to reach it." << endl;
				cout << endl;
				cout << "     Do not forget, instead of using whole words, for shortcut just" << endl;
				cout << "     type first letter of CAPITAL WORDS. Example EXIT or just E" << endl;
			}	//----------------------------------------(location_access == "CAMPING") {} END

			else if (location_access == "TRAVELING")
			{
				cout << "     While traveling, you can decide if you would like to explore specific" << endl;
				cout << "     part of the World or if you would like to just continue walking..." << endl;
				cout << endl;
				cout << "     Different parts of World contains different items to obtain." << endl;
				cout << endl;
				cout << "     Keep an eye for Gileeran Bear, their hide is very useful and precious." << endl;
				cout << endl;
				cout << "     Do not forget, instead of using whole words, for shortcut just" << endl;
				cout << "     type first letter of CAPITAL WORD. EXIT -> E" << endl;
			}	//----------------------------------------(location_access == "TRAVELING") {} END

			else if (location_access == "WORLD")
			{
				cout << "     While in world, you can only decide if You would like to continue your" << endl;
				cout << "     journey by traveling or return to near by town..." << endl;
				cout << endl;
				cout << "     Do not forget, instead of using whole words, for shortcut just" << endl;
				cout << "     type first letter of CAPITAL WORD. EXIT -> E" << endl;
			}	//----------------------------------------(location_access == "WORLD") {} END

			else
			{
				//system error , exit?
			}

			cout << endl;
			cout << "    ------------------------------------------------------------------------" << endl;
			cout << "    |Press ENTER to continue..." << endl;
			cout << "    |";
			
			WaitForEnter();
		}	//----------------------------------------(UserChoice == "HELP" || UserChoice == "H") {} END

		else if (UserChoice == "BACKPACK" || UserChoice == "B")
		{
			bExitLoop2 = false;
	
			do
			{
				ClearScreen();
				cout << endl;
				cout << "    " << location_access << " -> ACCOUNT -> BACKPACK" << endl;
				cout << "    ------------------------------------------------------------------------" << endl;

				combine_rucksack (backpack, backpack_names, armor, weapon, item);
				rucksack_open(backpack_names);

				cout << "    ------------------------------------------------------------------------" << endl;
				cout << "                                 DROP  |  EXIT                              " << endl;
				cout << "    ------------------------------------------------------------------------" << endl;
				cout << "    |Your decision: ";
		
				getline(cin, UserChoice);
				transform(UserChoice.begin(), UserChoice.end(),UserChoice.begin(), toupper);

				ClearScreen();

				if (UserChoice == "EXIT" || UserChoice == "E")
				{
					bExitLoop2 = true;
				}

				else if (UserChoice == "DROP" || UserChoice == "D") 
				{
					bExitLoop3 = false;
			
					do
					{
						combine_rucksack (backpack, backpack_names, armor, weapon, item);

						ClearScreen();
						cout << endl;
						cout << "    " << location_access << " -> ACCOUNT -> BACKPACK -> DROP ITEM" << endl;
						cout << "    ------------------------------------------------------------------------" << endl;
						cout << endl;
						cout << "     Decided to drop item from your backpack." << endl;
						cout << "     Looking trough all of your items in backpack..." << endl;
						cout << "     Once I drop the item, it will be lost forever..." << endl;
						cout << endl;
						cout << "    ------------------------------------------------------------------------" << endl;
						cout << "    |IDN|                               NAME                               |" << endl;
						cout << "    ------------------------------------------------------------------------" << endl;

						for (i = 0 ; i < (int) backpack.size() ; ++i)
						{
							if (backpack[i].ID == 999)
							{
								break;
							}

							else
							{
								cout << "    |"	 << setw(3) << i << "|"
										<< setw(35) << backpack[i].Name
										<< setw(32)	 << "|" << endl;
							}
						}

						cout << "    ------------------------------------------------------------------------" << endl;
						cout << endl;
						cout << endl;
						cout << endl;
						cout << endl;
						cout << "    ------------------------------------------------------------------------" << endl;
						cout << "                                 (IDN)  |  EXIT                             " << endl;
						cout << "    ------------------------------------------------------------------------" << endl;
						cout << "    |Your decision: ";
				
						getline(cin, UserChoice);
						user_valid_number = is_number(UserChoice);
						user_ID_choice = atoi(UserChoice.c_str());
						transform(UserChoice.begin(), UserChoice.end(),UserChoice.begin(), toupper);

						ClearScreen();

						if (UserChoice == "EXIT" || UserChoice == "E")
						{
							bExitLoop3 = true;
						}

						else if (user_valid_number == 1)	
						{
							if (user_ID_choice < i)
							{
								item_id = backpack[user_ID_choice].ID;
								holder_remove_item (backpack, item_id);
							}

							else if (user_ID_choice < (int) backpack.size())
							{
								cout << endl;
								cout << "                                     FIZZLE" << endl;
								cout << "    ------------------------------------------------------------------------" << endl;
								cout << endl;
								cout << "     No item in this backpack slot." << endl;
								cout << endl;
								cout << "    ------------------------------------------------------------------------" << endl;
								cout << "    |Press ENTER to continue..." << endl;
								cout << "    |";
								WaitForEnter();
							}

							else if (user_ID_choice >= (int) backpack.size())
							{
								cout << endl;
								cout << "                                     FIZZLE" << endl;
								cout << "    ------------------------------------------------------------------------" << endl;
								cout << endl;
								cout << "     Your backpack does not have so much slots." << endl;
								cout << endl;
								cout << "    ------------------------------------------------------------------------" << endl;
								cout << "    |Press ENTER to continue..." << endl;
								cout << "    |";
								WaitForEnter();
							}

						}	//----------------------------------------(user_valid_number == 1) {} END

						else
						{
							cout << endl;
							cout << "                                     FIZZLE" << endl;
							cout << "    ------------------------------------------------------------------------" << endl;
							cout << endl;
							cout << "     Please type in the correct IDN of item." << endl;
							cout << endl;
							cout << "    ------------------------------------------------------------------------" << endl;
							cout << "    |Press ENTER to continue..." << endl;
							cout << "    |";
							WaitForEnter();
						}	//----------------------------------------else {} END

					} while(!bExitLoop3);

				}

			}while(!bExitLoop2);

		}	//----------------------------------------(UserChoice == "BACKPACK" || UserChoice == "B") {} END
	
		else if (UserChoice == "POTION" || UserChoice == "P")
		{
			bExitLoop2 = false;

			do
			{
				ClearScreen();
				cout << endl;
				cout << "    " << location_access << " -> ACCOUNT -> POTION MENU" << endl;
				cout << "    ------------------------------------------------------------------------" << endl;

				combine_potion_kit (potion_kit, potionkit_names, potion);
				potion_kit_open(potionkit_names);

				for (i = 0 ; i < (int) potion_kit.size() ; ++i)
				{
					if (potion_kit[i].ID == 999)
					{
						break;
					}
				}

				cout << setw (27) << "HEALTH: " << setw(3) << player_combined_health << "/" << setw(3) << player_combined_max_health << setw(20) << "MAGIC: " << setw(3) << player_combined_magic << "/" << setw(3) << player_combined_max_magic << endl;
				cout << "    ------------------------------------------------------------------------" << endl;
				cout << "                                  (IDN)  |  EXIT                             " << endl;
				cout << "    ------------------------------------------------------------------------" << endl;
				cout << "    |Your decision: ";
			
				getline(cin, UserChoice);
				user_valid_number = is_number(UserChoice);
				user_ID_choice = atoi(UserChoice.c_str());
				transform(UserChoice.begin(), UserChoice.end(),UserChoice.begin(), toupper);

				ClearScreen();

				if (UserChoice == "EXIT" || UserChoice == "E")
				{
					bExitLoop2 = true;
				}

				else if (user_valid_number == 1)	
				{
					if (user_ID_choice < i)
					{
						item_id = potion_kit[user_ID_choice].ID;
						Use_Restoration_Potion (player_combined_health, player_combined_max_health, player_combined_magic, player_combined_max_magic, item_id, potion_kit, potion);
					}

					else if (user_ID_choice < (int) potion_kit.size())
					{
						cout << endl;
						cout << "                                     FIZZLE" << endl;
						cout << "    ------------------------------------------------------------------------" << endl;
						cout << endl;
						cout << "     No item in this potion kit slot." << endl;
						cout << endl;
						cout << "    ------------------------------------------------------------------------" << endl;
						cout << "    |Press ENTER to continue..." << endl;
						cout << "    |";
						WaitForEnter();
					}

					else if (user_ID_choice >= (int) potion_kit.size())
					{
						cout << endl;
						cout << "                                     FIZZLE" << endl;
						cout << "    ------------------------------------------------------------------------" << endl;
						cout << endl;
						cout << "     Your potion kit does not have so much slots." << endl;
						cout << endl;
						cout << "    ------------------------------------------------------------------------" << endl;
						cout << "    |Press ENTER to continue..." << endl;
						cout << "    |";
						WaitForEnter();
					}

				}	//----------------------------------------(user_valid_number == 1) {} END

				else
				{
					cout << endl;
					cout << "                                     FIZZLE" << endl;
					cout << "    ------------------------------------------------------------------------" << endl;
					cout << endl;
					cout << "     Please type in the correct IDN of Potion." << endl;
					cout << endl;
					cout << "    ------------------------------------------------------------------------" << endl;
					cout << "    |Press ENTER to continue..." << endl;
					cout << "    |";
					WaitForEnter();
				}	//----------------------------------------else {} END

			} while(!bExitLoop2);	// do-while loop for Bad Commands

		}	//----------------------------------------(UserChoice == "POTION" || UserChoice == "P") {} END

		else if (UserChoice == "GEAR" || UserChoice == "G")
		{
			bExitLoop2 = false;

			do
			{
				combine_rucksack (backpack, backpack_names, armor, weapon, item);

				ClearScreen();

				cout << endl;
				cout << "    " << location_access << " -> ACCOUNT -> GEAR" << endl;
				cout << "    ------------------------------------------------------------------------" << endl;
				cout << endl;
				cout << setw(27) << "Class Type:"				<< setw(32) << player_class_name												<< endl;
				cout << setw(27) << "Location:"					<< setw(32) << town_name														<< endl;
				cout << setw(27) << "Glass Marbles:"			<< setw(32) << player_glass_marbles 											<< endl;
				cout << endl;
				cout << setw(27) << "Health:"		<< setw(28) << player_combined_health << "/" << setw(3) <<  player_combined_max_health	<< endl;
				cout << setw(27) << "Magic:"		<< setw(28) << player_combined_magic	<< "/" << setw(3) << player_combined_max_magic	<< endl;
				cout << setw(27) << "Strength:"					<< setw(32) << player_combined_strength											<< endl;
				cout << setw(27) << "Defense:"					<< setw(32) << player_combined_defense											<< endl;
				cout << setw(27) << "Agility:"					<< setw(32) << player_combined_agility 											<< endl;
				cout << endl;
				cout << setw(27) << "Armor:"					<< setw(32) << player_armor_stats_name											<< endl;
				cout << setw(27) << "HEA:"						<< setw(32) << player_armor_stats_health										<< endl;
				cout << setw(27) << "DEF:"						<< setw(32) << player_armor_stats_defense 										<< endl;
				cout << setw(27) << "AGI:"						<< setw(32) << player_armor_stats_agility 										<< endl;
				cout << endl;
				cout << setw(27) << "Shield:"					<< setw(32) << player_shield_stats_name											<< endl;
				cout << setw(27) << "HEA:"						<< setw(32) << player_shield_stats_health										<< endl;
				cout << setw(27) << "DEF:"						<< setw(32) << player_shield_stats_defense										<< endl;
				cout << setw(27) << "AGI:"						<< setw(32) << player_shield_stats_agility										<< endl;
				cout << endl;
				cout << setw(27) << "Main Weapon:"				<< setw(32) << player_main_weapon_stats_name									<< endl;
				cout << setw(27) << "MAG:"						<< setw(32) << player_main_weapon_stats_magic									<< endl;
				cout << setw(27) << "STR:"						<< setw(32) << player_main_weapon_stats_strength								<< endl;
				cout << setw(27) << "AGI:"						<< setw(32) << player_main_weapon_stats_agility									<< endl;
				cout << endl;
				cout << setw(27) << "Offhand Weapon:"			<< setw(32) << player_offhand_weapon_stats_name									<< endl;
				cout << setw(27) << "MAG:"						<< setw(32) << player_offhand_weapon_stats_magic								<< endl;
				cout << setw(27) << "STR:"						<< setw(32) << player_offhand_weapon_stats_strength								<< endl;
				cout << setw(27) << "AGI:"						<< setw(32) << player_offhand_weapon_stats_agility								<< endl;
				cout << endl;
				cout << endl;
				cout << "    ------------------------------------------------------------------------" << endl;
				cout << "                      DISARM  |  ARMOR  |  WEAPON  |  EXIT                  " << endl;
				cout << "    ------------------------------------------------------------------------" << endl;
				cout << "    |Your decision: ";

				getline(cin, UserChoice);
				transform(UserChoice.begin(), UserChoice.end(),UserChoice.begin(), toupper);
		
				ClearScreen();

				if (UserChoice == "EXIT" || UserChoice == "E")
				{
					bExitLoop2 = true;
				}

				else if (UserChoice == "DISARM" || UserChoice == "D")
				{
					holder_space_counter (backpack,backpack_free_space);
					
					if (backpack_free_space >= 2)
					{
						if (player_main_weapon_ID_carried != 999)
						{
							i = player_main_weapon_ID_carried;
							holder_add_item (backpack, i);
							player_main_weapon_ID_carried = 999;
						}

						if (player_offhand_weapon_ID_carried != 999)
						{
							i = player_offhand_weapon_ID_carried;
							holder_add_item (backpack, i);
							player_offhand_weapon_ID_carried = 999;
						}

						special_weapon_equiped = 0;

						player_main_weapon_stats_name = "None";
						player_main_weapon_stats_magic = 0;
						player_main_weapon_stats_strength = 0;
						player_main_weapon_stats_agility = 0;

						player_offhand_weapon_stats_name = "None";
						player_offhand_weapon_stats_magic = 0;
						player_offhand_weapon_stats_strength = 0;
						player_offhand_weapon_stats_agility = 0;
																	
						cout << endl;
						cout << "    " << location_access << " -> ACCOUNT -> GEAR -> DISARMING" << endl;
						cout << "    ------------------------------------------------------------------------" << endl;
						cout << endl;
						cout << "     Your weapons has been stored in backpack successfully." << endl;
						cout << endl;
						cout << "    ------------------------------------------------------------------------" << endl;
						cout << "    |Press ENTER to continue..." << endl;
						cout << "    |";
						WaitForEnter();
					} // (backpack_free_space >= 2)

					else
					{
						cout << endl;
						cout << "                                     FIZZLE" << endl;
						cout << "    ------------------------------------------------------------------------" << endl;
						cout << endl;
						cout << "     No free space in backpack." << endl;
						cout << endl;
						cout << "    ------------------------------------------------------------------------" << endl;
						cout << "    |Press ENTER to continue..." << endl;
						cout << "    |";
						WaitForEnter();
					}
					
				} // (UserChoice == "DISARM" || UserChoice == "D") {} END

				else if (UserChoice == "ARMOR" || UserChoice == "A")
				{
					bExitLoop3 = false;

					do
					{
						combine_rucksack (backpack, backpack_names, armor, weapon, item);

						ClearScreen();
						cout << endl;
						cout << "    " << location_access << " -> ACCOUNT -> GEAR -> ARMORS" << endl;
						cout << "    ------------------------------------------------------------------------" << endl;
						cout << endl;
						cout << "    Manage your current armor." << endl;
						cout << endl;
						cout << "    Type in an IDN of item You want to use." << endl;
						cout << endl;
						cout << "    Following Armors are available:" << endl;
						cout << endl;
						cout << endl;
						cout << "    ------------------------------------------------------------------------" << endl;
						cout << "    |IDN|                     NAME                     |   SELLING PRICE   |" << endl;
						cout << "    ------------------------------------------------------------------------" << endl;

						for (i = 0 ; i < (int) backpack.size() ; ++i)
						{
							if (backpack[i].ID == 999)
							{
								break;
							}

							else if (backpack[i].ID >= 300 && backpack[i].ID <= 499)
							{
								cout << "    |"	 << setw(3) << i << "|"
										<< setw(31) << backpack[i].Name
										<< setw(16) << "|" 
										<< setw(11) << backpack[i].Value
										<< setw(9)	 << "|" << endl;
							}
						}

						cout << "    ------------------------------------------------------------------------" << endl;
						cout << endl;
						cout << endl;
						cout << endl;
						cout << endl;
						cout << "    ------------------------------------------------------------------------" << endl;
						cout << "                                  (IDN)  |  EXIT                             " << endl;
						cout << "    ------------------------------------------------------------------------" << endl;
						cout << "    |Your decision: ";

						getline(cin, UserChoice);
						user_valid_number = is_number(UserChoice);
						user_ID_choice = atoi(UserChoice.c_str());
						transform(UserChoice.begin(), UserChoice.end(),UserChoice.begin(), toupper);

						if (user_ID_choice < 0 || user_ID_choice >= (int) backpack.size())
						{
							user_valid_number = 0;
						}

						//----------------------------------------

						ClearScreen();

						if (UserChoice == "EXIT" || UserChoice == "E")
						{
							bExitLoop3 = true;
						}

						else if (user_valid_number == 1)
						{
							if (backpack[user_ID_choice].ID >= 300 && backpack[user_ID_choice].ID <= 499)
							{
								item_id = backpack[user_ID_choice].ID - 300;

								if (armor[item_id].Type == "Armor")
								{
									int x = player_armor_ID_carried;
									player_armor_ID_carried = backpack[user_ID_choice].ID;
									player_armor_stats_name = armor[item_id].Name;
									player_armor_stats_health = armor[item_id].Health;
									player_armor_stats_defense = armor[item_id].Defense;
									player_armor_stats_agility = armor[item_id].Agility;
									holder_remove_item (backpack, backpack[user_ID_choice].ID);
									holder_add_item(backpack, x);
								}

								else if (armor[item_id].Type == "Shield")
								{
									int x = player_shield_ID_carried;
									player_shield_ID_carried = backpack[user_ID_choice].ID;
									player_shield_stats_name = armor[item_id].Name;
									player_shield_stats_health = armor[item_id].Health;
									player_shield_stats_defense = armor[item_id].Defense;
									player_shield_stats_agility = armor[item_id].Agility;
									holder_remove_item (backpack, backpack[user_ID_choice].ID);
									holder_add_item(backpack, x);
								}

								else
								{
									//system error?!?
								}

								//----------------------------------------

								cout << endl;
								cout << "                                     SUCCESS" << endl;
								cout << "    ------------------------------------------------------------------------" << endl;
								cout << endl;
								cout << "     " << armor[item_id].Type << " has been successfully swapped." << endl;
								cout << endl;
								cout << "    ------------------------------------------------------------------------" << endl;
								cout << "    |Press ENTER to continue..." << endl;
								cout << "    |";
								WaitForEnter();
							}

							else if (user_ID_choice < (int) backpack.size())
							{
								cout << endl;
								cout << "                                     FIZZLE" << endl;
								cout << "    ------------------------------------------------------------------------" << endl;
								cout << endl;
								cout << "     No armor in this backpack slot." << endl;
								cout << endl;
								cout << "    ------------------------------------------------------------------------" << endl;
								cout << "    |Press ENTER to continue..." << endl;
								cout << "    |";
								WaitForEnter();
							}

							else if (user_ID_choice >= (int) backpack.size())
							{
								cout << endl;
								cout << "                                     FIZZLE" << endl;
								cout << "    ------------------------------------------------------------------------" << endl;
								cout << endl;
								cout << "     Your backpack does not have so much slots." << endl;
								cout << endl;
								cout << "    ------------------------------------------------------------------------" << endl;
								cout << "    |Press ENTER to continue..." << endl;
								cout << "    |";
								WaitForEnter();
							}

						}	// Valid Number Checker  {} END

						else
						{
							cout << endl;
							cout << "                                     FIZZLE" << endl;
							cout << "    ------------------------------------------------------------------------" << endl;
							cout << endl;
							cout << "     Please type in the correct IDN of Item." << endl;
							cout << endl;
							cout << "    ------------------------------------------------------------------------" << endl;
							cout << "    |Press ENTER to continue..." << endl;
							cout << "    |";
							WaitForEnter();
						}

					}while (!bExitLoop3);

				} // (UserChoice == "ARMOR" || UserChoice == "A") {} END

				else if (UserChoice == "WEAPON" || UserChoice == "W")
				{
					bExitLoop3 = false;

					do
					{
						holder_space_counter (backpack,backpack_free_space);
						combine_rucksack (backpack, backpack_names, armor, weapon, item);

						ClearScreen();
						cout << endl;
						cout << "    " << location_access << " -> ACCOUNT -> GEAR -> WEAPONS" << endl;
						cout << "    ------------------------------------------------------------------------" << endl;
						cout << endl;
						cout << "    Manage your current weapon." << endl;
						cout << endl;
						cout << "    Type in an IDN of item You want to use." << endl;
						cout << endl;
						cout << "    Following Weapons are available:" << endl;
						cout << endl;
						cout << endl;
						cout << "    ------------------------------------------------------------------------" << endl;
						cout << "    |IDN|                     NAME                     |   SELLING PRICE   |" << endl;
						cout << "    ------------------------------------------------------------------------" << endl;

						for (i = 0 ; i < (int) backpack.size() ; ++i)
						{
							if (backpack[i].ID == 999)
							{
								break;
							}

							else if (backpack[i].ID >= 100 && backpack[i].ID <= 299)
							{
								cout << "    |"	 << setw(3) << i << "|"
										<< setw(31) << backpack[i].Name
										<< setw(16) << "|" 
										<< setw(11) << backpack[i].Value
										<< setw(9)	 << "|" << endl;
							}
						}

						cout << "    ------------------------------------------------------------------------" << endl;
						cout << endl;
						cout << endl;
						cout << endl;
						cout << endl;
						cout << "    ------------------------------------------------------------------------" << endl;
						cout << "                                  (IDN)  |  EXIT                             " << endl;
						cout << "    ------------------------------------------------------------------------" << endl;
						cout << "    |Your decision: ";

						getline(cin, UserChoice);
						user_valid_number = is_number(UserChoice);
						user_ID_choice = atoi(UserChoice.c_str());
						transform(UserChoice.begin(), UserChoice.end(),UserChoice.begin(), toupper);

						if (user_ID_choice < 0 || user_ID_choice >= (int) backpack.size())
						{
							user_valid_number = 0;
						}

						//----------------------------------------

						ClearScreen();

						if (UserChoice == "EXIT" || UserChoice == "E")
						{
							bExitLoop3 = true;
						}

						else if (user_valid_number == 1)
						{
							if (backpack[user_ID_choice].ID >= 100 && backpack[user_ID_choice].ID <= 299)
							{
								item_id = backpack[user_ID_choice].ID - 100;
								int special_weapon_available = 0;
								bool bExitLoop4 = false;

								if (backpack_free_space >= 2)
								{
									special_weapon_available = 1;
								}
														
								do
								{
									ClearScreen();
									cout << endl;
									cout << "    " << location_access << " -> ACCOUNT -> GEAR -> WEAPONS -> ALLOCATION" << endl;
									cout << "    ------------------------------------------------------------------------" << endl;
									cout << endl;
									cout << "     Please select position of weapon to equip:" << endl;
									cout << endl;
									cout << setw(35) << "MAIN WEAPON" << endl;
									cout << setw(35) << "OFFHAND WEAPON" << endl;

									if (special_weapon_available == 1)
									{
										cout << setw(35) << "SPECIAL WEAPON" << endl;
										cout << endl;
										cout << endl;
										cout << "    ------------------------------------------------------------------------" << endl;
										cout << "                     MAIN  |  OFFHAND  |  SPECIAL  |  DISARM                " << endl;
									}

									else
									{
										cout << endl;
										cout << endl;
										cout << "    ------------------------------------------------------------------------" << endl;
										cout << "                           MAIN  |  OFFHAND  |  DISARM                      " << endl;
									}

									cout << "    ------------------------------------------------------------------------" << endl;
									cout << "    |Your decision: ";

									getline(cin, UserChoice);
									transform(UserChoice.begin(), UserChoice.end(),UserChoice.begin(), toupper);
															
									ClearScreen();
															
									if ((UserChoice == "MAIN" || UserChoice == "M") && (special_weapon_equiped == 0) && (weapon[item_id].Race == "All") && (weapon[item_id].Type == "One Handed"))
									{
										bExitLoop4 = true;

										int x = player_main_weapon_ID_carried;
										player_main_weapon_ID_carried = backpack[user_ID_choice].ID;
										player_main_weapon_stats_name = weapon[item_id].Name;
										player_main_weapon_stats_magic = weapon[item_id].Magic;
										player_main_weapon_stats_strength = weapon[item_id].Strength;
										player_main_weapon_stats_agility = weapon[item_id].Agility;
										holder_remove_item (backpack, backpack[user_ID_choice].ID);
										holder_add_item(backpack, x);						
									}
												
									else if ((UserChoice == "OFFHAND" || UserChoice == "O") && (special_weapon_equiped == 0) && (((player_class_name == "Rogue") && (weapon[item_id].Type == "One Handed")) || ((player_class_name == "Sorceress") && (weapon[item_id].Type == "Parchment Scroll"))))
									{
										bExitLoop4 = true;
																
										int x = player_offhand_weapon_ID_carried;
										player_offhand_weapon_ID_carried = backpack[user_ID_choice].ID;
										player_offhand_weapon_stats_name = weapon[item_id].Name;
										player_offhand_weapon_stats_magic = weapon[item_id].Magic;
										player_offhand_weapon_stats_strength = weapon[item_id].Strength;
										player_offhand_weapon_stats_agility = weapon[item_id].Agility;
										holder_remove_item (backpack, backpack[user_ID_choice].ID);
										holder_add_item(backpack, x);
									}
															
									else if ((UserChoice == "SPECIAL" || UserChoice == "S") && (special_weapon_available == 1) && (player_class_name == weapon[item_id].Race) && ((weapon[item_id].Type == "Two Handed") || (weapon[item_id].Type == "Grand Staff") || (weapon[item_id].Type == "Ranged Weapon")))
									{	
										bExitLoop4 = true;

										int x = player_main_weapon_ID_carried;
										int y = player_offhand_weapon_ID_carried;

										player_main_weapon_ID_carried = backpack[user_ID_choice].ID;
										player_main_weapon_stats_name = weapon[item_id].Name;
										player_main_weapon_stats_magic = weapon[item_id].Magic;
										player_main_weapon_stats_strength = weapon[item_id].Strength;
										player_main_weapon_stats_agility = weapon[item_id].Agility;
										holder_remove_item (backpack, backpack[user_ID_choice].ID);
										holder_add_item(backpack, x);
										holder_add_item(backpack, y);

										special_weapon_equiped = 1;
									}
									
									else if (UserChoice == "DISARM" || UserChoice == "D")
									{
										bExitLoop4 = true;

										holder_space_counter (backpack,backpack_free_space);
					
										if (backpack_free_space >= 2)
										{
											if (player_main_weapon_ID_carried != 999)
											{
												int x = player_main_weapon_ID_carried;
												holder_add_item (backpack, x);
												player_main_weapon_ID_carried = 999;
											}

											if (player_offhand_weapon_ID_carried != 999)
											{
												int x = player_offhand_weapon_ID_carried;
												holder_add_item (backpack, x);
												player_offhand_weapon_ID_carried = 999;
											}

											special_weapon_equiped = 0;

											player_main_weapon_stats_name = "None";
											player_main_weapon_stats_magic = 0;
											player_main_weapon_stats_strength = 0;
											player_main_weapon_stats_agility = 0;

											player_offhand_weapon_stats_name = "None";
											player_offhand_weapon_stats_magic = 0;
											player_offhand_weapon_stats_strength = 0;
											player_offhand_weapon_stats_agility = 0;
																	
											cout << endl;
											cout << "    " << location_access << " -> ACCOUNT -> GEAR -> WEAPONS -> ALLOCATION" << endl;
											cout << "    ------------------------------------------------------------------------" << endl;
											cout << endl;
											cout << "     Your weapons has been stored in backpack successfully." << endl;
											cout << endl;
											cout << "    ------------------------------------------------------------------------" << endl;
											cout << "    |Press ENTER to continue..." << endl;
											cout << "    |";
											WaitForEnter();
										}

										else
										{
											cout << endl;
											cout << "                                     FIZZLE" << endl;
											cout << "    ------------------------------------------------------------------------" << endl;
											cout << endl;
											cout << "     Not enough free space to store weapons." << endl;
											cout << endl;
											cout << "    ------------------------------------------------------------------------" << endl;
											cout << "    |Press ENTER to continue..." << endl;
											cout << "    |";
											WaitForEnter();
										}

									} // (UserChoice == "DISARM" || UserChoice == "D") {} END
															
									else
									{
										cout << endl;
										cout << "                                     FIZZLE" << endl;
										cout << "    ------------------------------------------------------------------------" << endl;
										cout << endl;
										cout << "     Please select right allocation of weapon." << endl;
										cout << endl;
										cout << "    ------------------------------------------------------------------------" << endl;
										cout << "    |Press ENTER to continue..." << endl;
										cout << "    |";
										WaitForEnter();
									}

								}while(!bExitLoop4);	// do-while "ALLOCATION" Loop End

							}

							else if (user_ID_choice < (int) backpack.size())
							{
								cout << endl;
								cout << "                                     FIZZLE" << endl;
								cout << "    ------------------------------------------------------------------------" << endl;
								cout << endl;
								cout << "     No armor in this backpack slot." << endl;
								cout << endl;
								cout << "    ------------------------------------------------------------------------" << endl;
								cout << "    |Press ENTER to continue..." << endl;
								cout << "    |";
								WaitForEnter();
							}

							else if (user_ID_choice >= (int) backpack.size())
							{
								cout << endl;
								cout << "                                     FIZZLE" << endl;
								cout << "    ------------------------------------------------------------------------" << endl;
								cout << endl;
								cout << "     Your backpack does not have so much slots." << endl;
								cout << endl;
								cout << "    ------------------------------------------------------------------------" << endl;
								cout << "    |Press ENTER to continue..." << endl;
								cout << "    |";
								WaitForEnter();
							}

						}	// Valid Number Checker  {} END

						else
						{
							cout << endl;
							cout << "                                     FIZZLE" << endl;
							cout << "    ------------------------------------------------------------------------" << endl;
							cout << endl;
							cout << "     Please type in the correct IDN of Item." << endl;
							cout << endl;
							cout << "    ------------------------------------------------------------------------" << endl;
							cout << "    |Press ENTER to continue..." << endl;
							cout << "    |";
							WaitForEnter();
						}

					}while (!bExitLoop3);

				} // (UserChoice == "WEAPON" || UserChoice == "W") {} END

			}while (!bExitLoop2);
		}	//----------------------------------------(UserChoice == "GEAR" || UserChoice == "G") {} END

		else if (UserChoice == "STATISTICS" || UserChoice == "S")
		{	
			showcase_required_combat = ( (3+2*(player_combat_level-1))*(100 * player_combat_level) - player_combat_experience);
			showcase_required_mining = ( (3+2*(player_mining_level-1))*(100 * player_mining_level) - player_mining_experience);
			showcase_required_farming = ( (3+2*(player_farming_level-1))*(100 * player_farming_level) - player_farming_experience);
			showcase_required_gambling = ( (3+2*(player_gambling_level-1))*(100 * player_gambling_level) - player_gambling_experience);
			Overall_Level_Checkup(overall_player_level, player_combat_level, player_mining_level, player_farming_level, player_gambling_level);

			overall_level_progress = static_cast<int> ( overall_player_level * 100 ) % 100;

			holder_space_counter (backpack,backpack_free_space);
			holder_space_counter (potion_kit, potionkit_free_space);

			cout << endl;
			cout << "    " << location_access << " -> ACCOUNT -> STATISTICS" << endl;
			cout << "    ------------------------------------------------------------------------" << endl;
			cout << endl;
			cout << setw(27) << "Class Type:"			<< setw(32) << player_class_name					<< endl;
			cout << setw(27) << "Location:"				<< setw(32) << town_name							<< endl;
			cout << setw(27) << "Glass Marbles:"		<< setw(32) << player_glass_marbles 				<< endl;
			cout << endl;
			cout << setw(27) << "Overall Level:"		<< setw(32) << (int) overall_player_level			<< endl;
			cout << setw(27) << "Progress:"				<< setw(31) << overall_level_progress 				<< "%" << endl;
			cout << endl;
			cout << setw(27) << "Combat Level:"			<< setw(32) << player_combat_level	 				<< endl;
			cout << setw(27) << "Required EXP:"			<< setw(32) << showcase_required_combat 			<< endl;
			cout << endl;
			cout << setw(27) << "SECRET:"				<< setw(32) << player_farming_level	 				<< endl;
			cout << setw(27) << "Required EXP:"			<< setw(32) << showcase_required_farming	 		<< endl;
			cout << endl;
			cout << setw(27) << "SECRET:"				<< setw(32) << player_mining_level		 			<< endl;
			cout << setw(27) << "Required EXP:"			<< setw(32) << showcase_required_mining				<< endl;
			cout << endl;
			cout << setw(27) << "SECRET:"				<< setw(32) << player_gambling_level 				<< endl;
			cout << setw(27) << "Required EXP:"			<< setw(32) << showcase_required_gambling			<< endl;
			cout << endl;
			cout << setw(27) << "Size of Backpack:"		<< setw(32) << backpack.size()						<< endl;
			cout << setw(27) << "Free Space:"			<< setw(32) << backpack_free_space					<< endl;
			cout << endl;
			cout << setw(27) << "Size of Potion Kit:"	<< setw(32) << potion_kit.size()		 			<< endl;
			cout << setw(27) << "Free Space:"			<< setw(32) << potionkit_free_space		 			<< endl;
			cout << endl;
			cout << setw(27) << "Total Travels:"		<< setw(32) << total_number_of_travels	 			<< endl;
			cout << setw(27) << "Current Travels:"		<< setw(32) << current_number_of_travels 			<< endl;
			cout << endl;
			cout << endl;
			cout << "    ------------------------------------------------------------------------" << endl;
			cout << "    |Press ENTER to continue..." << endl;
			cout << "    |";
			WaitForEnter();
		}	//----------------------------------------(UserChoice == "STATISTICS" || UserChoice == "S") {} END

	}while(!bExitLoop1);

}
//-------------------------------------------------------------END OF PROGRAM-------------------------------------------------------------\\