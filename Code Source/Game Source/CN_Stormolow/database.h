//
// Include guard
//
#ifndef DATABASE_H
#define DATABASE_H

#include <string>
using namespace std;

int load();
int code_string(string &str);

const int NUMBEROFITEMS = 120;	
const int NUMBEROFWEAPONS = 41;
const int NUMBEROFARMORS = 34;
const int NUMBEROFPOTIONS = 29;
const int NUMBEROFCARDS = 13;
const int NUMBEROFSKINNER = 8;
const int NUMBEROFMAP = 3;

//
// STRUCTURES
//

struct holder{
	int ID;
	string Name;
	int Value;

	holder()										//Constructor for Default New Default Values.
	{
		ID = 999;
		Name = "   Empty Slot   ";
		Value = 0;
	}
};

struct items {	
	int ID;
	string Name;
	string Description;
	string Location;
	string Type;
	int Buy;
	int Sell;
	int Requires1;
	int Requires2;
	int Requires3;
}extern item[NUMBEROFITEMS];

struct armors {
	int ID;
	string Name;
	string Description;
	string Location;
	string Race;
	string Type;
	int Buy;
	int Sell;
	int Health;
	int Defense;
	int Agility;
	int Level;
}extern armor[NUMBEROFARMORS];

struct weapons {
	int ID;
	string Name;
	string Description;
	string Location;
	string Race;
	string Type;
	int Buy;
	int Sell;
	int Magic;
	int Strength;
	int Agility;
	int Level;
}extern weapon[NUMBEROFWEAPONS];

struct potions {
	int ID;
	string Name;
	string Type;
	int Buy;
	int Sell;
	int Level;
	int Health;
	int Strength;
	int Magic;
	int Defense;
	int Agility;
	int Requires1;
	int Requires2;
	int Requires3;
}extern potion[NUMBEROFPOTIONS];

struct deck {
	int Cardvalue;
	string Cardname;
	int Lower_bonus;
	int Higher_bonus;
}extern cards[NUMBEROFCARDS];

struct skinners{
	int ID;
	string Name;
	int Fur;
	int Hide;
	int Buy;
	int Size;
	int Owned;
}extern skinner[NUMBEROFSKINNER];

struct maps{
	int ID;
	string Region_Name;
	string Area1;
	int Area1_ID1;
	int Area1_ID2;
	string Area2;
	int Area2_ID1;
	int Area2_ID2;
	string Area3;
	int Area3_ID1;
	int Area3_ID2;
	int Area3_ID3;
}extern map[NUMBEROFMAP];

#endif					//end of the include guard