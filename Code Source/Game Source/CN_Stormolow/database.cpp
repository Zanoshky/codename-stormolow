﻿#include <iostream>
#include <string>
#include <fstream>
#include <sstream>

#include "database.h"

using namespace std;

int code_all();	
int code_string(string &str);

maps map[NUMBEROFMAP];
deck cards[NUMBEROFCARDS];
items item[NUMBEROFITEMS];
armors armor[NUMBEROFARMORS];
weapons weapon[NUMBEROFWEAPONS];
potions potion[NUMBEROFPOTIONS];
skinners skinner[NUMBEROFSKINNER];

int load()
{		
	int check;
	int point = ';';
	char* chararray = new char[72];

	ifstream datafile;
	datafile.open("CODE-Data.nsk",ios::in);	

	if (!datafile.good())
	{
		cout << endl;

		if (datafile.eof())	
		{
			cout << "    |ERROR 400 - Damaged 'CODE-Data.nsk' file." << endl;
		}

		else
		{
			cout << "    |ERROR 401 - Missing 'CODE-Data.nsk' file." << endl;	
		}

		system("PAUSE");
		return 1;
	}

	for (int itemnumber = 0; itemnumber < NUMBEROFITEMS; itemnumber++)
	{
		datafile >> item[itemnumber].ID;
		datafile.ignore();

		datafile.getline(chararray,50,point);		
		item[itemnumber].Name = chararray;

		datafile.getline(chararray,100,point);
		item[itemnumber].Description = chararray;
		
		datafile.getline(chararray,100,point);
		item[itemnumber].Location = chararray;	

		datafile.getline(chararray,20,point);
		item[itemnumber].Type = chararray;

		datafile >> item[itemnumber].Buy;			
		datafile.ignore();	

		datafile >> item[itemnumber].Sell;			
		datafile.ignore();

		datafile >> item[itemnumber].Requires1;		
		datafile.ignore();	

		datafile >> item[itemnumber].Requires2;		
		datafile.ignore();	

		datafile >> item[itemnumber].Requires3;		
		datafile.ignore();

		datafile >> check;
		datafile.ignore();

		if (check != (item[itemnumber].ID -item[itemnumber].Buy +item[itemnumber].Sell+item[itemnumber].Requires1-item[itemnumber].Requires2+item[itemnumber].Requires3))
		{
			cout << check << endl;
			cout << "    |ERROR 402 - Damaged 'CODE-Data.nsk' file." << endl;

			system("PAUSE");
			return 1;
		}

	}

	for (int armornumber = 0; armornumber < NUMBEROFARMORS; armornumber++)
	{
		datafile >> armor[armornumber].ID;
		datafile.ignore();

		datafile.getline(chararray,50,point);		
		armor[armornumber].Name = chararray;

		datafile.getline(chararray,100,point);
		armor[armornumber].Description = chararray;

		datafile.getline(chararray,100,point);
		armor[armornumber].Location = chararray;
		
		datafile.getline(chararray,100,point);
		armor[armornumber].Race = chararray;

		datafile.getline(chararray,100,point);
		armor[armornumber].Type = chararray;

		datafile >> armor[armornumber].Buy;			
		datafile.ignore();	

		datafile >> armor[armornumber].Sell;			
		datafile.ignore();

		datafile >> armor[armornumber].Level;		
		datafile.ignore();

		datafile >> armor[armornumber].Health;		
		datafile.ignore();	

		datafile >> armor[armornumber].Defense;		
		datafile.ignore();	

		datafile >> armor[armornumber].Agility;			
		datafile.ignore();	
		
		datafile >> check;
		datafile.ignore();

		if (check != (armor[armornumber].ID -armor[armornumber].Buy +armor[armornumber].Sell+armor[armornumber].Health+armor[armornumber].Agility-armor[armornumber].Defense +armor[armornumber].Level))
		{
			cout << "    |ERROR 403 - Damaged 'CODE-Data.nsk' file." << endl;

			system("PAUSE");
			return 1;
		}
	}
	
	for (int weaponnumber = 0; weaponnumber < NUMBEROFWEAPONS; weaponnumber++)
	{
		datafile >> weapon[weaponnumber].ID;
		datafile.ignore();

		datafile.getline(chararray,50,point);		
		weapon[weaponnumber].Name = chararray;

		datafile.getline(chararray,100,point);
		weapon[weaponnumber].Description = chararray;

		datafile.getline(chararray,100,point);
		weapon[weaponnumber].Location = chararray;

		datafile.getline(chararray,100,point);
		weapon[weaponnumber].Race = chararray;

		datafile.getline(chararray,100,point);
		weapon[weaponnumber].Type = chararray;

		datafile >> weapon[weaponnumber].Buy;			
		datafile.ignore();	

		datafile >> weapon[weaponnumber].Sell;			
		datafile.ignore();	

		datafile >> weapon[weaponnumber].Level;		
		datafile.ignore();

		datafile >> weapon[weaponnumber].Magic;		
		datafile.ignore();	
		
		datafile >> weapon[weaponnumber].Strength;		
		datafile.ignore();	

		datafile >> weapon[weaponnumber].Agility;			
		datafile.ignore();	

		datafile >> check;
		datafile.ignore();

		if (check != (weapon[weaponnumber].ID -weapon[weaponnumber].Buy +weapon[weaponnumber].Sell +weapon[weaponnumber].Strength -weapon[weaponnumber].Magic +weapon[weaponnumber].Level))
		{
			cout << "    |ERROR 404 - Damaged 'CODE-Data.nsk' file." << endl;

			system("PAUSE");
			return 1;
		}
	}

	for(int potionnumber=0;potionnumber<NUMBEROFPOTIONS; potionnumber++)
	{
		datafile >> potion[potionnumber].ID;
		datafile.ignore();

		datafile.getline(chararray,50,point);		
		potion[potionnumber].Name = chararray;	

		datafile.getline(chararray,100,point);
		potion[potionnumber].Type = chararray;

		datafile >> potion[potionnumber].Buy;			
		datafile.ignore();	

		datafile >> potion[potionnumber].Sell;			
		datafile.ignore();	

		datafile >> potion[potionnumber].Level;			
		datafile.ignore();	

		datafile >> potion[potionnumber].Health;		
		datafile.ignore();	

		datafile >> potion[potionnumber].Magic;		
		datafile.ignore();	

		datafile >> potion[potionnumber].Strength;		
		datafile.ignore();	

		datafile >> potion[potionnumber].Defense;		
		datafile.ignore();	

		datafile >> potion[potionnumber].Agility;			
		datafile.ignore();	

	 	datafile >> potion[potionnumber].Requires1;		
		datafile.ignore();

		datafile >> potion[potionnumber].Requires2;		
		datafile.ignore();
		
		datafile >> potion[potionnumber].Requires3;		
		datafile.ignore();

		datafile >> check;
		datafile.ignore();

		if (check!= (potion[potionnumber].ID -potion[potionnumber].Buy +potion[potionnumber].Sell +potion[potionnumber].Strength -potion[potionnumber].Magic))
		{
			cout << "    |ERROR 405 - Damaged 'CODE-Data.nsk' file." << endl;

			system("PAUSE");
			return 1;
		}
	}

	for(int cardsnumber=0;cardsnumber<NUMBEROFCARDS; cardsnumber++)
	{
		datafile >> cards[cardsnumber].Cardvalue;
		datafile.ignore();

		datafile.getline(chararray,50,point);		
		cards[cardsnumber].Cardname = chararray;
	
		datafile >> cards[cardsnumber].Lower_bonus;
		datafile.ignore();

		datafile >> cards[cardsnumber].Higher_bonus;
		datafile.ignore();				
			
		datafile >> check;
		datafile.ignore();

		if (check !=(cards[cardsnumber].Cardvalue + cards[cardsnumber].Higher_bonus + cards[cardsnumber].Lower_bonus))
		{
			cout << "    |ERROR 406 - Damaged 'CODE-Data.nsk' file." << endl;

			system("PAUSE");
			return 1;
		}
	}
		
	for (int skinnernumber(0); skinnernumber < NUMBEROFSKINNER; skinnernumber++)
	{
		datafile >> skinner[skinnernumber].ID;
		datafile.ignore();

		datafile.getline(chararray,50,point);		
		skinner[skinnernumber].Name = chararray;

		datafile >> skinner[skinnernumber].Buy;			
		datafile.ignore();	

		datafile >> skinner[skinnernumber].Fur;		
		datafile.ignore();	

		datafile >> skinner[skinnernumber].Hide;		
		datafile.ignore();	
		
		datafile >> skinner[skinnernumber].Size;		
		datafile.ignore();	

		datafile >> skinner[skinnernumber].Owned;			
		datafile.ignore();	
		
		datafile >> check;
		datafile.ignore();	

		if (check != (skinner[skinnernumber].Buy + 2* skinner[skinnernumber].Fur + skinner[skinnernumber].Hide + skinner[skinnernumber].ID * skinner[skinnernumber].Size + skinner[skinnernumber].Owned))
		{
			cout << "    |ERROR 407 - Damaged 'CODE-Data.nsk' file." << endl;

			system("PAUSE");
			return 1;
		}
	}
		
	for (int mapnumber = 0; mapnumber < NUMBEROFMAP; mapnumber++)
	{
		datafile >> map[mapnumber].ID;
		datafile.ignore();

		datafile.getline(chararray,50,point);		
		map[mapnumber].Region_Name = chararray;	

		datafile.getline(chararray,50,point);		
		map[mapnumber].Area1 = chararray;

		datafile >> map[mapnumber].Area1_ID1;			
		datafile.ignore();	

		datafile >> map[mapnumber].Area1_ID2;		
		datafile.ignore();	

		datafile.getline(chararray,50,point);		
		map[mapnumber].Area2 = chararray;

		datafile >> map[mapnumber].Area2_ID1;			
		datafile.ignore();	

		datafile >> map[mapnumber].Area2_ID2;		
		datafile.ignore();	

		datafile.getline(chararray,50,point);		
		map[mapnumber].Area3 = chararray;		

		datafile >> map[mapnumber].Area3_ID1;			
		datafile.ignore();	

		datafile >> map[mapnumber].Area3_ID2;		
		datafile.ignore();	

		datafile >> map[mapnumber].Area3_ID3;		
		datafile.ignore();	

		datafile >> check;
		datafile.ignore();	

		if (check != (map[mapnumber].Area1_ID1 + 2*map[mapnumber].Area1_ID2	+ 3 * map[mapnumber].Area2_ID1 + map[mapnumber].Area2_ID2 + 2 * map[mapnumber].Area3_ID1 + map[mapnumber].Area3_ID2 + map[mapnumber].Area3_ID3))
		{
			cout << "    |ERROR 408 - Damaged 'CODE-Data.nsk' file." << endl;

			system("PAUSE");
			return 1;
		}
	}

	code_all();	

	return 0;
}

int code_all()
{
	for (int itemnumber = 0; itemnumber<NUMBEROFITEMS; itemnumber++)
	{
		item[itemnumber].ID					=(item[itemnumber].ID/758)-1;
		item[itemnumber].Buy				=(item[itemnumber].Buy/48)-1;
		item[itemnumber].Sell				=(item[itemnumber].Sell /5)-1;
		item[itemnumber].Requires1			=(item[itemnumber].Requires1/97)-1;
		item[itemnumber].Requires2			=(item[itemnumber].Requires2/12)-1;
		item[itemnumber].Requires3			=(item[itemnumber].Requires3/5)-1;

		code_string(item[itemnumber].Name);	
		code_string(item[itemnumber].Description);
		code_string(item[itemnumber].Location);
		code_string(item[itemnumber].Type);			
	}

	for (int weaponnumber = 0; weaponnumber<NUMBEROFWEAPONS; weaponnumber++)
	{ 
		weapon[weaponnumber].ID					=(weapon[weaponnumber].ID/758)-1;
		weapon[weaponnumber].Buy				=(weapon[weaponnumber].Buy/48)-1;
		weapon[weaponnumber].Sell				=(weapon[weaponnumber].Sell/5)-1;
		weapon[weaponnumber].Level				=(weapon[weaponnumber].Level/25)-1;
		weapon[weaponnumber].Magic				=(weapon[weaponnumber].Magic/15)-1;
		weapon[weaponnumber].Strength			=(weapon[weaponnumber].Strength/5)-1;
		weapon[weaponnumber].Agility			=(weapon[weaponnumber].Agility/57)-1;

		code_string(weapon[weaponnumber].Name);
		code_string(weapon[weaponnumber].Description);
		code_string(weapon[weaponnumber].Location);
		code_string(weapon[weaponnumber].Race);	
		code_string(weapon[weaponnumber].Type);	
	}

	for (int armornumber = 0; armornumber<NUMBEROFARMORS; armornumber++)
	{
		armor[armornumber].ID				=(armor[armornumber].ID/758)-1;
		armor[armornumber].Buy				=(armor[armornumber].Buy/48)-1;
		armor[armornumber].Sell				=(armor[armornumber].Sell/5)-1;
		armor[armornumber].Level			=(armor[armornumber].Level/13)-1;
		armor[armornumber].Health			=(armor[armornumber].Health/158)-1;
		armor[armornumber].Defense			=(armor[armornumber].Defense/158)-1;
		armor[armornumber].Agility			=(armor[armornumber].Agility/57)-1;

		code_string(armor[armornumber].Name);
		code_string(armor[armornumber].Description);
		code_string(armor[armornumber].Location);
		code_string(armor[armornumber].Race);
		code_string(armor[armornumber].Type);
	}

	for (int potionnumber = 0; potionnumber<NUMBEROFPOTIONS; potionnumber++)
	{
		potion[potionnumber].Agility		=(potion[potionnumber].Agility/57)-1;
		potion[potionnumber].Buy			=(potion[potionnumber].Buy/48)-1;
		potion[potionnumber].Defense		=(potion[potionnumber].Defense/158)-1;
		potion[potionnumber].Health			=(potion[potionnumber].Health/158)-1;
		potion[potionnumber].ID				=(potion[potionnumber].ID/758)-1;
		potion[potionnumber].Magic			=(potion[potionnumber].Magic/15)-1;
		potion[potionnumber].Requires1		=(potion[potionnumber].Requires1/97)-1;
		potion[potionnumber].Requires2		=(potion[potionnumber].Requires2/12)-1;
		potion[potionnumber].Requires3		=(potion[potionnumber].Requires3/5)-1;
		potion[potionnumber].Sell			=(potion[potionnumber].Sell/5)-1;
		potion[potionnumber].Strength		=(potion[potionnumber].Strength/5)-1;
			
		code_string(potion[potionnumber].Name);
		code_string(potion[potionnumber].Type);
	}

	for (int cardsnumber = 0; cardsnumber<NUMBEROFCARDS; cardsnumber++)
	{
		cards[cardsnumber].Cardvalue		=(cards[cardsnumber].Cardvalue/35)-1;
		cards[cardsnumber].Lower_bonus		=(cards[cardsnumber].Lower_bonus/762)-1;
		cards[cardsnumber].Higher_bonus		=(cards[cardsnumber].Higher_bonus/95)-1;

		code_string(cards[cardsnumber].Cardname);
	}

	for (int skinnernumber = 0; skinnernumber < NUMBEROFSKINNER; skinnernumber++)
	{
		skinner[skinnernumber].ID			=(skinner[skinnernumber].ID/97)-1;
		skinner[skinnernumber].Fur			=(skinner[skinnernumber].Fur/31)-1;
		skinner[skinnernumber].Hide			=(skinner[skinnernumber].Hide/12)-1;
		skinner[skinnernumber].Buy			=(skinner[skinnernumber].Buy/84)-1;
		skinner[skinnernumber].Size			=(skinner[skinnernumber].Size/45)-1;

		code_string(skinner[skinnernumber].Name);
	}

	for (int mapnumber(0); mapnumber < NUMBEROFMAP; mapnumber++)
	{
		map[mapnumber].ID					 =(map[mapnumber].ID/37)-1;
		map[mapnumber].Area1_ID1			 =(map[mapnumber].Area1_ID1/154)-1;
		map[mapnumber].Area1_ID2			 =(map[mapnumber].Area1_ID2/75)-1;
		map[mapnumber].Area2_ID1			 =(map[mapnumber].Area2_ID1/97)-1;
		map[mapnumber].Area2_ID2			 =(map[mapnumber].Area2_ID2/34)-1;
		map[mapnumber].Area3_ID1			 =(map[mapnumber].Area3_ID1/16)-1;
		map[mapnumber].Area3_ID2			 =(map[mapnumber].Area3_ID2/18)-1;
		map[mapnumber].Area3_ID3			 =(map[mapnumber].Area3_ID3/39)-1;

		code_string(map[mapnumber].Area1);
		code_string(map[mapnumber].Area2);
		code_string(map[mapnumber].Area3);
		code_string(map[mapnumber].Region_Name);
	}

	return 0;
}

int code_string(string &str)
{		
	int const codesize(29);
	static int const CODE[codesize] = {14,21,18,17,16,24,18,19,16,16,18,17,16,18,25,18,15,14,21,15,16,17,18,19,20,19,18,17,16};
	int codeletter(0);

	char *a = new char[str.size()+1];				//strings don't support '^' so every string gets an equivalent char[]
	a[str.size()]=0;								//end of the string is always 0 so has to be the end of the char
	memcpy(a,str.c_str(),str.size());				//copying string to char

	for (unsigned int letter = 0; letter < (str.size()); letter++,codeletter++)  //coding for every letter, BUT the terminating 0;
	{ 
		a[letter]= static_cast <int> (a[letter]) - CODE[codeletter];		//exclusive disjuntion 
		if (codeletter == (codesize-1))									//checking of the end of the code has been reached 
		{							
			codeletter=0;												//going to the next letter of the code
		}
	}

	str = a;															//copying back
	a ="";

	return 0;
}